 var countries = {
    'America': {
        		'flag1': 'img/eng.png',
        		'country1': [
			            'USA',
			            'Canada',
			            'Cuba',
						'aaaaaaaa',
						'bbbbbbb'
			             ]
        	},
    'Asia': {
		        'flag1': 'img/rus.png',
		        'country1': [
		            'Russia',
		            'aaaaaaaa',
		            'aaaaaaaa2'
		             ]
        	},
    'Europe': {
		        'flag1': 'img/ukr.png',
		        'country1': [
		            'Ukraine',
		            'Germany',
		            'France'
		             ]
        	},
    'Afrika': {
		        'flag1': 'img/eng.png',
		        'country1': [
		            'Sudan',
		            'Niger',
		            'Egypt'
		             ]
        	},
    'Australia': {
		        'flag1': 'img/rus.png',
		        'country1': [
		            'Australia'
		             ]
        	},   
}


$(function(){
	//запуск слайдера для новостей
	$('#slider1').tinycarousel({
		axis: 'y', // vertical or horizontal scroller? 'x' or 'y' .
		animation : 'true',
		duration: 500,
	});

	var index = $('#menu');
	if (index.length>0){
		var html = $('#menu').html();
	    var content = tmpl(html, countries);  
	    $('.select-menu ul').append(content);   
	}
//////////////////////////////////////////////////////////////

	//Подгружение картинок после загрузки DOM
	var source = {
		mainBg: "img/bg_dots.jpg",
		footerBg: "img/bg-bottom.png",
		errorBg: "img/404bg.png",
		aboutBg: "img/aboutBg.jpg",
		video: "<iframe width='auto' height='auto' src='https://www.youtube.com/embed/xcvBovMKEAE' allowfullscreen></iframe>"
	};

	//$('.center-wr').css({'backgroundImage':'url('+source.mainBg+')', 'backgroundPosition': 'center', 'backgroundRepeat': 'no-repeat', 'backgroundSize': 'cover'});
	//$('footer').css('backgroundImage','url('+source.footerBg+')');
	//$('.error-wr').css('backgroundImage','url('+source.errorBg+')');
	//$('.about-wr').css('backgroundImage','url('+source.aboutBg+')');
	//$('.video').html(source.video);

	///////////////////////////////////////////////
	//Слайдер новостей
	var src_first = $('#slider1 li').first().children().attr('src');
	$('.sl-big img').attr('src', src_first);
	$('#slider1 li').on('click', function(){
		var src = $(this).children('img').attr('src');
		$('.sl-big img').attr('src', src);
	});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//раскрыть селект
	$('.country').on('click', function(){
		$('.select-menu').slideToggle();
		$('.part-w').first().children('.sub-menu').fadeIn(1000);
	});

	//при наведении выпадает подменю
	$('.part-w').hover(
		function(){ 
			$(this).addClass('active');
					$(this).children('.sub-menu').show();
				},
		function(){
			$(this).removeClass('active');
					$(this).children('.sub-menu').hide();
				}
	);

	//при покидании области меню, оно прячется
	$('.select-menu').on('mouseleave', function(){
		$(this).slideUp();
	});
	//$('.language').on('mouseleave', function(){
	//	$(this).slideUp();
	//});

	//при нажатии меняется страна
	$('.select-menu a').on('click', changeCountry);
	
		function changeCountry(){
			var title = $(this).html();
			var titlefl = $(this).attr('style');

			$('.country').html(title);
			$('.country').attr('style', titlefl);
			$('.select-menu').hide();
		}


	/////////////////////////
	$('.language li').first().addClass('active');
	// Смена языка
	$('.selected').on('click', function(){
		//$('.language').show();
		$(this).siblings('.language').slideDown();
	});
	$('.language li').on('click', function(){
		var lang_icon = $(this).children('a').attr('style');
		var lang_text = $(this).children('a').html();
		$('.selected').attr('style', lang_icon);
		$('.selected').html(lang_text);
		$(this).addClass('active');
		$('.language li').not(this).removeClass('active');
		$('.language').slideUp();
	});
////////////////////////////////////////////////////////////////////////////////////////////
    //Добавляем кнопку для меню - меню будет сложено в столбик
	$('.menuBut').click(function(){
		$('.menuSmall').show();
	});
	$('.newMenu .menuSmall').on('mouseleave', function(){
		$(this).slideUp();
	});
	//if($(window).width()<400){
	//	$('.selected, .language a').text('');
	//}

	$('.products').hover(function(){
		$(this).children('.products-under').show();
		$(this).children('.products-over').hide();
	},
			function(){
				$(this).children('.products-over').show();
				$(this).children('.products-under').hide();
			}
	);

});