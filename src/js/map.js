/* 
 * @author Anastasia Popova
 * @copyright (c) 2016, VipDesign
 */

/**
 * initialize function
 * @returns {undefined}
 */

function initialize() {
    var myLatlng = new google.maps.LatLng(50.4501, 30.5234);
    var myOptions = {
        zoom: 2,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl:false
    }
    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    /**
     * map styles
     * @type Array
     */
    var styles = [
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {"color": "#0c4da1"}
            ]
        },
        {
            featureType: 'administrative.country',
            elementType: 'geometry',
            stylers: [
                {"lightness": -5},
                {"color": "#ed8058"}
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {"color": "#2963ba"}

            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {"color": "#808080"}
            ]
        },
        {
            "featureType": "water",
            "stylers": [
                {"color": "#54a7d9"},
                {"lightness": 11}
            ]
        }

    ];
    map.setOptions({styles: styles});

    /**
     * geocoder find adress param {id}
     * @type google.maps.Geocoder
     */
    var geocoder = new google.maps.Geocoder();
    
    $('.country').bind('DOMSubtreeModified', function(){
        address = $(this).html();

       geocodeAddress(geocoder, map);

       //set zoom for country
       if (address == "Russia" || address == "USA" || address == 'Россия' || address == 'США') {
           map.setZoom(4);
       } else {
           map.setZoom(6);
       }
    
    });
    
    //create infoWindow
    var infoWindow = new google.maps.InfoWindow;
    /**
     * function create marker
     * @returns {undefined}
     */
    var onMarkerClick = function () {
        var marker = this;
        var latLng = marker.getPosition();
        infoWindow.setContent(marker.content);
        //infoWindow.setStyle('overflow', 'hidden');
        infoWindow.open(map, marker);
    };
    
    var onMarkerOver = function(){
        var marker = this;
            marker.setIcon('img/marker-active.png'); 
    };
    var onMarkerOut = function(){
        var marker = this;
            marker.setIcon('img/marker.png'); 
    };
    
    google.maps.event.addListener(map, 'click', function () {
        infoWindow.close();
    });

    //Kyiv
    var marker1 = new google.maps.Marker({
        map: map,
        icon: 'img/marker.png',
        content: "Kyiv <br><a href='#'>kiev.com.ua</a>",
        position: new google.maps.LatLng(50.4501, 30.5234)
    });
    //Warwava
    var marker2 = new google.maps.Marker({
        map: map,
        icon: 'img/marker.png',
        content: 'Warwava',
        position: new google.maps.LatLng(52.2296756, 21.0122287)
    });
    //Minsk
    var marker3 = new google.maps.Marker({
        map: map,
        icon: 'img/marker.png',
        content: 'Minsk',
        position: new google.maps.LatLng(53.9045398, 27.5615244)
    });
    
    //create events for all markers
    var markers = [marker1, marker2, marker3];
    for (i=0; i<markers.length; i++){
        google.maps.event.addListener(markers[i], 'click', onMarkerClick);
        google.maps.event.addListener(markers[i], 'mouseover', onMarkerOver);
        google.maps.event.addListener(markers[i], 'mouseout', onMarkerOut);
    }
  

}

initialize();

/**
 * 
 * @param {type} geocoder
 * @param {type} resultsMap
 * @returns {undefined}
 * calculates where the county is situated
 */
function geocodeAddress(geocoder, resultsMap) {

    geocoder.geocode({'address': address}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            //get center on the country
            resultsMap.setCenter(results[0].geometry.location);

        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}
