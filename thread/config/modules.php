<?php
return [
    'page' => [
        'class' => \thread\modules\page\Page::class,
    ],
    'news' => [
        'class' => \thread\modules\news\News::class,
    ],
    'user' => [
        'class' => \thread\modules\user\User::class,
    ],
    'seo' => [
        'class' => \thread\modules\seo\Seo::class,
    ],
    'location' => [
        'class' => \thread\modules\location\Location::class,
    ],
    'menu' => [
        'class' => \thread\modules\menu\Menu::class,
    ],
    'brands' => [
        'class' => \thread\modules\brands\Brands::class,
    ],
    'aboutus' => [
        'class' => \thread\modules\aboutus\Aboutus::class,
    ],
    'forms' => [
        'class' => \thread\modules\forms\Forms::class,
    ],
    'contacts' => [
        'class' => \thread\modules\contacts\Contacts::class,
    ],
];
