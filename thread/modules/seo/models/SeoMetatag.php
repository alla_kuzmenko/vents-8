<?php

namespace thread\modules\seo\models;

use Yii;
//
use thread\app\base\models\ActiveRecord;
use thread\modules\seo\Seo as SeoModule;

/**
 * Class SeoMetatag
 *
 * @property integer $id
 * @property string $url
 * @property string $url_hash
 * @property boolean $published
 * @property boolean $deleted
 *
 * @property SeoMetatagLang[] $lang
 *
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
class SeoMetatag extends ActiveRecord
{
    public $modelName = 'SeoMetatag';

    /**
     * Using db connection
     *
     * @return null|object|string
     */
    public static function getDb()
    {
        return SeoModule::getDb();
    }

    /**
     * Page table name
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%seo_metatag}}';
    }

    /**
     * Description
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url_hash'], 'unique'],
            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())]
        ];
    }

    /**
     * Description
     *
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'backend' => [
                'url',
                'published',
                'deleted',
            ],
        ];
    }

    /**
     *  Description
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'URL'),
            'url_hash' => Yii::t('app', 'Url hash'),
            'published' => Yii::t('app', 'Published'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasMany(SeoMetatagLang::class, ['rid' => 'id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->url_hash = sha1($this->url);
        return parent::beforeSave($insert);
    }

    /**
     * find by url
     * @param $url
     * @return mixed
     */
    public static function findByUrl($url)
    {
        return self::findBase()->andWhere(['url' => $url])->enabled()->one();
    }
}
