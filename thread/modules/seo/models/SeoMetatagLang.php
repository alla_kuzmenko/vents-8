<?php
namespace thread\modules\seo\models;

use Yii;
use yii\helpers\ArrayHelper;
//
use thread\app\base\models\ActiveRecordLang;
use thread\modules\seo\Seo as SeoModule;

/**
 * Class SeoMetatagLang
 *
 * @property integer $rid
 * @property string $lang
 * @property string $title
 * @property string $description
 * @property string $keywords
 *
 * @package thread\modules\seo\models
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 * @copyright (c) 2016, VipDesign
 */
class SeoMetatagLang extends ActiveRecordLang
{
    /**
     * Db connection
     * @return string
     */
    public static function getDb()
    {
        return SeoModule::getDb();
    }

    /**
     * Table name
     * @return string
     */
    public static function tableName()
    {
        return '{{%seo_metatag_lang}}';
    }

    /**
     * Rules
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['rid', 'exist', 'targetClass' => Seo::class, 'targetAttribute' => 'id'],
                [['title', 'description', 'keywords'], 'string', 'max' => 255],
            ]
        );
    }

    /**
     * Labels
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'rid' => Yii::t('app', 'rid'),
            'lang' => Yii::t('app', 'lang'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'keywords' => Yii::t('app', 'Keywords'),
        ];
    }

    /**
     * Scenarios
     * @return array
     */
    public function scenarios()
    {
        return [
            'backend' => ['title', 'description', 'keywords'],
        ];
    }
}
