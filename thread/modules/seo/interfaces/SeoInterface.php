<?php
namespace thread\modules\seo\interfaces;

/**
 * Interface SeoInterface
 */

interface SeoInterface
{

    /**
     * Получаем namespace модели, мелательно чтобы namespace был модели common
     * @return mixed
     */
    public static function getModelNamespace();

}