<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 * @version 28/03/2015
 */
return [
    'Display in the search engine' => 'Отображать в поисковике',
    'Disallow in robots txt' => 'Запретить в robots.txt',
    'Write to siteMap' => 'Записывать в siteMap',
    'SEO title' => 'SEO заголовок',
    'SEO description' => 'SEO описание',
    'SEO keywords' => 'SEO ключевые слова'

];
