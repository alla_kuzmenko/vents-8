<?php

use yii\db\Migration;

class m160728_121703_create_table_seo_robots extends Migration
{
    public $tableName = '{{%seo_robots_element}}';

    public function init()
    {
        $this->db = \thread\modules\seo\Seo::getDb();
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->unsigned()->comment('ID'),
            'module_id' => $this->string(255)->notNull()->comment('Module ID'),
            'model_id' => $this->string(255)->notNull()->comment('Model ID'),
            'key' => $this->string(50)->notNull()->comment('Key'),
            'url' => $this->text()->notNull()->comment('Url'),
            'created_at' => $this->integer(10)->notNull()->defaultValue(0)->comment('Create time'),
            'updated_at' => $this->integer(10)->notNull()->defaultValue(0)->comment('Update time'),
        ]);
        $this->createIndex('module_id', $this->tableName, 'module_id');
        $this->createIndex('model_id', $this->tableName, 'model_id');
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropIndex('module_id', $this->tableName);
        $this->dropIndex('model_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
