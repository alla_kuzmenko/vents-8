<?php

use yii\db\Migration;

class m160729_065335_create_table_seo_metatag extends Migration
{

    public $seoMetatagTable = '{{%seo_metatag}}';

    public function safeUp()
    {
        $this->createTable($this->seoMetatagTable, [
            'id' => $this->primaryKey()->unsigned()->comment('ID'),
            'url' => $this->text()->notNull()->comment('URL'),
            'url_hash' => $this->string(40)->notNull()->unique()->comment('URL hash'),
            'published' => $this->boolean()->notNull()->defaultValue(0)->comment('Published'),
            'deleted' => $this->boolean()->notNull()->defaultValue(0)->comment('Deleted'),
            'created_at' => $this->integer(10)->notNull()->defaultValue(0)->comment('Create time'),
            'updated_at' => $this->integer(10)->notNull()->defaultValue(0)->comment('Update time')
        ]);

        $this->createIndex('published', $this->seoMetatagTable, 'published');
        $this->createIndex('deleted', $this->seoMetatagTable, 'deleted');

        $this->insert($this->seoMetatagTable, [
            'id' => 1,
            'url' => '/',
            'url_hash' => sha1('/'),
            'published' => 1,
            'deleted' => 0,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function safeDown()
    {
        $this->dropIndex('deleted', $this->seoMetatagTable);
        $this->dropIndex('published', $this->seoMetatagTable);
        $this->dropTable($this->seoMetatagTable);
    }
}
