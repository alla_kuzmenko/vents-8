<?php

use yii\db\Migration;

class m160729_070555_create_table_seo_metatag_lang extends Migration
{
    /**
     * @var string
     */
    public $tableSeoMetaTag = '{{%seo_metatag}}';

    /**
     * @var string
     */
    public $tableSeoMetaTagLang = '{{%seo_metatag_lang}}';

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tableSeoMetaTagLang, [
            'rid' => $this->integer()->unsigned()->notNull()->comment('Related model ID'),
            'lang' => $this->string(5)->notNull()->comment('Language'),
            'title' => $this->string(255)->notNull()->comment('Seo Title'),
            'description' => $this->string(255)->notNull()->comment('Seo Description'),
            'keywords' => $this->string(255)->notNull()->comment('Seo Keywords'),
        ]);
        $this->createIndex('rid', $this->tableSeoMetaTagLang, ['rid', 'lang'], true);
        $this->addForeignKey(
            'fk-seo_metatag_lang-rid-seo_metatag-id',
            $this->tableSeoMetaTagLang,
            'rid',
            $this->tableSeoMetaTag,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->insert($this->tableSeoMetaTagLang, [
            'rid' => 1,
            'lang' => 'en-EN',
            'title' => 'Vents Title',
            'description' => 'Meta index description',
            'keywords' => 'Meta index keywords'
        ]);
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-seo_metatag_lang-rid-seo_metatag-id', $this->tableSeoMetaTagLang);
        $this->dropIndex('rid', $this->tableSeoMetaTagLang);
        $this->dropTable($this->tableSeoMetaTagLang);
    }
}
