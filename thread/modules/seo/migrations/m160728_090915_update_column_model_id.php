<?php

use yii\db\Migration;

class m160728_090915_update_column_model_id extends Migration
{

    public $tableSeo = '{{%seo}}';
    public $tableSeoLang = '{{%seo_lang}}';

    public function up()
    {
        //clean table
        $this->dropForeignKey('fk-seo_lang-rid-seo-id', $this->tableSeoLang);
        $this->truncateTable($this->tableSeoLang);
        $this->truncateTable($this->tableSeo);
        $this->addForeignKey(
            'fk-seo_lang-rid-seo-id',
            $this->tableSeoLang,
            'rid',
            $this->tableSeo,
            'id',
            'CASCADE',
            'CASCADE'
        );

        //change attribute
        $this->alterColumn($this->tableSeo, 'model_id', $this->integer(11)->notNull()->unsigned()->comment('Model ID'));
    }

    public function down()
    {
        $this->dropForeignKey('fk-seo_lang-rid-seo-id', $this->tableSeoLang);
        $this->truncateTable($this->tableSeoLang);
        $this->truncateTable($this->tableSeo);
        $this->addForeignKey(
            'fk-seo_lang-rid-seo-id',
            $this->tableSeoLang,
            'rid',
            $this->tableSeo,
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->alterColumn($this->tableSeo, 'model_id', $this->integer(11)->notNull()->defaultValue(0)->comment('Model ID'));
    }
}
