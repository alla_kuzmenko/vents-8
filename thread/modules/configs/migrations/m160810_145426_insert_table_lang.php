<?php

use yii\db\Migration;

class m160810_145426_insert_table_lang extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('fv_languages', 'default', $this->integer()->defaultValue(null)->comment('default'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('fv_languages', 'default');
    }
  
}
