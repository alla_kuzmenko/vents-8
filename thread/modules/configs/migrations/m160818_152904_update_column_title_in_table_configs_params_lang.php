<?php

use yii\db\Migration;

class m160818_152904_update_column_title_in_table_configs_params_lang extends Migration
{
    public $tableConfigsParams = '{{%configs_params}}';
    public $tableConfigsParamsLang = '{{%configs_params_lang}}';

    public function up()
    {
        //change attribute
        $this->alterColumn($this->tableConfigsParamsLang, 'title', $this->text()->defaultValue(null)->comment('Title'));
    }

    public function down()
    {
        $this->alterColumn($this->tableConfigsParamsLang, 'title',  $this->string(255)->notNull()->comment('Title'));
    }
}
