<?php

use yii\db\Migration;

class m161019_134446_add_column_only_editor_table_language extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute('ALTER TABLE `fv_languages` ADD `only_editor` ENUM(\'0\',\'1\') NOT NULL DEFAULT \'0\' COMMENT \'only_editor\' AFTER `default`, ADD INDEX (`only_editor`);');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('fv_languages', 'only_editor');
    }
}
