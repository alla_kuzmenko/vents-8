<?php

namespace thread\modules\configs\models;

use Yii;
//
use thread\app\base\models\{
    ActiveRecord, query\ActiveQuery
};

use thread\modules\configs\Configs as ConfigsModule;


/**
 * Class Language
 *
 * @property integer $id
 * @property string $alias
 * @property string $local
 * @property string $label
 * @property string $img_flag
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published
 * @property integer $deleted
 *
 * @package thread\modules\configs\models
 * @author FilamentV <vortex.filament@gmail.com>
 * @author Alla Kuzmenko
 * @copyright (c) 2016, VipDesign
 */
class Language extends ActiveRecord implements \thread\app\model\interfaces\LanguageModel
{
    /** @var null Текущая модель языка */
    private static $_currentLangModel = null;

    /**
     * @return string
     */
    public static function getDb()
    {
        return ConfigsModule::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%languages}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['alias', 'local', 'label'], 'string', 'max' => 50],
            [['img_flag'], 'string', 'max' => 225],
            [['alias', 'local', 'label'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['published', 'deleted', 'default', 'only_editor'], 'in', 'range' => array_keys(static::statusKeyRange())],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'only_editor' => ['only_editor'],
            'backend' => ['published', 'deleted', 'alias', 'local', 'label', 'img_flag', 'default', 'only_editor'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Create time'),
            'updated_at' => Yii::t('app', 'Update time'),
            'published' => Yii::t('app', 'Published'),
            'deleted' => Yii::t('app', 'Deleted'),
            'alias' => Yii::t('app', 'Alias'),
            'local' => Yii::t('app', 'Local'),
            'label' => Yii::t('app', 'Label'),
            'img_flag' => Yii::t('app', 'Img_flag'),
            'default' => Yii::t('app', 'Default'),
            'only_editor' => Yii::t('app', 'only_editor'),
        ];
    }

    /**
     * @return mixed
     */
    public function getLanguages():array
    {
        return self::findBase()->asArray()->all();
    }

    /**
     * @return mixed
     */
    public static function getCurrent():array
    {
        return self::findBase()->andWhere(['local' => \Yii::$app->language])->asArray()->one();
    }

    /**
     * @return string the homepage URL
     */
    public static function getHomeUrl()
    {
        $current = self::getCurrent();

        if (!$current['default']) {
            return Yii::$app->request->hostInfo . '/' . $current['alias'] . '/';
        } else {
            return Yii::$app->request->hostInfo . '/';
        }
    }

    /**
     * Получить текущий язык
     * @return null
     */
    public static function getCurrentModel()
    {
        if (self::$_currentLangModel === null) {
            $self = new self;
            self::$_currentLangModel = $self->getCurrent();
        }

        return self::$_currentLangModel;
    }
}