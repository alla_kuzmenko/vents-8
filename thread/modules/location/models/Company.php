<?php

namespace thread\modules\location\models;

use thread\app\base\models\ActiveRecord;
use Yii;

/**
 * Class Company
 * @package thread\modules\location\models
 */

class Company extends ActiveRecord
{

    /**
     * @inheritdoc
     */

    public static function getDb()
    {
        return \thread\modules\location\Location::getDb();
    }

    /**
     * @return string
     */

    public static function tableName()
    {
        return '{{%location_company}}';
    }

    /**
     * @return array
     */

    public function rules()
    {
        return [
            [['region_id', 'country_id', 'city_id', 'lat', 'lng'], 'required'],
            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())],
            [['type_marker'], 'in', 'range' => array_keys(static::typeMarkerRange())],
            //typeMarkerRange
            [['region_id', 'country_id', 'city_id', 'created_at', 'updated_at'], 'integer'],
            [['default_title', 'link_site', 'link_shop'], 'string', 'max' => 255],
            [['lat', 'lng'],  'double'],
            [['lat', 'lng'],  'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'default_title' => Yii::t('app', 'Default title'),
            'region_id' => Yii::t('app', 'Region'),
            'country_id' => Yii::t('app', 'Country'),
            'city_id' => Yii::t('app', 'City'),
            'lat' => Yii::t('app', 'Latitude'),
            'lng' => Yii::t('app', 'Longitude'),
            'link_site' => Yii::t('app', 'Link site'),
            'link_shop' => Yii::t('app', 'Link shop'),
            'updated_at' => Yii::t('app', 'updated_at'),
            'published' => Yii::t('app', 'published'),
            'deleted' => Yii::t('app', 'deleted'),
            'type_marker' => Yii::t('app', 'type_marker'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'backend' => [
                'default_title',
                'region_id',
                'country_id',
                'city_id',
                'lat',
                'lng',
                'link_site',
                'link_shop',
                'type_marker',
                'created_at',
                'updated_at',
                'published',
                'deleted'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(CompanyLang::class, ['rid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return array
     */
    public static function typeMarkerRange()
    {
        return [
            'white' => Yii::t('app', 'white'),
            'blue' => Yii::t('app', 'blue')
        ];
    }

}
