<?php

namespace thread\modules\location\models;

use thread\app\base\models\ActiveRecordLang;
use Yii;

/**
 * Class RegionLang
 *
 * @package thread\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class RegionLang extends ActiveRecordLang
{

    /**
     *
     * @return string
     */
    public static function getDb()
    {
        return \thread\modules\location\Location::getDb();
    }

    /**
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%location_region_lang}}';
    }


    /**
     * Rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['rid'], 'required'],
            ['rid', 'integer'],
            ['rid', 'exist', 'targetClass' => City::class, 'targetAttribute' => 'id'],
            ['lang', 'string', 'min' => 5, 'max' => 5],
            ['title', 'string', 'max' => 255]
        ];
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'rid' => Yii::t('app', 'rid'),
            'lang' => Yii::t('app', 'Lang'),
            'title' => Yii::t('app', 'title'),
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'backend' => ['title'],
        ];
    }

}
