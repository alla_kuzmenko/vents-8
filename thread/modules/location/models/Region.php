<?php

namespace thread\modules\location\models;

use thread\app\base\models\ActiveRecord;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class Region
 *
 * @package thread\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Region extends ActiveRecord
{

    /**
     * @inheritdoc
     */

    public static function getDb()
    {
        return \thread\modules\location\Location::getDb();
    }

    /**
     * @return string
     */

    public static function tableName()
    {
        return '{{%location_region}}';
    }


    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => AttributeBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => 'alias',
                        ActiveRecord::EVENT_BEFORE_UPDATE => 'alias',
                    ],
                    'value' => function ($event) {
                        return Inflector::slug($this->alias);
                    },
                ],
            ]
        );
    }

    /**
     * @return array
     */

    public function rules()
    {
        return [
            [['alias'], 'required'],
            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())],
            [['created_at', 'updated_at'], 'integer'],
            [['alias', 'default_title'], 'string', 'max' => 128],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'alias' => Yii::t('app', 'alias'),
            'default_title' => Yii::t('app', 'Default title'),
            'created_at' => Yii::t('app', 'created_at'),
            'updated_at' => Yii::t('app', 'updated_at'),
            'published' => Yii::t('app', 'published'),
            'deleted' => Yii::t('app', 'deleted'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'backend' => [
                'alias',
                'default_title',
                'created_at',
                'updated_at',
                'published',
                'deleted',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(RegionLang::class, ['rid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountrys()
    {
        return $this->hasMany(Country::class, ['region_id' => 'id']);
    }

}
