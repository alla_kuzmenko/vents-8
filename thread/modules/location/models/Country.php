<?php

namespace thread\modules\location\models;

use thread\app\base\models\ActiveRecord;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\helpers\ArrayHelper;
use thread\behaviors\TransliterateBehavior;
use yii\helpers\Inflector;

/**
 * Class Country
 *
 * @package thread\modules\location\models
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
class Country extends ActiveRecord
{

    /**
     *
     * @return string
     */
    public static function getDb()
    {
        return \thread\modules\location\Location::getDb();
    }

    /**
     * Table name
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%location_country}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => AttributeBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => 'alias',
                        ActiveRecord::EVENT_BEFORE_UPDATE => 'alias',
                    ],
                    'value' => function ($event) {
                        return Inflector::slug($this->alias);
                    },
                ],
            ]
        );
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['alias'], 'required'],
            [['zoom', 'region_id', 'created_at', 'updated_at',],'integer'],
            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())],
            [['position'], 'default', 'value' => 0],
            [['zoom'], 'default', 'value' => 5],
            [['alias', 'default_title'], 'string', 'max' => 128],
            [['alpha2'], 'string', 'min' => 2, 'max' => 2],
            [['alpha3'], 'string', 'min' => 3, 'max' => 3],
            [['alias'], 'unique'],
        ];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'backend' => [
                'region_id',
                'alias',
                'default_title',
                'alpha2',
                'alpha3',
                'iso',
                'zoom',
                'published',
                'deleted',
            ],
        ];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'region_id' => Yii::t('app', 'Region'),
            'alias' => Yii::t('app', 'alias'),
            'default_title' => Yii::t('app', 'Default title'),
            'alpha2' => Yii::t('app', 'alpha2'),
            'alpha3' => Yii::t('app', 'alpha3'),
            'iso' => Yii::t('app', 'iso'),
            'zoom' => Yii::t('app', 'Zoom'),
            'created_at' => Yii::t('app', 'created_at'),
            'updated_at' => Yii::t('app', 'updated_at'),
            'published' => Yii::t('app', 'published'),
            'deleted' => Yii::t('app', 'deleted'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(CountryLang::class, ['rid' => 'id'])->orderBy(['title' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['location_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanys()
    {
        return $this->hasMany(Company::class, ['country_id' => 'id']);
    }
}
