<?php

use yii\db\Migration;

class m160727_112633_add_row_region_to_table_country extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_country', 'region_id', 'int(11) unsigned NOT NULL COMMENT \'region_id\' AFTER id');
    }

    public function down()
    {
        $this->dropColumn('fv_location_country', 'region_id');
    }
}
