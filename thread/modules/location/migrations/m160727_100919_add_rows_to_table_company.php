<?php

use yii\db\Migration;

class m160727_100919_add_rows_to_table_company extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_company', 'region_id', 'int(11) unsigned NOT NULL COMMENT \'region_id\' AFTER id');
        $this->addColumn('fv_location_company', 'lat', 'double(12) DEFAULT 0 COMMENT \'Latitude\' AFTER city_id');
        $this->addColumn('fv_location_company', 'lng', 'double(12) DEFAULT 0 COMMENT \'Longitude\' AFTER lat');
    }

    public function down()
    {
        $this->dropColumn('fv_location_company', 'region_id');
        $this->dropColumn('fv_location_company', 'lat');
        $this->dropColumn('fv_location_company', 'lng');
    }
}
