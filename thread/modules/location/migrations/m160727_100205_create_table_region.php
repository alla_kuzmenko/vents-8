<?php

use yii\db\Migration;

/**
 * Class m160727_100205_create_table_region
 *
 * @package thread\modules\location
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class m160727_100205_create_table_region extends Migration
{
    /**
     * Region table name
     * @var string
     */
    public $tableRegion = '{{%location_region}}';

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableRegion,
            [
                'id' => $this->primaryKey()->unsigned()->comment('ID'),
                'alias' => $this->string(128)->notNull()->unique()->comment('alias'),
                'created_at' => $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('Create time'),
                'updated_at' => $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('Update time'),
                'published' => $this->boolean()->notNull()->defaultValue(0)->comment('Published'),
                'deleted' => $this->boolean()->notNull()->defaultValue(0)->comment('Deleted')
            ]
        );

        $this->createIndex('published', $this->tableRegion, 'published');
        $this->createIndex('deleted', $this->tableRegion, 'deleted');
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropIndex('deleted', $this->tableRegion);
        $this->dropIndex('published', $this->tableRegion);
        $this->dropTable($this->tableRegion);
    }
}
