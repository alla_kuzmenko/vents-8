<?php

use yii\db\Migration;

class m160809_112055_add_column_to_company_table extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_company', 'type_marker', 'ENUM(\'white\', \'blue\') NOT NULL DEFAULT \'white\'');

    }

    public function down()
    {
        $this->dropColumn('fv_location_company', 'type_marker');

    }
}
