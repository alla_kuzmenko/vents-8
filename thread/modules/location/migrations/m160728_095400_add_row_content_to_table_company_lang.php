<?php

use yii\db\Migration;

class m160728_095400_add_row_content_to_table_company_lang extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_company_lang', 'content', 'text COMMENT \'content\' AFTER house');
    }

    public function down()
    {
        $this->dropColumn('fv_location_company_lang', 'content');
    }
}
