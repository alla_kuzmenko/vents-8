<?php

use yii\db\Migration;

class m160810_145009_add_default_title_to_table_location_country extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_country', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER alias');
    }

    public function down()
    {
        $this->dropColumn('fv_location_country', 'default_title');
    }
}
