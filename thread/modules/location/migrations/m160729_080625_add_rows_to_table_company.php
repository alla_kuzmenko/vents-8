<?php

use yii\db\Migration;

class m160729_080625_add_rows_to_table_company extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_company', 'link_site', 'varchar(255) DEFAULT NULL COMMENT \'link_site\' AFTER lng');
        $this->addColumn('fv_location_company', 'link_shop', 'varchar(255) DEFAULT NULL COMMENT \'link_shop\' AFTER link_site');
    }

    public function down()
    {
        $this->dropColumn('fv_location_company', 'link_site');
        $this->dropColumn('fv_location_company', 'link_shop');
    }
}
