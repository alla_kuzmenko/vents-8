<?php

use yii\db\Migration;

class m160818_144659_add_row_zoom_to_table_country extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_country', 'zoom', 'tinyint(2) UNSIGNED NOT NULL DEFAULT \'5\' COMMENT \'zoom\' AFTER iso');
    }

    public function down()
    {
        $this->dropColumn('fv_location_country', 'zoom');
    }
}
