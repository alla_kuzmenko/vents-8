<?php

use yii\db\Migration;

class m160810_144904_add_default_title_to_table_location_region extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_region', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER alias');
    }

    public function down()
    {
        $this->dropColumn('fv_location_region', 'default_title');
    }
}
