<?php

use yii\db\Migration;

class m160810_145128_add_default_title_to_table_location_company extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_company', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER id');
    }

    public function down()
    {
        $this->dropColumn('fv_location_company', 'default_title');
    }
}
