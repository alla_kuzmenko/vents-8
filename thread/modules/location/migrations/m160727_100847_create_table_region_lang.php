<?php

use yii\db\Migration;

/**
 * Class m160727_100847_create_table_region_lang
 *
 * @package thread\modules\location
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class m160727_100847_create_table_region_lang extends Migration
{
    /**
     * Related Region table name
     *
     * @var string
     */
    public $tableRegion = '{{%location_region}}';

    /**
     * Language Region table name
     *
     * @var string
     */
    public $tableRegionLang = '{{%location_region_lang}}';

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tableRegionLang, [
            'rid' => $this->integer()->unsigned()->notNull()->comment('Related model ID'),
            'lang' => $this->string(5)->notNull()->comment('Language'),
            'title' => $this->string(255)->notNull()->comment('Title')
        ]);

        $this->createIndex('rid', $this->tableRegionLang, ['rid', 'lang'], true);

        $this->addForeignKey(
            'fk-location_region_lang-rid-location_region-id',
            $this->tableRegionLang,
            'rid',
            $this->tableRegion,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-location_region_lang-rid-location_region-id', $this->tableRegionLang);
        $this->dropIndex('rid', $this->tableRegionLang);
        $this->dropTable($this->tableRegionLang);
    }
}
