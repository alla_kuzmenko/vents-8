<?php

use yii\db\Migration;

class m160811_083738_delete_title_from_table_location_country extends Migration
{
    public function up()
    {
        $this->dropColumn('fv_location_country', 'title');
    }

    public function down()
    {
        $this->addColumn('fv_location_country', 'title', 'varchar(128) NULL COMMENT \'default_title\' AFTER default_title');
    }
}
