<?php

use yii\db\Migration;

class m160810_145112_add_default_title_to_table_location_city extends Migration
{
    public function up()
    {
        $this->addColumn('fv_location_city', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER alias');
    }

    public function down()
    {
        $this->dropColumn('fv_location_city', 'default_title');
    }
}
