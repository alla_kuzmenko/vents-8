<?php

use yii\db\Migration;

class m161028_130448_add_column_redirects_to_article extends Migration
{

    /**
     * @var string
     */
    public $tableNewsArticle = '{{%news_article}}';

    public function safeUp()
    {
        $this->addColumn($this->tableNewsArticle, 'redirects', $this->text()->defaultValue(null)->comment('redirects'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableNewsArticle, 'redirects');
    }
}
