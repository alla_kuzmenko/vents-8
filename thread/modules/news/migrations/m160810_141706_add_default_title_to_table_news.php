<?php

use yii\db\Migration;

class m160810_141706_add_default_title_to_table_news extends Migration
{
    public function up()
    {
        $this->addColumn('fv_news_article', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER alias');
    }

    public function down()
    {
        $this->dropColumn('fv_news_article', 'default_title');
    }
}
