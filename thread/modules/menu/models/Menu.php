<?php

namespace thread\modules\menu\models;

use thread\app\base\models\ActiveRecord;
use thread\app\base\models\query\ActiveQuery;
use thread\modules\menu\Menu as MenuModule;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class Menu
 *
 * @property integer $id
 * @property string $alias
 * @property string $default_title
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published
 * @property integer $deleted
 * @property integer $readonly
 *
 * @property MenuLang $lang
 *
 * @package thread\modules\menu\models
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
class Menu extends ActiveRecord
{
    /**
     * @return string
     */
    public static function getDb()
    {
        return MenuModule::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%menu}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'alias',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'alias',
                ],
                'value' => function ($event) {
                    return Inflector::slug($this->alias);
                },
            ],
        ]);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['alias'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['published', 'deleted', 'readonly'], 'in', 'range' => array_keys(static::statusKeyRange())],
            [['alias', 'default_title'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'backend' => ['alias', 'default_title', 'published', 'deleted', 'readonly'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'alias' => Yii::t('app', 'Alias'),
            'default_title' => Yii::t('app', 'Default title'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'published' => Yii::t('app', 'Published'),
            'deleted' => Yii::t('app', 'Deleted'),
            'readonly' => Yii::t('app', 'Readonly'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(MenuLang::class, ['rid' => 'id'])
            ->andOnCondition(['lang' => \Yii::$app->language]);
    }

    /**
     * @return ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(MenuItem::class, ['group_id' => 'id'])->undeleted();
    }
}
