<?php

namespace thread\modules\menu\models;

use thread\app\base\models\ActiveRecordLang;
use thread\app\base\models\query\ActiveQuery;
use thread\app\model\Language;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class MenuLang
 *
 * @property integer $rid
 * @property string $lang
 * @property string $title
 *
 * @package thread\modules\menu\models
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
class MenuLang extends ActiveRecordLang
{

    /**
     * @return string
     */
    public static function getDb()
    {
        return \thread\modules\menu\Menu::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%menu_lang}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['title'], 'required'],
                ['rid', 'exist', 'targetClass' => MenuItem::class, 'targetAttribute' => 'id'],
                ['title', 'string', 'max' => 255],
            ]
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'rid' => Yii::t('app', 'rid'),
            'lang' => Yii::t('app', 'Lang'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return [
            'backend' => ['title'],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Language::class, ['lang' => 'id']);
    }
}
