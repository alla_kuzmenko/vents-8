<?php

use yii\db\Migration;

class m160810_133845_add_default_title_to_table_menuitem extends Migration
{
    public function up()
    {
        $this->addColumn('fv_menu_item', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER id');
    }

    public function down()
    {
        $this->dropColumn('fv_menu_item', 'default_title');
    }
}
