<?php

use yii\db\Migration;

class m160810_133824_add_default_title_to_table_menu extends Migration
{
    public function up()
    {
        $this->addColumn('fv_menu', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER alias');
    }

    public function down()
    {
        $this->dropColumn('fv_menu', 'default_title');
    }
}
