<?php

use thread\modules\contacts\Contacts;
use yii\db\Migration;

class m160809_143449_create_table_contacts_lang extends Migration
{
    /**
     * @var string
     */
    public $tableFeedbackTopics = '{{%contacts}}';

    /**
     * @var string
     */
    public $tableFeedbackTopicsLang = '{{%contacts_lang}}';

    public function init()
    {
        $this->db = Contacts::getDb();
        parent::init();
    }

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tableFeedbackTopicsLang, [
            'rid' => $this->integer()->unsigned()->notNull()->comment('Related model ID'),
            'lang' => $this->string(5)->notNull()->comment('Language'),
            'image' => $this->string(255)->comment('image'),
            'delivery_phone' => $this->string(255)->comment('delivery_phone'),
            'export_phone' => $this->string(255)->comment('export_phone'),
            'customer_phone' => $this->string(255)->comment('customer_phone'),
            'customer_info' => $this->string(255)->comment('customer_info'),
            'hr_phone' => $this->string(255)->comment('hr_phone'),
            'address' => $this->string(255)->comment('address'),
        ]);

        $this->createIndex('rid', $this->tableFeedbackTopicsLang, ['rid', 'lang'], true);

        $this->addForeignKey(
            'fk-contacts_lang-rid-contacts-id',
            $this->tableFeedbackTopicsLang,
            'rid',
            $this->tableFeedbackTopics,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-contacts_lang-rid-contacts-id', $this->tableFeedbackTopicsLang);
        $this->dropIndex('rid', $this->tableFeedbackTopicsLang);
        $this->dropTable($this->tableFeedbackTopicsLang);
    }
}
