<?php

use thread\modules\contacts\Contacts;
use yii\db\Migration;

class m160809_143437_create_table_contacts extends Migration
{
    /**
     * @var string
     */
    public $tableFeedbackTopics = '{{%contacts}}';

    public function init()
    {
        $this->db = Contacts::getDb();
        parent::init();
    }

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tableFeedbackTopics, [
            'id' => $this->primaryKey()->unsigned()->comment('ID'),
            'created_at' => $this->integer(10)->notNull()->defaultValue(0)->comment('Create time'),
            'updated_at' => $this->integer(10)->notNull()->defaultValue(0)->comment('Update time'),
            'published' => $this->boolean()->notNull()->defaultValue(0)->comment('Published'),
            'deleted' => $this->boolean()->notNull()->defaultValue(0)->comment('Deleted'),
        ]);

        $this->createIndex('published', $this->tableFeedbackTopics, 'published');
        $this->createIndex('deleted', $this->tableFeedbackTopics, 'deleted');
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropIndex('deleted', $this->tableFeedbackTopics);
        $this->dropIndex('published', $this->tableFeedbackTopics);
        $this->dropTable($this->tableFeedbackTopics);
    }
}
