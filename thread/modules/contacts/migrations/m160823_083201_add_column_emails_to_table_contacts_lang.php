<?php

use yii\db\Migration;

class m160823_083201_add_column_emails_to_table_contacts_lang extends Migration
{
    public $tableContactsLang = '{{%contacts_lang}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn($this->tableContactsLang, 'emails', $this->string(225)->defaultValue(null)->comment('emails'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->tableContactsLang, 'emails');
    }
}
