<?php

namespace thread\modules\contacts\models;

use Yii;
use yii\helpers\ArrayHelper;

use thread\modules\contacts\Contacts as ContactsModule;
use thread\app\base\models\ActiveRecordLang;

/**
 * Class ContactLang
 *
 * @property integer $rid
 * @property string $lang
 * @property string $image
 * @property string $delivery_phone
 * @property string $export_phone
 * @property string $customer_phone
 * @property string $customer_info
 * @property string $hr_phone
 * @property string $address
 * @property string emails

 * @package thread\modules\contacts\models
 * @author Alla Kuzmenko
 * @copyright (c) 2016
 */
class ContactLang extends ActiveRecordLang
{
    /**
     * @return string
     */
    public static function getDb()
    {
        return ContactsModule::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%contacts_lang}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['image'], 'required'],
            ['rid', 'exist', 'targetClass' => Contact::class, 'targetAttribute' => 'id'],
            [['image', 'delivery_phone', 'export_phone', 'customer_phone', 'customer_info', 'hr_phone', 'address', 'emails'], 'string', 'max' => 255],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'rid' => Yii::t('app', 'RID'),
            'lang' => Yii::t('app', 'Lang'),
            'image' => Yii::t('app', 'Image'),
            'delivery_phone' => Yii::t('app', 'Delivery_phone'),
            'export_phone' => Yii::t('app', 'Export_phone'),
            'customer_phone' => Yii::t('app', 'Customer_phone'),
            'customer_info' => Yii::t('app', 'Customer_info'),
            'hr_phone' => Yii::t('app', 'Hr_phone'),
            'address' => Yii::t('app', 'address'),
            'emails' => Yii::t('app', 'emails'),
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'backend' => ['image', 'delivery_phone', 'export_phone', 'customer_phone', 'customer_info', 'hr_phone', 'address', 'emails'],
        ];
    }
    /**
     * @return null|string
     */
    public function getImage()
    {
        /** @var contacts $Module */
        $Module = Yii::$app->getModule('contacts');
        $path = $Module->getUploadPath();
        $url = $Module->getUploadUrl();
        $image = null;
        if (isset($this->image) && file_exists($path . $this->image)) {
            $image = $url . $this->image;
        }
        return $image;
    }
}
