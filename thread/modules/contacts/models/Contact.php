<?php

namespace thread\modules\contacts\models;

use Yii;

use thread\app\base\models\ActiveRecord;
use thread\modules\contacts\Contacts as ContactsModule;

/**
 * Class Contact
 *
 * @property integer id
 * @property integer created_at
 * @property integer updated_at
 * @property boolean published
 * @property boolean deleted
 *
 * @property ContactLang $lang
 *
 * @package thread\modules\contacts\models
 * @author Alla Kuzmenko
 * @copyright (c) 2016
 */
class Contact extends ActiveRecord
{
    /**
     * @return null|object|string
     */
    public static function getDb()
    {
        return ContactsModule::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%contacts}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['create_time', 'update_time'], 'integer'],
            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'backend' => ['published', 'deleted'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Create time'),
            'updated_at' => Yii::t('app', 'Update time'),
            'published' => Yii::t('app', 'Published'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ContactLang::class, ['rid' => 'id']);
    }
}
