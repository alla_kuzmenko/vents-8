<?php

namespace thread\modules\contacts;

use thread\app\base\module\abstracts\Module as aModule;
use Yii;

/**
 * Class Contacts
 *
 * @package thread\modules\contacts
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Contacts extends aModule
{
    public $name = 'contacts';
    public $translationsBasePath = __DIR__ . '/messages';
    public $configPath = __DIR__ . '/config.php';

    public static function getDb()
    {
        return Yii::$app->get('db');
    }
}
