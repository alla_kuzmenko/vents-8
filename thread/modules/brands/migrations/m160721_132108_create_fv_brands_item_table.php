<?php

use yii\db\Migration;
use thread\modules\brands\Brands;

/**
 * Class m160721_132108_create_fv_brands_item_table
 *
 * @package thread\modules\brands
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class m160721_132108_create_fv_brands_item_table extends Migration
{
    /**
     * @var string
     */
    public $tableBrandsItem = '{{%brands_item}}';


    public function init()
    {
        $this->db = Brands::getDb();
        parent::init();
    }

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tableBrandsItem, [
            'id' => $this->primaryKey()->unsigned()->comment('ID'),
            'alias' => $this->string(255)->unique()->notNull()->comment('Alias'),
            'image_link' => $this->string(255)->defaultValue(null)->comment('Image link'),
            'image_link_s' => $this->string(255)->defaultValue(null)->comment('Image link small'),
            'link' => $this->string(255)->defaultValue(null)->comment('Image link'),
            'created_at' => $this->integer(10)->notNull()->defaultValue(0)->comment('Create time'),
            'updated_at' => $this->integer(10)->notNull()->defaultValue(0)->comment('Update time'),
            'published' => $this->boolean()->notNull()->defaultValue(0)->comment('Published'),
            'deleted' => $this->boolean()->notNull()->defaultValue(0)->comment('Deleted'),
        ]);
        $this->createIndex('published', $this->tableBrandsItem, 'published');
        $this->createIndex('deleted', $this->tableBrandsItem, 'deleted');
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropIndex('deleted', $this->tableBrandsItem);
        $this->dropIndex('published', $this->tableBrandsItem);
        $this->dropTable($this->tableBrandsItem);
    }
}
