<?php

use yii\db\Migration;

/**
 * Class m160721_133429_insert_default_brands
 *
 * @package thread\modules\brands
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class m160721_133429_insert_default_brands extends Migration
{
    /**
     * @var string
     */
    public $tableBrandsItem = '{{%brands_item}}';

    /**
     * @var string
     */
    public $tableBrandsItemLang = '{{%brands_item_lang}}';

    public function safeUp()
    {

        /** Insert brands_item */
        $this->batchInsert(
            $this->tableBrandsItem,
            [
                'id',
                'alias',
                'image_link',
                'image_link_s',
                'link',
                'created_at',
                'updated_at',
                'published',
                'deleted',
            ],
            [
                [
                    'id' => 1,
                    'alias' => 'blauberg',
                    'image_link' => NULL,
                    'image_link_s' => NULL,
                    'link' => 'http://blaubergventilatoren.de/',
                    'created_at' => time(),
                    'updated_at' => time(),
                    'published' => 1,
                    'deleted' => 0,
                ],
                [
                    'id' => 2,
                    'alias' => 'zern',
                    'image_link' => NULL,
                    'image_link_s' => NULL,
                    'link' => 'http://blaubergventilatoren.de/',
                    'created_at' => time(),
                    'updated_at' => time(),
                    'published' => 1,
                    'deleted' => 0,
                ],
                [
                    'id' => 3,
                    'alias' => 'vents',
                    'image_link' => NULL,
                    'image_link_s' => NULL,
                    'link' => 'http://blaubergventilatoren.de/',
                    'created_at' => time(),
                    'updated_at' => time(),
                    'published' => 1,
                    'deleted' => 0,
                ],
                [
                    'id' => 4,
                    'alias' => 'orchid',
                    'image_link' => NULL,
                    'image_link_s' => NULL,
                    'link' => 'http://blaubergventilatoren.de/',
                    'created_at' => time(),
                    'updated_at' => time(),
                    'published' => 1,
                    'deleted' => 0,
                ],
                [
                    'id' => 5,
                    'alias' => 'bl-motor',
                    'image_link' => NULL,
                    'image_link_s' => NULL,
                    'link' => 'http://blaubergventilatoren.de/',
                    'created_at' => time(),
                    'updated_at' => time(),
                    'published' => 1,
                    'deleted' => 0,
                ],
            ]
        );

        /** Insert brands_item_lang */
        $this->batchInsert(
            $this->tableBrandsItemLang,
            [
                'rid',
                'lang',
                'title',
                'description',
                'content',
            ],
            [
                [
                    'rid' => 1,
                    'lang' => 'en-EN',
                    'title' => 'blauberg',
                    'description' => 'The wide assortment of the ventilating equipment in which innovative technologies, modern design and the German quality are optimum combined.',
                    'content' => 'The wide assortment of the ventilating equipment in which innovative technologies, modern design and the German quality are optimum combined.'
                ],
                [
                    'rid' => 2,
                    'lang' => 'en-EN',
                    'title' => 'zern',
                    'description' => 'Offers the market recuperators lamellar counterflow and cross current, and also the entalpiyny and rotor regenerators differing in the increased efficiency.',
                    'content' => 'Offers the market recuperators lamellar counterflow and cross current, and also the entalpiyny and rotor regenerators differing in the increased efficiency.'
                ],
                [
                    'rid' => 3,
                    'lang' => 'en-EN',
                    'title' => 'vents',
                    'description' => 'Vents makes a full range of the equipment for systems of ventilation and conditioning of any level of complexity.',
                    'content' => 'Vents makes a full range of the equipment for systems of ventilation and conditioning of any level of complexity.'
                ],
                [
                    'rid' => 4,
                    'lang' => 'en-EN',
                    'title' => 'orchid',
                    'description' => 'Black Orchid is a horticultural lifestyle brand offering customers an extensive range of high quality, innovative and reliable products for all their indoor gardening needs.',
                    'content' => 'Black Orchid is a horticultural lifestyle brand offering customers an extensive range of high quality, innovative and reliable products for all their indoor gardening needs.'
                ],
                [
                    'rid' => 5,
                    'lang' => 'en-EN',
                    'title' => 'bl-motor',
                    'description' => 'Represents the most modern AC, DC and power effective EC motors. The wide range allows to choose motors for the most various configurations of ventilations.',
                    'content' => 'Represents the most modern AC, DC and power effective EC motors. The wide range allows to choose motors for the most various configurations of ventilations.'
                ],
            ]
        );
    }

    public function safeDown()
    {
        $this->truncateTable($this->tableBrandsItemLang);
        $this->dropForeignKey('fk-brands_item_lang-rid-brands_item-id', $this->tableBrandsItemLang);
        $this->truncateTable($this->tableBrandsItem);
        $this->addForeignKey(
            'fk-brands_item_lang-rid-brands_item-id',
            $this->tableBrandsItemLang,
            'rid',
            $this->tableBrandsItem,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
