<?php

use yii\db\Migration;

class m160810_141211_add_default_title_to_table_brands extends Migration
{
    public function up()
    {
        $this->addColumn('fv_brands_item', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER alias');
    }

    public function down()
    {
        $this->dropColumn('fv_brands_item', 'default_title');
    }
}
