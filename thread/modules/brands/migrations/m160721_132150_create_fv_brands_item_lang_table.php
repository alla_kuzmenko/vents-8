<?php

use yii\db\Migration;
use thread\modules\brands\Brands;

/**
 * Class m160721_132150_create_fv_brands_item_lang_table
 *
 * @package thread\modules\brands
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class m160721_132150_create_fv_brands_item_lang_table extends Migration
{
    /**
     * @var string
     */
    public $tableBrandsItem = '{{%brands_item}}';

    /**
     * @var string
     */
    public $tableBrandsItemLang = '{{%brands_item_lang}}';

    public function init()
    {
        $this->db = Brands::getDb();
        parent::init();
    }

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tableBrandsItemLang, [
            'rid' => $this->integer(11)->unsigned()->notNull()->comment('Related model ID'),
            'lang' => $this->string(5)->notNull()->comment('Language'),
            'title' => $this->string(255)->notNull()->comment('Title'),
            'description' => $this->string(255)->defaultValue(null)->comment('Description'),
            'content' => $this->text()->defaultValue(null)->comment('Content'),
        ]);
        $this->createIndex('rid', $this->tableBrandsItemLang, ['rid', 'lang'], true);
        $this->addForeignKey(
            'fk-brands_item_lang-rid-brands_item-id',
            $this->tableBrandsItemLang,
            'rid',
            $this->tableBrandsItem,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-brands_item_lang-rid-brands_item-id', $this->tableBrandsItemLang);
        $this->dropIndex('rid', $this->tableBrandsItemLang);
        $this->dropTable($this->tableBrandsItemLang);
    }
}
