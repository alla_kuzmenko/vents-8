<?php

use thread\modules\brands\Brands;
use yii\db\Migration;

class m160725_142358_add_columns_to_brand_items_table extends Migration
{
    /**
     * @var string
     */
    public $tableBrandsItem = '{{%brands_item}}';


    public function init()
    {
        $this->db = Brands::getDb();
        parent::init();
    }

    public function safeUp()
    {
        $this->addColumn($this->tableBrandsItem, 'on_index', $this->boolean()->notNull()->defaultValue(0)->comment('Show on index'));
        $this->addColumn($this->tableBrandsItem, 'position', $this->integer(11)->notNull()->defaultValue(0)->comment('Position'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableBrandsItem, 'on_index');
        $this->dropColumn($this->tableBrandsItem, 'position');
    }
}
