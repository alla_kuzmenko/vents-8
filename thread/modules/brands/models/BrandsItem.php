<?php

namespace thread\modules\brands\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\behaviors\AttributeBehavior;
//
use thread\app\base\models\ActiveRecord;
use thread\modules\brands\Brands as BrandsModule;

/**
 * Class BrandsItem
 *
 * @property integer id
 * @property string alias
 * @property string $default_title
 * @property string image_link
 * @property string link
 * @property integer created_at
 * @property integer updated_at
 * @property boolean published
 * @property boolean deleted
 * @property integer $position
 * @property boolean $on_index
 *
 * @property BrandsItemLang $lang
 *
 * @package thread\modules\brands\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class BrandsItem extends ActiveRecord
{
    /**
     * @return null|object|string
     */
    public static function getDb()
    {
        return BrandsModule::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%brands_item}}';
    }

    /**
     * @return null|string
     */
    public function getArticleImage()
    {
        /** @var News $brandsModule */
        $brandsModule = Yii::$app->getModule('brands');
        $path = $brandsModule->getUploadPath();
        $url = $brandsModule->getUploadUrl();
        $image = null;
        if (isset($this->image_link) && file_exists($path . $this->image_link)) {
            $image = $url . $this->image_link;
        }
        return $image;
    }

    /**
     * @return null|string
     */
    public function getArticleImageSmall()
    {
        /** @var News $brandsModule */
        $brandsModule = Yii::$app->getModule('brands');
        $path = $brandsModule->getUploadPath();
        $url = $brandsModule->getUploadUrl();
        $image = null;
        if (isset($this->image_link_s) && file_exists($path . $this->image_link_s)) {
            $image = $url . $this->image_link_s;
        }
        return $image;
    }

    /**
     * @var
     */
    public static $commonQuery = query\ActiveQuery::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'alias',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'alias',
                ],
                'value' => function ($event) {
                    return Inflector::slug($this->alias);
                },
            ],
        ]);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['alias'], 'required'],
            [['create_time', 'update_time', 'position'], 'integer'],
            [['published', 'deleted', 'on_index'], 'in', 'range' => array_keys(static::statusKeyRange())],
            [['alias', 'default_title', 'image_link', 'image_link_s', 'link'], 'string', 'max' => 255],
            [['alias'], 'unique']
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'on_index' => ['on_index'],
            'backend' => ['alias', 'default_title', 'on_index', 'position', 'image_link', 'image_link_s', 'link', 'published', 'deleted', 'published_time'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'alias' => Yii::t('app', 'Alias'),
            'default_title' => Yii::t('app', 'Default title'),
            'image_link' => Yii::t('app', 'Image link'),
            'image_link_s' => Yii::t('app', 'Image link small'),
            'link' => Yii::t('app', 'Link'),
            'created_at' => Yii::t('app', 'Create time'),
            'updated_at' => Yii::t('app', 'Update time'),
            'published' => Yii::t('app', 'Published'),
            'deleted' => Yii::t('app', 'Deleted'),
            'position' => Yii::t('app', 'Position'),
            'on_index' => Yii::t('app', 'Show on index page')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(BrandsItemLang::class, ['rid' => 'id']);
    }
}
