<?php

namespace thread\modules\brands\models\query;

use thread\app\base\models\ActiveRecord;
use thread\app\base\models\query\ActiveQuery as AQuery;

/**
 * class ActiveQuery
 *
 * @package thread\modules\brands\models\query
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
class ActiveQuery extends AQuery
{

    /**
     * @return $this
     */
    public function on_index()
    {
        /** @var ActiveRecord $modelClass */
        $modelClass = $this->modelClass;
        $this->andWhere($modelClass::tableName() . '.on_index = :on_index', [':on_index' => ActiveRecord::STATUS_KEY_ON]);
        return $this;
    }

}
