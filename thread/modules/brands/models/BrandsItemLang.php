<?php

namespace thread\modules\brands\models;

use Yii;
use yii\helpers\ArrayHelper;
//
use thread\modules\brands\Brands as BrandsModule;
use thread\app\base\models\ActiveRecordLang;

/**
 * Class BrandsItemLang
 *
 * @property integer $rid
 * @property string $lang
 * @property string $title
 * @property string $description
 * @property string $content
 *
 * @package thread\modules\brands\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class BrandsItemLang extends ActiveRecordLang
{
    /**
     * @return string
     */
    public static function getDb()
    {
        return BrandsModule::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%brands_item_lang}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['title'], 'required'],
            ['rid', 'exist', 'targetClass' => BrandsItem::class, 'targetAttribute' => 'id'],
            ['title', 'string', 'max' => 255],
            ['content', 'string'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'rid' => Yii::t('app', 'RID'),
            'lang' => Yii::t('app', 'Lang'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'backend' => ['title', 'description', 'content'],
        ];
    }
}
