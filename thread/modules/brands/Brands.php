<?php

namespace thread\modules\brands;

use thread\app\base\module\abstracts\Module as aModule;
use Yii;

/**
 * Class Brands
 *
 * @package thread\modules\brands
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Brands extends aModule
{
    public $name = 'brands';
    public $translationsBasePath = __DIR__ . '/messages';
    public $configPath = __DIR__ . '/config.php';

    public static function getDb()
    {
        return Yii::$app->get('db');
    }
}
