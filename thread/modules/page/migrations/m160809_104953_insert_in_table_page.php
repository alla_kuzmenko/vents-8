<?php

use yii\db\Migration;

class m160809_104953_insert_in_table_page extends Migration
{
    /**
     * Page table name
     * @var string
     */
    public $tablePage = '{{%page}}';

    /**
     * Language Page table name
     * @var string
     */
    public $tablePageLang = '{{%page_lang}}';


    public function safeUp()
    {
        /** Insert page */
        $this->batchInsert(
            $this->tablePage,
            [
                'id',
                'alias',
                'image_link',
                'created_at',
                'updated_at',
                'published',
                'deleted'
            ],
            [
                [
                    'id' => 777,
                    'alias' => 'basestart',
                    'image_link' => NULL,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'published' => 1,
                    'deleted' => 0,
                ],

            ]
        );

        /** Insert page_lang */
        $this->batchInsert(
            $this->tablePageLang,
            [
                'rid',
                'lang',
                'title',

            ],
            [
                [
                    'rid' => 777,
                    'lang' => 'en-EN',
                    'title' => 'Map',

                ],
                [
                    'rid' => 777,
                    'lang' => 'ru-RU',
                    'title' => 'Карта',

                ],
                [
                    'rid' => 777,
                    'lang' => 'uk-UK',
                    'title' => 'Карта',

                ],


            ]
        );
    }
    public function safeDown()
    {
        $this->truncateTable($this->tablePageLang);
        $this->dropForeignKey('fk-page_lang-rid-page-id', $this->tablePageLang);

        $this->truncateTable($this->tablePage);

        $this->addForeignKey(
            'fk-page_lang-rid-page-id',
            $this->tablePageLang,
            'rid',
            $this->tablePage,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
