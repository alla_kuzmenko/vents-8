<?php

use yii\db\Migration;

class m160808_121836_create_in_numbers_widget_lang extends Migration
{
    /**
     * @var string
     */
    public $tableName = '{{%in_numbers_widget}}';

    /**
     * @var string
     */
    public $tableLangName = '{{%in_numbers_widget_lang}}';

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tableLangName, [
            'rid' => $this->integer()->unsigned()->notNull()->comment('Related model ID'),
            'lang' => $this->string(5)->notNull()->comment('Language'),
            'title' => $this->string(255)->notNull()->comment('Title'),
            'content' => $this->text()->defaultValue(null)->comment('Content')
        ]);

        $this->createIndex('rid', $this->tableLangName, ['rid', 'lang'], true);

        $this->addForeignKey(
            'fk-in_numbers_widget_lang-rid-in_numbers_widget-id',
            $this->tableLangName,
            'rid',
            $this->tableName,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-in_numbers_widget_lang-rid-in_numbers_widget-id', $this->tableLangName);
        $this->dropIndex('rid', $this->tableLangName);
        $this->dropTable($this->tableLangName);
    }
}
