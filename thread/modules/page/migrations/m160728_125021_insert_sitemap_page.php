<?php

use yii\db\Migration;

class m160728_125021_insert_sitemap_page extends Migration
{
    /**
     * Page table name
     * @var string
     */
    public $tablePage = '{{%page}}';

    /**
     * Language Page table name
     * @var string
     */
    public $tablePageLang = '{{%page_lang}}';


    public function safeUp()
    {
        /** Insert page */
        $this->batchInsert(
            $this->tablePage,
            [
                'id',
                'alias',
                'image_link',
                'created_at',
                'updated_at',
                'published',
                'deleted'
            ],
            [
                [
                    'id' => 77,
                    'alias' => 'sitemap',
                    'image_link' => NULL,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'published' => 1,
                    'deleted' => 0,
                ],

            ]
        );

        /** Insert page_lang */
        $this->batchInsert(
            $this->tablePageLang,
            [
                'rid',
                'lang',
                'title',
                'content'
            ],
            [
                [
                    'rid' => 77,
                    'lang' => 'en-EN',
                    'title' => 'Site Map',
                    'content' => 'Site Map'
                ],
                [
                    'rid' => 77,
                    'lang' => 'ru-RU',
                    'title' => 'Карта сайта',
                    'content' => 'Карта сайта'
                ],
                [
                    'rid' => 77,
                    'lang' => 'uk-UK',
                    'title' => 'Карта сайту',
                    'content' => 'Карта сайту'
                ],


            ]
        );
    }
    public function safeDown()
    {
        $this->truncateTable($this->tablePageLang);
        $this->dropForeignKey('fk-page_lang-rid-page-id', $this->tablePageLang);

        $this->truncateTable($this->tablePage);

        $this->addForeignKey(
            'fk-page_lang-rid-page-id',
            $this->tablePageLang,
            'rid',
            $this->tablePage,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
