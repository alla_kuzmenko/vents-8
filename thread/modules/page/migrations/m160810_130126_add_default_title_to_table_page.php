<?php

use yii\db\Migration;

class m160810_130126_add_default_title_to_table_page extends Migration
{
    public function up()
    {
        $this->addColumn('fv_page', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER alias');
    }

    public function down()
    {
        $this->dropColumn('fv_page', 'default_title');
    }
}
