<?php

use yii\db\Migration;

class m160808_115240_create_in_numbers_widget extends Migration
{

    public $tableName = '{{%in_numbers_widget}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->unsigned()->comment('ID'),
            'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            'created_at' => $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('Create time'),
            'updated_at' => $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('Update time'),
            'published' => $this->boolean()->notNull()->defaultValue(0)->comment('Published'),
            'deleted' => $this->boolean()->notNull()->defaultValue(0)->comment('Deleted')
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
