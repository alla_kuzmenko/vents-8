<?php

use yii\db\Migration;
use thread\modules\page\Page;

/**
 * Class m160126_232541_create_fv_page_lang_table
 *
 * @package thread\modules\page
 * @author Roman Gonchar <Roman Gonchar@gmail.com>
 * @copyright (c) 2016, VipDesign
 */
class m160126_232541_create_fv_page_lang_table extends Migration {

    /**
     * Related Page table name
     * @var string
     */
    public $tablePage = '{{%page}}';

    /**
     * Language Page table name
     * @var string
     */
    public $tablePageLang = '{{%page_lang}}';

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tablePageLang, [
            'rid' => $this->integer()->unsigned()->notNull()->comment('Related model ID'),
            'lang' => $this->string(5)->notNull()->comment('Language'),
            'title' => $this->string(255)->notNull()->comment('Title'),
            'content' => $this->text()->defaultValue(null)->comment('Content')
        ]);

        $this->createIndex('rid', $this->tablePageLang, ['rid', 'lang'], true);

        $this->addForeignKey(
            'fk-page_lang-rid-page-id',
            $this->tablePageLang,
            'rid',
            $this->tablePage,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-page_lang-rid-page-id', $this->tablePageLang);
        $this->dropIndex('rid', $this->tablePageLang);
        $this->dropTable($this->tablePageLang);
    }
}
