<?php

namespace thread\modules\aboutus\models;

use Yii;
use yii\helpers\ArrayHelper;

use thread\modules\aboutus\Aboutus as AboutusModule;
use thread\app\base\models\ActiveRecordLang;

/**
 * Class ItemLang
 *
 * @property integer $rid
 * @property string $lang
 * @property string $title
 * @property string $description
 * @property string $content
 *
 * @package thread\modules\aboutus\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class ItemLang extends ActiveRecordLang
{
    /**
     * @return string
     */
    public static function getDb()
    {
        return AboutusModule::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%aboutus_item_lang}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['title'], 'required'],
            ['rid', 'exist', 'targetClass' => Item::class, 'targetAttribute' => 'id'],
            ['title', 'string', 'max' => 255],
            ['content', 'string'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'rid' => Yii::t('app', 'RID'),
            'lang' => Yii::t('app', 'Lang'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'backend' => ['title', 'description', 'content'],
        ];
    }
}
