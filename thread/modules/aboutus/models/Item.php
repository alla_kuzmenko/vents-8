<?php

namespace thread\modules\aboutus\models;

use Yii;

use thread\app\base\models\ActiveRecord;
use thread\modules\aboutus\Aboutus as AboutusModule;

/**
 * Class Item
 *
 * @property integer id
 * @property string $default_title
 * @property string image_link
 * @property integer created_at
 * @property integer updated_at
 * @property boolean published
 * @property boolean deleted
 *
 * @property ItemLang $lang
 *
 * @package thread\modules\aboutus\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Item extends ActiveRecord
{
    /**
     * @return null|object|string
     */
    public static function getDb()
    {
        return AboutusModule::getDb();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%aboutus_item}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['create_time', 'update_time'], 'integer'],
            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())],
            [['default_title', 'image_link', 'image_link_s'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'backend' => ['default_title', 'image_link', 'image_link_s', 'published', 'deleted', 'published_time'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'default_title' => Yii::t('app', 'Default title'),
            'image_link' => Yii::t('app', 'Image link'),
            'image_link_s' => Yii::t('app', 'Image link small'),
            'created_at' => Yii::t('app', 'Create time'),
            'updated_at' => Yii::t('app', 'Update time'),
            'published' => Yii::t('app', 'Published'),
            'deleted' => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ItemLang::class, ['rid' => 'id']);
    }
}
