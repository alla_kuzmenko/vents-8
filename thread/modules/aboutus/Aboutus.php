<?php

namespace thread\modules\aboutus;

use thread\app\base\module\abstracts\Module as aModule;
use Yii;

/**
 * Class Aboutus
 *
 * @package thread\modules\brands
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Aboutus extends aModule
{
    public $name = 'aboutus';
    public $translationsBasePath = __DIR__ . '/messages';
    public $configPath = __DIR__ . '/config.php';

    public static function getDb()
    {
        return Yii::$app->get('db');
    }
}
