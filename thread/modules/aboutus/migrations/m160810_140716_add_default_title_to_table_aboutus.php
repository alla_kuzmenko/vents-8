<?php

use yii\db\Migration;

class m160810_140716_add_default_title_to_table_aboutus extends Migration
{
    public function up()
    {
        $this->addColumn('fv_aboutus_item', 'default_title', 'varchar(255) NULL COMMENT \'default_title\' AFTER id');
    }

    public function down()
    {
        $this->dropColumn('fv_aboutus_item', 'default_title');
    }
}
