<?php

use yii\db\Migration;
use thread\modules\aboutus\Aboutus;

/**
 * Class m160721_132150_create_fv_brands_item_lang_table
 *
 * @package thread\modules\aboutus
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class m160725_140902_create_fv_aboutus_item_lang_table extends Migration
{
    /**
     * @var string
     */
    public $tableBrandsItem = '{{%aboutus_item}}';

    /**
     * @var string
     */
    public $tableBrandsItemLang = '{{%aboutus_item_lang}}';

    public function init()
    {
        $this->db = Aboutus::getDb();
        parent::init();
    }

    /**
     * Implement migration
     */
    public function safeUp()
    {
        $this->createTable($this->tableBrandsItemLang, [
            'rid' => $this->integer(11)->unsigned()->notNull()->comment('Related model ID'),
            'lang' => $this->string(5)->notNull()->comment('Language'),
            'title' => $this->string(255)->notNull()->comment('Title'),
            'description' => $this->string(255)->defaultValue(null)->comment('Description'),
            'content' => $this->text()->defaultValue(null)->comment('Content'),
        ]);
        $this->createIndex('rid', $this->tableBrandsItemLang, ['rid', 'lang'], true);
        $this->addForeignKey(
            'fk-aboutus_item_lang-rid-aboutus_item-id',
            $this->tableBrandsItemLang,
            'rid',
            $this->tableBrandsItem,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Cancel migration
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-aboutus_item_lang-rid-aboutus_item-id', $this->tableBrandsItemLang);
        $this->dropIndex('rid', $this->tableBrandsItemLang);
        $this->dropTable($this->tableBrandsItemLang);
    }
}
