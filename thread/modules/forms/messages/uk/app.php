<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 * @version 28/03/2015
 */
return [
    //module name
    'Forms' => 'Форми',
    'FeedbackForm' => 'Форма зворотнього зв\'язку',
    'All calls' => 'Всі звернення',
    'Topic' => 'Теми',
    'name' =>'Ваше ім\'я та прізвище',
    'question' =>'Опишіть ваше питання',
    
];
