<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 * @version 28/03/2015
 */
return [
    //module name
    'Forms' => 'Forms',
    'FeedbackForm' => 'Feedback Form',
    'All calls' => 'All calls',
    'Topic' => 'Topic',
    'name' =>'Your name and surname',
    'question' =>'Describe your question',
 
];
