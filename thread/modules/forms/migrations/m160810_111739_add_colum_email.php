<?php

use yii\db\Migration;

class m160810_111739_add_colum_email extends Migration
{
    /**
 * @inheritdoc
 */
    public function up()
    {
        $this->addColumn('fv_feedback_topics', 'email', $this->string(225)->defaultValue(null)->comment('email'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('fv_feedback_topics', 'email');
    }
}
