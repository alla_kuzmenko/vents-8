<?php

namespace thread\app\base\module\interfaces;

/**
 * Interface Module
 *
 * @package thread\app\base\module\interfaces
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
interface Module {

    /**
     * @return string
     */
    public static function getDb();
}
