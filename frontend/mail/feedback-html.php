<?php

//TODO 1: Разобраться с почтовыми сообщениями.
//TODO 2: Переписать на Массивы
//TODO 3: Ссылка должна формироваться в соотвествуюем модуле, если это глобальная ссылка
//TODO 4: Почему Сброс пароля размещен в профайле, если профайл за это не отвечает. Вынести в отдельный контроллер User/Password/Reset

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \thread\modules\user\models\User */

?>
<div class="password-reset">
    <p>You have a feedback from <?= Html::encode($user->name) ?></p>
    <p><?=$user->phone?></p>
    <p><?=$user->email?></p>
    <p><?=$user->question?></p>

</div>
