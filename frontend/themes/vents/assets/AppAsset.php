<?php

namespace frontend\themes\vents\assets;

use yii\web\AssetBundle;
use yii\helpers\ArrayHelper;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $flowCss = true;

    /**
     * Registers the CSS and JS files with the given view.
     * @param \yii\web\View $view the view that the asset files are to be registered with.
     */
    public function registerAssetFiles($view)
    {
        $manager = $view->getAssetManager();
        foreach ($this->js as $js) {
            if (is_array($js)) {
                $file = array_shift($js);
                $options = ArrayHelper::merge($this->jsOptions, $js);
                $view->registerJsFile($manager->getAssetUrl($this, $file), $options);
            } else {
                $view->registerJsFile($manager->getAssetUrl($this, $js), $this->jsOptions);
            }
        }
        $manager->appendTimestamp = false;
        foreach ($this->css as $css) {
            if (is_array($css)) {
                $file = array_shift($css);
                $file_str_replace = str_replace(['../../'], [$manager->getAssetUrl($this, '')],
                    file_get_contents($this->sourcePath . '/' . $file));

                $view->registerCss(str_replace(['../'], [$manager->getAssetUrl($this, '')],
                    $file_str_replace));
            } else {

                $file_str_replace = str_replace(['../../'], [$manager->getAssetUrl($this, '')],
                    file_get_contents($this->sourcePath . '/' . $css));

                $view->registerCss(str_replace(['../'], [$manager->getAssetUrl($this, '')],
                    $file_str_replace));
            }
        }
    }

    public $sourcePath = '@app/themes/vents/web';

    public $css = [
        'css/styles/main.min.css',
        'css/website.css',
        'js/growl1.0.1/css/growl-min.css',
        'css/intlTelInput.css',
        'css/select2.css',
        'css/menu-lines.css'
    ];

    public $js = [
        '//maps.googleapis.com/maps/api/js?key=AIzaSyB_I_kCIed68Pwk1sykAyQhCI_dWHYkd5c',
        'js/all.js',
        'js/growl1.0.1/js/jquery.growl-min.js',
        'js/intlTelInput.min.js',
        'js/utils.js',
        'js/select2-min.js'
    ];

    public $depends = [
        \yii\web\YiiAsset::class,
//        \yii\bootstrap\BootstrapPluginAsset::class
    ];
}
