"use strict";
// process.env.DISABLE_NOTIFIER = true;
const imageminJpegtran = require('imagemin-jpegtran');
var gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  rename = require("gulp-rename"),
  rigger = require('gulp-rigger'),
  runSequence = require('run-sequence'), //задает последовательность задач

// STYLES
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  autoprefixer = require('gulp-autoprefixer'),
  concatCss = require('gulp-concat-css'),
  uglifycss = require('gulp-uglifycss'),
  cleanCSS = require('gulp-clean-css'),
  cssBase64 = require('gulp-css-base64'), //transform all resources found in a CSS into base64-encoded data URI strings
      //You can ignore a resource with a comment /*base64:skip*/ in CSS file after url definition.

// SCRIPTS
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  order = require("gulp-order"),

//HTML 
  minifyHTML = require('gulp-htmlmin'),

// IMAGES
    imagemin = require('gulp-imagemin'), //Обробка зображень
    imagePngquant = require('imagemin-pngquant'), //Обробка зображень формату PNG
    imageResize = require('gulp-image-resize'), //
    sprite = require('gulp-sprite-generator'),
    spritesmith = require('gulp.spritesmith'), //другой плагин для спрайтов
    gulpif = require('gulp-if'),
    cache = require('gulp-cache'),

//LIVERELOAD
  livereload = require('gulp-livereload'),
  notify = require('gulp-notify'),
  connect = require('gulp-connect');

//sass
gulp.task('sass', function () {
  gulp.src('src/scss/styles.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(autoprefixer({
      browsers: ['last 7 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('web/css/'))
    .pipe(connect.reload())
    .pipe(notify('Done!'));
});
//concat CSS and uglify it
gulp.task('concatCss', function () {
  //return gulp.src(['web/css/reset.css', 'web/css/styles.css'])
    return gulp.src('web/css/styles.css')
    .pipe(concatCss("main.css"))
    .pipe(gulp.dest('web/css/styles'));
});

gulp.task('ugCss', ['concatCss'], function () {
  gulp.src('web/css/styles/main.css')
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    // .pipe(cssBase64({
    //   baseDir: dev_paths['images-css'],
    //   maxWeightResource: 100,
    //   extensionsAllowed: ['.gif', '.jpg', '.png']
    // }))
    .pipe(cleanCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('web/css/styles/'))
     .pipe(notify('Done uglify!'));
});
//js
gulp.task('js', function () {
  // gulp.src('src/js/*.js')
    gulp.src('src/js/rigfile.js')
    .pipe(rigger()) 
    .pipe(plumber())
    // .pipe(order([
    //   "src/js/template.js",
    //   "src/js/jquery.tinycarousel.min.js",
    //   "src/js/script.js",
    //   "src/js/map.js"
    // ]))
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('web/js/'))
    .pipe(connect.reload())
    .pipe(notify('Done!'));
});

// html
gulp.task('html', function () {
  var opts = {
    conditionals: true,
    spare: true
  };
  gulp.src('src/*.html')
    .pipe(plumber())
    // .pipe(minifyHTML(opts))
    .pipe(gulp.dest('web/'))
    .pipe(connect.reload())
    .pipe(notify('Done!'));
});

// gulp.task('assets', function () {
//   gulp.src('src/img/**')
//     .pipe(plumber())
//     .pipe(gulp.dest('dist/img/'))
//     .pipe(connect.reload())
//     .pipe(notify('Images: done'));
//   gulp.src('src/scss/fonts/**')
//     .pipe(gulp.dest('dist/css/fonts/'))
//     .pipe(connect.reload())
//     .pipe(notify('Fonts: done'));
// });

var dev_paths = {
  'images-jpg': ['src/img/**/*.jpg'],
  'images-png': ['src/img/**/*.png'],
  'images':     ['src/img/*'],
  'images-css': ['./web/img/']
 };
//destination
var build_paths = {
  'images': 'web/img'
};
////////////////////////////////////////////////////////////////////////////////////////
// [generate sprite] 
// берет картинки из файла стилей и склеивает их в спрайт, стили меняются
// картинка должна быть прописана в background-image
gulp.task('sprites', function() {//зависимость от задачи css, потому что он анализирует css файл
    var spriteOutput;
 
  spriteOutput = gulp.src("web/css/styles/main.css")
    .pipe(sprite({
            baseUrl:         "./", //Default value: ./ Defines where to find relatively defined image references in the input stylesheet.
            spriteSheetName: "sprite.png",//имя спрайта
            spriteSheetPath: ""  //путь будет подставляться в файл со спрайтом
    }));
 
    spriteOutput.css.pipe(gulp.dest("web/img/sprite"));// куда вставляются новые стили с новыми путями
    spriteOutput.img.pipe(gulp.dest("web/img/sprite")); //куда вставляетс спрайт
});
/////////////////////////////////////////////////////////////////////////
// gulp.task('sprite', function () {
//     var spriteData = gulp.src('src/img/sprites/*.png')
//         .pipe(spritesmith({
//             /* this whole image path is used in css background declarations */
//             imgName: './dist/img/sprites/sprite.png',
//             cssName: 'sprite.css'
//         }));
//     spriteData.img.pipe(gulp.dest('dist/img/sprites/'));
//     spriteData.css.pipe(gulp.dest('dist/css/sprites/'));
// });
///////////////////////////////////////////////////////////////////////////////////////
//построим последовательность задач
gulp.task('buildCss', function(callback) {
    runSequence('sass', 'concatCss', 'sprites', callback);
});
// [ ОБРОБКА ЗОБРАЖЕНЬ ]
gulp.task('images', ['image-jpg', 'image-png']);

  // [ ОБРОБКА JPG ]
 
gulp.task('image-jpg', function () {
  return gulp.src(dev_paths['images-jpg'])
      .pipe(cache(imagemin({use: [imageminJpegtran()]})))
      .pipe(gulp.dest(build_paths['images']));
});

  // [ ОБРОБКА PNG ]
gulp.task('image-png', function () {
  return gulp.src(dev_paths['images-png'])
      .pipe(cache(imagemin({
        use: [imagePngquant()]
      })))
      .on('error', console.log)
      .pipe(gulp.dest(build_paths['images']));
});
// gulp.task('img', () =>
//   gulp.src('src/img/**/*')
//     .pipe(imagemin())
//     .pipe(gulp.dest('dist/img/'))
// );
//connect
gulp.task('connect', function () {
  connect.server({
    root: 'web/',
    port: 8000,
    livereload: true
  });
});


//Watch
gulp.task('watch', function () {
  gulp.watch(['src/js/*.js'], ['js']);
  gulp.watch(['src/img/**/*'], ['images']);
  gulp.watch(['src/*.html'], ['html']);
  gulp.watch(['src/scss/*.scss'], ['sass']);
  gulp.watch(['web/css/*.css'], ['concatCss','ugCss']);
});

gulp.task('compile', ['sass', 'js', 'html']);
// gulp.task('compileCSS', ['css', 'ugCss']);

//Default Task
gulp.task('default', ['connect', 'sass', 'concatCss', 'ugCss','js', 'html', 'watch']);