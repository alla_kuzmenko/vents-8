/* 
 * @author Anastasia Popova
 * @copyright (c) 2016, VipDesign
 */

/**
 * initialize function
 * @returns {undefined}
 */

function initialize() {
    if(!document.getElementById("map")) return false;
    var myLatlng = new google.maps.LatLng(37.4501, 12.5234);

    var myOptions = {
        zoom: 2,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        zoomControl: false,
        scrollwheel: false,
        draggable: false,
        minZoom: 2
    }
    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    /**
     * map styles
     * @type Array
     */
    var styles = [
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {"color": "#0c4da1"}
            ]
        },
        {
            featureType: 'administrative.country',
            elementType: 'geometry',
            stylers: [
                {"color": "#54a7d9"},
                {"lightness": 11}
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {"color": "#2963ba"}

            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {"color": "#808080"}
            ]
        },
        {
            "featureType": "water",
            "stylers": [
                {"color": "#54a7d9"},
                {"lightness": 11}
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [{ "visibility": "off" }]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "elementType": "labels",
            "stylers": [{ "visibility": "off" }]
        }

    ];
    map.setOptions({styles: styles});

    /**
     * geocoder find adress param {id}
     * @type google.maps.Geocoder
     */
    var geocoder = new google.maps.Geocoder();
    
    $('.country').bind('DOMSubtreeModified', function(){
        address = $(this).html();

       geocodeAddress(geocoder, map);

       //set zoom for country
       if (address == "Russia" || address == "USA" || address == 'Россия' || address == 'США') {
           map.setZoom(4);
       } else {
           map.setZoom(5);
       }
    
    });
    
    //create infoWindow
    var infoWindow = new google.maps.InfoWindow;
    /**
     * function create marker
     * @returns {undefined}
     */
    var onMarkerClick = function () {
        var marker = this;
        var latLng = marker.getPosition();
        infoWindow.setContent(marker.content);
        //infoWindow.setStyle('overflow', 'hidden');
        infoWindow.open(map, marker);
    };
    
    var onMarkerOver = function(){
        var marker = this;
            marker.setIcon(''+fv_BASE_URL+'/img/marker-active.png');
    };
    var onMarkerOut = function(){
        var marker = this;
            marker.setIcon(''+fv_BASE_URL+'/img/marker.png');
    };
    
    google.maps.event.addListener(map, 'click', function () {
        infoWindow.close();
    });

    //Kyiv
    var marker1 = new google.maps.Marker({
        map: map,
        icon: ''+fv_BASE_URL+'/img/marker.png',
        content: "Kyiv <br><a href='#'>kiev.com.ua</a>",
        position: new google.maps.LatLng(50.4501, 30.5234)
    });

    //Warwava
    var marker2 = new google.maps.Marker({
        map: map,
        icon: ''+fv_BASE_URL+'/img/marker.png',
        content: 'Office Vents Group <br>'+
        '<div class="info"><img src = '+fv_BASE_URL+'/img/icon/map1.png></img> Poznan, Poland <br> '+
        '<img src = '+fv_BASE_URL+'/img/icon/map2.png></img> <a href="#">www.ventsgroup.pl</a> <br>'+
        '<img src = '+fv_BASE_URL+'/img/icon/map3.png></img>  <a href="#">www.ventsgroup-online.pl</a></div>',
        position: new google.maps.LatLng(52.2296756, 21.0122287)
    });
    //Minsk
    var marker3 = new google.maps.Marker({
        map: map,
        icon: ''+fv_BASE_URL+'/img/marker.png',
        content: 'Minsk',
        position: new google.maps.LatLng(53.9045398, 27.5615244)
    });
    
    //create events for all markers
    var markers = [marker1, marker2, marker3];
    for (i=0; i<markers.length; i++){
        google.maps.event.addListener(markers[i], 'click', onMarkerClick);
        google.maps.event.addListener(markers[i], 'mouseover', onMarkerOver);
        google.maps.event.addListener(markers[i], 'mouseout', onMarkerOut);
    }

    google.maps.event.addDomListener(map,'zoom_changed', function() {
        var zoom =  map.getZoom();
        if (zoom ==2) {
            $('#map').css('max-width','1024px');
            google.maps.event.trigger(map, 'resize');
            map.set('draggable', false);
            map.set('zoomControl', false);
            map.set('center', myLatlng);
            $('.top-wr').removeClass('top-abs');
            $('.header-wr').css('height','auto');
            $('#map').css('height','550px');
            $('.main-title').css('marginTop','-415px');
        }
        else if (zoom >2) {
            $('#map').css('max-width','100%');
            google.maps.event.trigger(map, 'resize');
            map.set('draggable', true);
            map.set('zoomControl', true);
            map.set('zoomControl', true);
            $('.top-wr').addClass('top-abs');
            $('.header-wr').css('height','615px');
            $('#map').css('height','660px');
            $('.main-title').css('marginTop','-410px');
        }

    });
}

initialize();

/**
 * 
 * @param {type} geocoder
 * @param {type} resultsMap
 * @returns {undefined}
 * calculates where the county is situated
 */
function geocodeAddress(geocoder, resultsMap) {

    geocoder.geocode({'address': address}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            //get center on the country
            resultsMap.setCenter(results[0].geometry.location);

        } else {
            //alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}
