/* 
 * @author Anastasia Popova
 * @copyright (c) 2016, VipDesign
 */

/**
 * initialize function
 * @returns {undefined}
 */

function initialize() {
    if(!document.getElementById("map1")) return false;
    var myLatlng = new google.maps.LatLng(50.449, 30.498);
    var myOptions = {
        zoom: 16,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        zoomControl: false,
        scrollwheel: false

    }
    var map = new google.maps.Map(document.getElementById("map1"), myOptions);

    /**
     * map styles
     * @type Array
     */
    var styles = [
  {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [
      { "hue": "#00f6ff" },
      { "gamma": 0.66 },
      { "lightness": 4 }
    ]
  },{
  }
];
    map.setOptions({styles: styles});

    
    
    //create infoWindow
    var infoWindow = new google.maps.InfoWindow;
    /**
     * function create marker
     * @returns {undefined}
     */
    var onMarkerClick = function () {
        var marker = this;
        var latLng = marker.getPosition();
        infoWindow.setContent(marker.content);
        infoWindow.open(map, marker);
    };
    
    var onMarkerOver = function(){
        var marker = this;
            marker.setIcon(''+fv_BASE_URL+'/img/bl-marker.png');
    };
    var onMarkerOut = function(){
        var marker = this;
            marker.setIcon(''+fv_BASE_URL+'/img/bl-marker.png');
    };
    
    google.maps.event.addListener(map, 'click', function () {
        infoWindow.close();
    });

    //Kyiv
    var marker1 = new google.maps.Marker({
        map: map,
        icon: ''+fv_BASE_URL+'/img/bl-marker.png',
        content: "Kyiv <br><a href='#'>kiev.com.ua</a>",
        position: new google.maps.LatLng(50.448482, 30.504677)
    });
    //infoWindow.setContent(marker1.content);
    //infoWindow.open(map, marker1);
    
    //create events for all markers
    var markers = [];
    for (i=0; i<markers.length; i++){
        google.maps.event.addListener(markers[i], 'click', onMarkerClick);
        google.maps.event.addListener(markers[i], 'mouseover', onMarkerOver);
        google.maps.event.addListener(markers[i], 'mouseout', onMarkerOut);

    }
  
}

initialize();

