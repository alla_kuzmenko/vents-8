"use strict";
var gulp = require('gulp'),
    cssBase64 = require('gulp-css-base64'); //transform all resources found in a CSS into base64-encoded data URI strings
//You can ignore a resource with a comment /*base64:skip*/ in CSS file after url definition.

gulp.task('default', function () {
    return gulp.src('css/styles/main.min.css')
        .pipe(cssBase64())
        .pipe(gulp.dest('css/styles/'));
});