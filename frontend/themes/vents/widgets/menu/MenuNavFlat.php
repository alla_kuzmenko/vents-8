<?php

namespace frontend\themes\vents\widgets\menu;

use yii\bootstrap\Widget;
use frontend\modules\menu\models\Menu;
use frontend\modules\menu\models\MenuItem;

/**
 * Class MenuNavFlat
 *
 * @package frontend\themes\vents\widgets\menu
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class MenuNavFlat extends Widget
{

    public $view = 'header_menu';
    public $name = 'menunavflat';
    public $menuAlias = '';
    protected $menu = null;
    protected $items = null;

    public function init()
    {
        parent::init();

        if (!empty($this->menuAlias)) {
            $this->menu = Menu::findByAlias($this->menuAlias);
            if ($this->menu !== null) {
                $this->items = MenuItem::findAllByGroup($this->menu['id']);
            }

            if ($this->items !== null) {
                foreach ($this->items as $k => $item) {
                    if ($item['link_type'] == 'internal') {
                        $this->items[$k]['link'] = (isset($item->source)) ? $item->source->getUrl() : false;
                    }
                }
            }
        }
    }

    public function run()
    {
        return ($this->menu !== null) ? $this->render($this->view, ['menu' => $this->menu, 'items' => $this->items]) : '';
    }

}
