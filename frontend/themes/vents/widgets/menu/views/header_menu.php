<?php

use yii\helpers\Html;

$i = '';

/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */

?>
<div class="newMenu">
    <div class="menuBut">MENU</div>
        <?php if (!empty($items)):
            echo Html::beginTag('ul', ['class' => 'menuSmall']);
            foreach ($items as $item) {
                $i .= Html::beginTag("li") .
                    Html::a($item['lang']['title'], [$item['link']], ['target' => $item['link_target']]) .
                    Html::endTag('li');
            }
            echo $i;
            echo Html::endTag('ul');
        endif; ?>
    </ul>
</div>


<?php
$i = '';

if (!empty($items)):
    echo Html::beginTag('ul', ['class' => 'menu']);
    foreach ($items as $item) {
        $i .= Html::beginTag("li") .
                Html::a($item['lang']['title'], [$item['link']], ['target' => $item['link_target']]) .
                Html::endTag('li');
    }
    echo $i;
    echo Html::endTag('ul');
endif;

