<?php

use yii\helpers\Html;

$i = '';
/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */

if (!empty($items)):
    echo Html::beginTag('ul', ['class' => 'menu']);
    foreach ($items as $item) {
        $i .= Html::beginTag("li") .
                Html::a($item['lang']['title'], $item['link'], ['target' => $item['link_target']]) .
                Html::endTag('li');
    }
    echo $i;
    echo Html::endTag('ul');
endif;

