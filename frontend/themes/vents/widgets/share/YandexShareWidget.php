<?php

namespace frontend\themes\vents\widgets\share;

use Yii;
use yii\widgets\InputWidget;

/**
 * @author Andrii Bondarchuk
 *
 * API: https://tech.yandex.ru/share/
 * API: https://tech.yandex.ru/share/doc/dg/concepts/share-button-ov-docpage/
 * DEBUG Url: https://developers.facebook.com/tools/debug/og/object/
 */
class YandexShareWidget extends InputWidget
{
    /**
     * @var array available for facebook Share Button
     * facebook,twitter,vkontakte
     */
    public $services;

    /**
     * @var array the meta settings
     */
    public $meta = [];

    /**
     * Initialize the widget
     */
    public function init()
    {
        Yii::$app->view->registerJsFile(
            '//yastatic.net/es5-shims/0.0.2/es5-shims.min.js'
        );
        Yii::$app->view->registerJsFile(
            '//yastatic.net/share2/share.js'
        );

        // Meta Tag , Open Graph properties
        if (isset($this->meta['fb_app_id'])) {
            Yii::$app->view->registerMetaTag(['name' => 'fb:app_id', 'property' => $this->meta['fb_app_id']], 'fb:app_id');
        }

        if ($this->meta['type']) {
            Yii::$app->view->registerMetaTag(['name' => 'og:type', 'content' => $this->meta['type']], 'og:type');
        }

        if ($this->meta['image']) {
            Yii::$app->view->registerMetaTag(['name' => 'og:image', 'content' => $this->meta['image']], 'og:image');
        }

        if ($this->meta['title']) {
            Yii::$app->view->registerMetaTag(['name' => 'og:title', 'content' => $this->meta['title']], 'og:title');
        }

        if ($this->meta['description']) {
            Yii::$app->view->registerMetaTag(['name' => 'og:description', 'content' => $this->meta['description']], 'og:description');
        }

        if ($this->meta['url']) {
            Yii::$app->view->registerMetaTag(['name' => 'og:url', 'content' => $this->meta['url']], 'og:url');
        }
    }

    /**
     * Run the widget
     */
    public function run()
    {
        return '<div class="ya-share2" data-lang="ru" data-services="' . $this->services . '" data-counter></div>';
    }

}