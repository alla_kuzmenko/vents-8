<?php
use yii\helpers\Html;

?>

<?php foreach ($models as $article) : ?>
    <div>
        <a href="<?= $article['link'] ?>" rel="nofollow" target="_blank">
            <img src="<?= $article->getArticleImageSmall() ?>" alt="<?= Html::encode($article['lang']['title']) ?>">
        </a>
    </div>
<?php endforeach; ?>







