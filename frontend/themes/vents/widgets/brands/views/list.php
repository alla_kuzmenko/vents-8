<?php
use yii\helpers\Html;
?>

<div class="brand-cont">
    <div class="block-slogan"><h2 id="br-block"><?=Yii::t('app', 'our brands')?></h2><p class="br-slogan"><?= Yii::$app->getModule('configs')->getParam('10') ?></p></div>
    <?php foreach ($models as $article) : ?>
        <div class="brand-text">
            <div class="vent-br"><img src="<?= $article->getArticleImage() ?>" alt="<?= Html::encode($article['lang']['title']) ?>"></div>
            <div class="description"><?= Html::encode(strip_tags($article['lang']['description'])) ?></div>
            <a href="<?= Html::encode($article['link']) ?>" class="button" target="_blank"><?=Yii::t('app', 'Visit website')?></a>
        </div>
    <?php endforeach; ?>
</div>







