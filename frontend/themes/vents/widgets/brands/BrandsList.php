<?php

namespace frontend\themes\vents\widgets\brands;

use frontend\modules\brands\models\BrandsItem;
use thread\app\base\widgets\Widget;

/**
 * Class BrandsList
 *
 * @package frontend\themes\vents\widgets\brands
 * @author Andrii Bondarchuk
 * @copyright (c) 2016*
 */
class BrandsList extends Widget
{
    public $view = 'list';

    /**
     * @var int
     */
    public $limit = 4;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render($this->view, [
            'models' => $this->getItems(),
        ]);
    }

    /**
     * @return mixed
     */
    private function getItems()
    {
        return BrandsItem::findForIndexPage()
            ->limit($this->limit)
            ->all();
    }
}
