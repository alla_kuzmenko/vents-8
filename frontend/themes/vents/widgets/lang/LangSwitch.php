<?php

namespace frontend\themes\vents\widgets\lang;

use Yii;
//
use thread\app\base\widgets\Widget;
//
use frontend\modules\configs\models\Language;


/**
 * Class LangSwitch
 *
 * @package frontend\themes\vents\widgets\lang
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class LangSwitch extends Widget
{
    protected $current = null;
    protected $items = null;
    public $view = 'LangSwitch';

    public function init()
    {
        $model = new Language();

        $this->items = $model->getLanguages();
    }

    public function run()
    {
        $items = [];

        $userGuest = Yii::$app->getUser()->isGuest;

        foreach ($this->items as $key => $lang) {

            if ($userGuest) {
                if (isset($lang['only_editor']) && $lang['only_editor'] == 1) {
                    unset($this->items[$key]);
                }
            }
        }

        /**
         * Redirects huck
         */
        $module = Yii::$app->controller->module->id;
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;

        $redirects = '';
        if ($controller == 'article' && $module == 'news') {
            $model = Yii::$app->controller->getModel();
            $redirects = $model->getRedrects();
        }

        foreach ($this->items as $key => $lang) {

            $pathinfo = (!empty(\Yii::$app->request->pathInfo)) ? '/' . \Yii::$app->request->pathInfo : '/';

            if ($lang['local'] == Yii::$app->language) {
                $this->current = [
                    'label' => $lang['label'],
                    'url' => '/' . $lang['alias'] . $pathinfo,
                    'alias' => $lang['alias'],
                    'model' => $lang,
                ];
            }

            if (!$lang['default']) {
                $items[$key] = [
                    'label' => $lang['label'],
                    'url' => '/' . $lang['alias'] . $pathinfo,
                    'alias' => $lang['alias'],
                    'model' => $lang,
                ];
            } else {
                $items[$key] = [
                    'label' => $lang['label'],
                    'url' => '/' . \Yii::$app->request->pathInfo,
                    'alias' => $lang['alias'],
                    'model' => $lang,
                ];
            }

            /**
             * Redirects huck
             */
            if ($controller == 'article' && $module == 'news') {
                if (isset($redirects->{$lang['local']}) && !empty($redirects->{$lang['local']})) {
                    $items[$key]['url'] = $redirects->{$lang['local']};
                } else {
                    $items[$key]['url'] = '/' . ((!$lang['default']) ? $lang['alias'] . '/' : '');
                }

            }
        }

        if ($this->current == null) {
            if ($lang['local'] == Yii::$app->language && $lang['only_editor'] == 1) {
                $this->current = array_shift($items);
            }
        }
        return $this->render($this->view, [
            'current' => $this->current,
            'items' => $items,
        ]);

    }
}
