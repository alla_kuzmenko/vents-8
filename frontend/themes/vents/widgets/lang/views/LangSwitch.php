<?php
use frontend\themes\vents\assets\AppAsset;

$bundle = AppAsset::register($this);

?>

<div class="selected upper" style="background-image: url(<?=$current['model']->getImage()?>)"><?= $current['alias'] ?></div>
<ul class="language">
    <?php foreach ($items as $item): ?>
        <?php if ($item['alias'] == $current['alias']) continue; ?>
        <li class="upper"><a href="<?= $item['url'] ?>" style="background-image: url(<?= $item['model']->getImage()?>)"><?= $item['alias'] ?></a></li>
    <?php endforeach; ?>
</ul>
