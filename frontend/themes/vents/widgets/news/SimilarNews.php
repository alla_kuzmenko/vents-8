<?php

namespace frontend\themes\vents\widgets\news;

use frontend\modules\news\models\Article;
use thread\app\base\widgets\Widget;

/**
 * Class SimilarNews
 *
 * @package frontend\themes\vents\widgets\news
 * @author Andrii Bondarchuk
 * @copyright (c) 2016*
 */
class SimilarNews extends Widget
{
    /**
     * @var int
     */
    public $limit = 3;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('list', [
            'models' => $this->getItems(),
        ]);
    }

    /**
     * @return mixed
     */
    private function getItems()
    {
        return Article::find_base()
            ->orderBy(['published_time' => SORT_DESC])
            ->limit($this->limit)
            ->all();
    }
}
