<?php
use frontend\themes\vents\assets\AppAsset;
use yii\widgets\Breadcrumbs;

$bundle = AppAsset::register($this);
Yii::$app->view->registerJsFile($bundle->baseUrl . '/js/map/map1.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->beginContent('@app/layouts/main.php');

$this->title = Yii::t('app', 'Site map');

?>

<div class="map-wr">
    <div class="bg-dots">
        <div class="map-content">
            <?= $this->render('@app/layouts/parts/_header', ['bundle' => $bundle]) ?>
            <?=
            Breadcrumbs::widget([
                'links' => $this->context->breadcrumbs
            ])
            ?>

            <div class="neck">
                <h1><?= Yii::t('app', 'Site map') ?></h1>
            </div>


            <?= \frontend\modules\page\widgets\Sitemap\Sitemap::widget() ?>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>
