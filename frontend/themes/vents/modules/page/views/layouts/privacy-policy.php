<?php

use frontend\themes\vents\assets\AppAsset;
use frontend\themes\vents\modules\page\widgets\inNumbers\InNumbers;
use frontend\themes\vents\modules\page\widgets\inWorld\InWorld;

$bundle = AppAsset::register($this);

$this->beginContent('@app/layouts/main.php');
?>

    <div class="confid-wr">
        <div class="inner">

            <?= $this->render('@app/layouts/parts/_header', ['bundle' => $bundle]) ?>

            <?= $content ?>

        </div>
        <div class="info">
            <?= InWorld::widget() ?>

            <?= InNumbers::widget(); ?>
        </div>
    </div>
<?php $this->endContent(); ?>