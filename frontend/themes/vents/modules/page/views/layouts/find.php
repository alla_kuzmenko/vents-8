<?php
use frontend\themes\vents\modules\page\widgets\inNumbers\InNumbers;
use frontend\themes\vents\modules\page\widgets\inWorld\InWorld;
use yii\widgets\Breadcrumbs;
use frontend\themes\vents\assets\AppAsset;
use yii\helpers\Url;

$bundle = AppAsset::register($this);

$this->beginContent('@app/layouts/main.php');

?>
    <div class="confid-wr">
        <div class="inner">

            <?= $this->render('@app/layouts/parts/_header', ['bundle' => $bundle]) ?>

            <?=
            Breadcrumbs::widget([
                'links' => $this->context->breadcrumbs
            ])
            ?>

            <div class="neck">
                <h1><?= Yii::t('app', 'Search') ?></h1>
            </div>
        </div>
    </div>

    <div class="clearfix mutual">
        <?= $content ?>

        <div class="info">
                <?= InWorld::widget() ?>

                <?= InNumbers::widget() ?>
            </div>
    </div>


<?php $this->endContent(); ?>