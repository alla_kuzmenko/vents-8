<?php
use yii\widgets\Breadcrumbs;
use frontend\themes\vents\assets\AppAsset;

$bundle = AppAsset::register($this);
Yii::$app->view->registerJsFile($bundle->baseUrl . '/js/map/map1.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->beginContent('@app/layouts/main.php');

?>

    <div class="contact-wr">
<!--        <div id="map1"></div>-->
<!--        <div class="shadow"></div>-->

        <div class="content">
            <?= $this->render('@app/layouts/parts/_header', ['bundle' => $bundle]) ?>

            <?=
            Breadcrumbs::widget([
                'links' => $this->context->breadcrumbs
            ])
            ?>

            <div class="contact">

                <div class="left-part">
                    <div class="left-corner"></div>
                    <div class="line"></div>
                </div>

                <h1><?=Yii::t('app', 'contacts')?></h1>
                <?php echo $content ?>


<!--                <div class="branch clearfix">-->
<!--                    <p class="delivery">--><?php //=Yii::t('app', 'For delivery of products on the territory of Ukraine') ?><!--:<span>+38 (044) 406-36-27</span><span>+38 (044) 406-07-50</span></p>-->
<!--                    <p class="custom">--><?php //=Yii::t('app', 'Customer Service') ?><!--:<span class="pd">0-800-501-74-80</span><span class="free">--><?php //=Yii::t('app', 'free Ukraine from fixed phones') ?><!--</span></p>-->
<!--                    <p class="export">--><?php //=Yii::t('app', 'export Department') ?><!--:<span>+38 (044) 406-36-25</span></p>-->
<!--                    <p class="cadr">--><?php //=Yii::t('app', 'Human Resources Department') ?><!--:<span>+38 (044) 406-36-26</span></p>-->
<!--                </div>-->
<!--                <p class="adr">--><?php //=Yii::t('app', 'address_vents')?><!--</p>-->
                <div class="right-part">
                    <div class="line"></div>
                    <div class="right-corner"></div>
                </div>

            </div>
        </div>
    </div>
    <div class="form-wr">
        <div class="form">
            <?= \frontend\modules\page\widgets\Feedback\Feedback::widget() ?>
        </div>
    </div>
    <!-- Контакты -->
    <script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "Organization",
 "url": "<?= Yii::$app->getUrlManager()->getHostInfo(); ?>",
 "logo": {
       "@type": "ImageObject",
       "url": "<?= Yii::$app->getUrlManager()->getHostInfo() . $bundle->baseUrl ?>/img/logo.png",
       "width": "174",
       "height": "77"
       },
 "contactPoint": [{
       "@type": "ContactPoint",
       "telephone": "+38(044)406-36-27",
       "contactType": "customer service"
       },
       {
       "@type": "ContactPoint",
       "telephone": "+3800-501-74-80",
       "contactType": "customer service"
       }],

   "location": {
           "@type": "PostalAddress",
           "addressLocality": "Ukraine",
           "addressRegion": "Kiev",
           "postalCode": "01030",
           "streetAddress": "Kotsyubyns'kogo street 1"
           }

}
</script>
<?php $this->endContent(); ?>