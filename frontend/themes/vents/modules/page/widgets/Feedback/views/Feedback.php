<?php

use frontend\modules\forms\models\FeedbackForm;
use backend\themes\inspinia\widgets\forms\ActiveForm;
use frontend\themes\vents\assets\AppAsset;
use thread\modules\configs\Configs;
use yii\helpers\Html;
use yii\captcha\Captcha;
$bundle = AppAsset::register($this);
$url=$bundle->baseUrl;
?>

<p><?= Yii::t('form', 'FeedbackForm') ?></p>
<?php $modelCallBack = new FeedbackForm() ?>

<?php
$formCallBack = ActiveForm::begin([
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'ajaxDataType' => true,
    'id' => 'feedback-form',
    'action' => '#'
]);
?>
<div class="collection clearfix">

    <div class="input-block clearfix">
        <?= $formCallBack->field($modelCallBack, 'name', [
            'inputOptions' => [
                'style' => '',
                'placeholder' => $modelCallBack->getAttributeLabel('name'),
                'class' => 'name'
            ]
        ])->label(false); ?>

        <?= Html::error($modelCallBack, 'name', ['class' => 'name-error errors']) ?>
    </div>
    <div class="input-block clearfix">

        <?= $formCallBack->field($modelCallBack, 'phone', [
            'inputOptions' => [


                'id' => 'phone',
//                'type' => 'tel',
            ]
        ])->label(false); ?>

        <?= Html::error($modelCallBack, 'phone', ['class' => 'phone-error errors']); ?>
    </div>
    <div class="input-block clearfix">
        <?= $formCallBack->field($modelCallBack, 'email', [
            'inputOptions' => [
                'style' => '',
                'placeholder' => $modelCallBack->getAttributeLabel('email'),
                'class' => 'email'
            ]
        ])->label(false); ?>

    </div>
    <div class="input-block clearfix">
        <div class="select-style">
            <?= $formCallBack->field($modelCallBack, 'topic_id')->label('')->dropDownList($TopicDropdownList);
            ?>

        </div>
        <?= Html::error($modelCallBack, 'email', ['class' => 'topic_id-error errors']); ?>
    </div>
    <div class="input-block clearfix">
        <?= $formCallBack->field($modelCallBack, 'question', [
            'inputOptions' => [
                'style' => 'resize:none;',
                'placeholder' => $modelCallBack->getAttributeLabel('question'),
                'class' => 'tarea',
                'rows' => 5,
            ]
        ])->textarea()->label(false); ?>

        <?= Html::error($modelCallBack, 'question', ['class' => 'question-error errors']); ?>
    </div>
    <!--<div class="captcha clearfix">
    <?php
    /*= $formCallBack->field($modelCallBack, 'reCaptcha')->label('')->widget(
        \himiklab\yii2\recaptcha\ReCaptcha::className(),
        ['siteKey' => '6LffASYTAAAAAOzNPp76vwu77r5GLyMMLu1d11TQ']
    ) */
    ?>
    </div>-->

    <?php // var_dump($bundle); die; ?>

</div>

<!--<input type="submit" class="submit" value="Отправить">-->
<?= Html::submitButton('<b>' . Yii::t('form', 'send') . '</b>', ['class' => 'submit']) ?>
<!-- <button type="submit" class="submit"><b>Отправить</b></button>-->
<?php $formCallBack->end();
$this->registerJs('
var telInput = $("#phone"),
errorMsg = $("#error-msg"),
validMsg = $("#valid-msg");

// initialise plugin
telInput.intlTelInput({
  geoIpLookup: function(callback) {
        $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : "";
          callback(countryCode);
        });
      },
      initialCountry: "auto",
      separateDialCode: true, //code of the country
  utilsScript: "utils.js"
});

telInput.on("focus", function(e, countryData) {

  newValue = e.target.placeholder.replace(/[^0-9\.]/g,\'\').length;

  $(this).on(\'keyup\', function(e){

    $(this).val($(this).val().replace(/\D/g,\'\'));
    if($(this).val().replace(/\D/g,\'\').length>newValue){
      $(this).val($(this).val().substring(0,newValue)) ;
      
    }
  });
});

//reset telephone number after change country
telInput.on(\'countrychange\', function(){
$(this).val(\'\');
});
');
?>
<div class="popup"  style="display: none">
    <div class="shadow"></div>
    <div class="message"><div class="text"><?=Configs::getParam(3)?></div><a href="#" class="button"><?= Yii::t('app', 'Close') ?></a><div class="close"></div></div>
</div>

