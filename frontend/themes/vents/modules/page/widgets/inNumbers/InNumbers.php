<?php
namespace frontend\themes\vents\modules\page\widgets\inNumbers;

use frontend\modules\page\models\InNumbersWidget;
use thread\app\base\widgets\Widget;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class InNumbers extends Widget
{
    public $view = 'inNumbers';
    public $elements = [];

    public function init()
    {
        $this->elements = InNumbersWidget::findBase()->asArray()->all();
    }

    public function run()
    {
        return $this->render($this->view, ['models' => $this->elements]);
    }
}
