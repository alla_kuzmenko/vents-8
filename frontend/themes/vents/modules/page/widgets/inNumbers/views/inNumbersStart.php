<div class="achieve">
    <?php foreach ($models as $model) : ?>
        <div class="ach">
            <span><?= $model['lang']['title'] ?></span>
            <div><?= $model['lang']['content'] ?></div>
        </div>
    <?php endforeach; ?>
</div>