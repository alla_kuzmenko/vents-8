<div class="in-number">
    <div class="f-block"><p class="feature num"><?= Yii::t('app', 'In numbers') ?></p></div>
    <div class="jcarousel2">
        <ul>
            <?php foreach ($models as $model) : ?>
                <li>
                    <div class="ach">
                        <span><?= $model['lang']['title'] ?></span>
                        <div><?= $model['lang']['content'] ?></div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="jcarousel-pagination"></div>
</div>