<?php
namespace frontend\themes\vents\modules\page\widgets\inWorld;

use thread\app\base\widgets\Widget;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class InWorld extends Widget
{
    public function run()
    {
        return $this->render('inWorld');
    }
}
