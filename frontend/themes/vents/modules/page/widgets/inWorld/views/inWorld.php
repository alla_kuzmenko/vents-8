<?php

use yii\helpers\Url;

?>
<div class="in-world">
    <div class="f-block"><p class="feature"><?= Yii::t('app', 'In the world') ?></p></div>
    <a href="<?= Url::toRoute(['/page/page/view', 'alias' => 'basestart']) ?>" class="button"><?= Yii::t('app', 'Learn more') ?></a>
</div>