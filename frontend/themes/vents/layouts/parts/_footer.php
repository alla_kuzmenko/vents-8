<?php
use frontend\themes\vents\widgets\brands\BrandsList;
use frontend\themes\vents\widgets\menu\MenuNavFlat;

?>

<footer>
    <div class="footer-wr clearfix">
        <div class="footer-left-fl">
            <div class="footer-left">
                <nav>
                    <?= MenuNavFlat::widget(['menuAlias' => 'footer', 'view' => 'footer_menu']) ?>
                </nav>

            </div>
        </div>
        <div class="footer-right-fl">
            <div class="footer-right">
                <?= BrandsList::widget(['view' => 'logo_list', 'limit' => 99]) ?>
            </div>
        </div>
        <div class="copyright">&copy;2016 Blauberg Group</div>
    </div>
</footer>