<?php
use frontend\themes\vents\widgets\lang\LangSwitch;
use frontend\themes\vents\widgets\menu\MenuNavFlat;
use yii\helpers\Url;
use frontend\modules\configs\models\Language;

?>

<header>
    <div class="nav-box clearfix">
        <div class="head-shad"></div>
        <div class="logo">
            <?php if (Yii::$app->request->pathInfo): ?>
                <a href="<?= Language::getHomeUrl() ?>">
                    <img srcset="<?= $bundle->baseUrl ?>/img/blauberg-group.png 1x, <?= $bundle->baseUrl ?>/img/bl1.png 2x, <?= $bundle->baseUrl ?>/img/bl2.png 3x" src="<?= $bundle->baseUrl ?>/img/blauberg-group.png" alt="Blauberg Group">
                </a>
            <?php else: ?>
                <img srcset="<?= $bundle->baseUrl ?>/img/blauberg-group.png 1x, <?= $bundle->baseUrl ?>/img/bl1.png 2x, <?= $bundle->baseUrl ?>/img/bl2.png 3x" src="<?= $bundle->baseUrl ?>/img/blauberg-group.png" alt="Blauberg Group">
            <?php endif; ?>
        </div>
        <nav class="clearfix">
            <div class="fl">
                <?= MenuNavFlat::widget(['menuAlias' => 'header']) ?>
            </div>

            <div class="select-wr">
                <?= LangSwitch::widget() ?>
            </div>
            <form action="#" class="search-form">
                <!--<input type="search" name="search" class="input-search">-->
                <select class="input-search js-data-example-ajax">
                    <option value="3620194" name="search" selected="selected"></option>
                </select>
                <span class="search-result"></span>
            </form>
        </nav>
    </div>
</header>
<!--images/space-needle.jpg 1x, images/space-needle-2x.jpg 2x,images/space-needle-hd.jpg 3x-->

<?php
/**
 * Ajax request for search form
 */
$this->registerJs(" 
$(function () {
    $('.js-data-example-ajax').select2({        
        ajax: {
            url: '" . Url::toRoute(['/page/find/ajax']) . "',
            dataType: 'json',
            delay: 250,            
            data: function (params) {              
                return {
                    condition: params.term, // search term
                    page: params.page,  
                };
            },
            processResults: function (data, params) { 
                $('span.search-result').html(data);           
            },
            cache: true
        },  
    }); 
});", $this::POS_END);
?>