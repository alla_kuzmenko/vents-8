<?php

use frontend\themes\vents\assets\AppAsset;
use frontend\themes\vents\widgets\brands\BrandsList;
use frontend\themes\vents\modules\page\widgets\inNumbers\InNumbers;
use frontend\modules\location\widgets\CompanyMap;
use frontend\modules\page\models\Page;

$bundle = AppAsset::register($this);

$this->beginContent('@app/layouts/main.php');
?>

<?php
$this->endContent();
