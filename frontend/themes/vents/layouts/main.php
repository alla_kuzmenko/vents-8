<?php

use frontend\themes\vents\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

$bundle = AppAsset::register($this);

$this->beginPage();

$css_editor_file = substr(Yii::$app->basePath, 0, strlen(Yii::$app->basePath) - 8) . '/web/backend/editor1.css';
?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <link rel="shortcut icon" href="<?= $bundle->baseUrl ?>/favicon.ico" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="geo.placename" content="Киев, Украина"/>
    <meta name="geo.position" content="50.4501000;30.5234000"/>
    <meta name="geo.region" content="UA-город Киев"/>

    <title><?= Html::encode($this->title) ?></title>

    <script>var fv_BASE_URL = '<?= $bundle->baseUrl?>';</script>

    <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "WebSite",
          "url": "<?= Yii::$app->request->hostInfo ?>",
          "potentialAction": {
            "@type": "SearchAction",
            "target": "<?= Yii::$app->request->hostInfo ?>/find/{search}",
            "query-input": "required name=search" }
        }
    </script>

    <?php $this->head(); ?>
    <style>
        <?php if (is_file($css_editor_file)) {
            echo file_get_contents($css_editor_file);
        } ?>
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<?= $content ?>

<?= $this->render('parts/_footer', ['bundle' => $bundle]) ?>

<?php $this->endBody() ?>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-11281915-42', 'auto');
    ga('send', 'pageview');

</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter35825970 = new Ya.Metrika({
                    id: 35825970,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    trackHash: true
                });
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks"); </script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/35825970" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript> <!-- /Yandex.Metrika counter -->

</body>
</html>

<?php $this->endPage() ?>

