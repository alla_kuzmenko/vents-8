<?php

use frontend\modules\location\widgets\CompanyMap;
use frontend\themes\vents\assets\AppAsset;

$bundle = AppAsset::register($this);

$this->beginContent('@app/layouts/main.php');
?>

    <div class="header-wr">
        <?= $this->render('parts/_header', ['bundle' => $bundle]) ?>
    </div>

    <div class="top-wr learn-more">

        <?= CompanyMap::widget() ?>

    </div>
    <div class="information-container">
        <div class="information">
            <span class="title"><?= Yii::$app->getModule('configs')->getParam('11') ?></span>

            <div class="left-bl-inf"><?= Yii::$app->getModule('configs')->getParam('12') ?></div>
            <div class="right-bl-inf">
                <div class="inner-bl-inf"><?= Yii::$app->getModule('configs')->getParam('13') ?></div>
            </div>
        </div>
    </div>


<?php
$this->endContent();
