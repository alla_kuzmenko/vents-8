<?php

use frontend\themes\vents\assets\AppAsset;

$bundle = AppAsset::register($this);

$this->beginContent('@app/layouts/main.php');
?>
    <div class="error-wr">
<div class="bg-dots">
        <?= $this->render('parts/_header', ['bundle' => $bundle]) ?>

        <div class="center">
            <div class="paper"></div>
            <div class="center_block">
                <img src="<?= $bundle->baseUrl ?>/img/er-pic-5.png" alt="404">
                <p><?= Yii::t('app', 'Sorry, page not found') ?></p>
                <a href="<?= Yii::$app->request->hostInfo ?>" class="button"><?= Yii::t('app', 'To main') ?></a>
            </div>
        </div>
</div>
    </div>

<?php $this->endContent(); ?>