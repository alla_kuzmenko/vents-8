<?php

use frontend\themes\vents\assets\AppAsset;

$bundle = AppAsset::register($this);

$this->beginContent('@app/layouts/main.php');
?>
    <div class="news-wr-top">

        <?= $this->render('parts/_header', ['bundle' => $bundle]) ?>

        <?= $content ?>

    </div>

<?php $this->endContent(); ?>
