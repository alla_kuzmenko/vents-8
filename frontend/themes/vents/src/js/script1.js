// var countries = {
//    'America': {
//        		'flag1': fv_BASE_URL+'/img/en.png',
//        		'country1': [
//			            'USA',
//			            'Canada',
//			            'Cuba',
//						'aaaaaaaa',
//						'bbbbbbb'
//			             ]
//        	},
//    'Asia': {
//		        'flag1': fv_BASE_URL+'/img/ru.png',
//		        'country1': [
//		            'Russia',
//		            'aaaaaaaa',
//		            'aaaaaaaa2'
//		             ]
//        	},
//    'Europe': {
//		        'flag1': fv_BASE_URL+'/img/ua.png',
//		        'country1': [
//		            'Ukraine',
//		            'Germany',
//		            'France'
//		             ]
//        	},
//    'Afrika': {
//		        'flag1': fv_BASE_URL+'/img/en.png',
//		        'country1': [
//		            'Sudan',
//		            'Niger',
//		            'Egypt'
//		             ]
//        	},
//    'Australia': {
//		        'flag1': fv_BASE_URL+'/img/ru.png',
//		        'country1': [
//		            'Australia'
//		             ]
//        	}
//}


$(function(){
	//запуск слайдера для новостей
	$('#slider1').tinycarousel({
		axis: 'y', // vertical or horizontal scroller? 'x' or 'y' .
		animation : 'true',
		duration: 500
	});

	$('#slider3').tinycarousel({
		axis: 'x',
		controls: true
	});
	if($(window).width()<600){
		var slWidth = $('#slider3 .viewport').width();
		$('#slider3 .overview li').css('width', slWidth-1);
		//$('#slider3 .overview li img').css('width', slWidth-3);
	}




	var index = $('#menu');
	if (index.length>0){
		var html = $('#menu').html();
	    var content = tmpl(html, countries);  
	    $('.select-menu ul').append(content);   
	}
//////////////////////////////////////////////////////////////
	//Подгружение картинок после загрузки DOM
	var videoLink = $('.video').attr('data-video-url');
	var source = {
		mainBg: fv_BASE_URL+"/img/startBg.jpg",
		footerBg: fv_BASE_URL+"/img/bg-bottom.png",
		errorBg: fv_BASE_URL+"/img/404bg1.png",
		aboutBg: fv_BASE_URL+"/img/aboutBg.jpg",
		//newsBg: fv_BASE_URL+"/img/news-bg.png",
		mapBg: fv_BASE_URL+"/img/sitemap-bg.png",
		//contactBg: fv_BASE_URL+"/img/contact_map.jpg",
		dot:fv_BASE_URL+"/img/dot.png",
		video: "<iframe width='auto' height='auto' src='"+videoLink+"' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"
	};

	$('.center-wr').css('backgroundImage','url('+source.mainBg+')');
	$('footer').css('backgroundImage','url('+source.footerBg+')');
	$('.error-wr').css('backgroundImage','url('+source.errorBg+')');
	$('.about-wr').css('backgroundImage','url('+source.aboutBg+')');
	$('.map-wr').css('backgroundImage','url('+source.mapBg+')');
	//$('.contact-wr').css('backgroundImage','url('+source.contactBg+')');

	console.log(source.video);
	$('.video').html(source.video);

	///////////////////////////////////////////////
	//Слайдер новостей
	var src_first = $('#slider1 li').first().children().attr('src');
	$('.sl-big img').attr('src', src_first);
	$('#slider1 li').on('click', function(){
		var src = $(this).children('img').attr('src');
		$('.sl-big img').attr('src', src);
	});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//раскрыть селект

	$('.country').on('click', function(){
		$('.main-title').css('width','100%');
		//$('.top-wr .country').css('width','35%');
		$('.select-menu').slideToggle();
		$('.part-w').first().children('.sub-menu').fadeIn(1000);
		$('.part-w').first().addClass('active');
	});

	//при наведении выпадает подменю
	$('.part-w').hover(
		function(){ 
			$(this).addClass('active');
			$('.part-w').not(this).removeClass('active');
			$(this).children('.sub-menu').show();
				},
		function(){
			$(this).removeClass('active');
			$(this).children('.sub-menu').hide();
				}
	);

	//при покидании области меню, оно прячется
	$('.select-menu').on('mouseleave', function(){
		$(this).slideUp();
		$('.main-title').css('width','465px');
	});

	//$('.language').on('mouseleave', function(){
	//	$(this).slideUp();
	//});

	//при нажатии меняется страна
	$('.select-menu a').on('click', changeCountry);
	
		function changeCountry(){
			var title = $(this).html();
			//var titlefl = $(this).attr('style');

			$('.country').html(title);
			//$('.country').attr('style', titlefl);
			//$('.country').css('position', '');
			$('.select-menu').hide();
			$('.main-title').css('width','465px');
			$('.top-wr .country').css('paddingTop','18px');
		}


	/////////////////////////
	$('.language li').first().addClass('active');
	// Смена языка
	$('.selected').on('click', function(){
		//$('.language').show();
		$(this).siblings('.language').slideToggle();
	});
	$('.language li').on('click', function(){
		var lang_icon = $(this).children('a').attr('style');
		var lang_text = $(this).children('a').html();
		$('.selected').attr('style', lang_icon);
		$('.selected').html(lang_text);
		$(this).addClass('active');
		$('.language li').not(this).removeClass('active');
		$('.language').slideUp();
	});
////////////////////////////////////////////////////////////////////////////////////////////
    //Добавляем кнопку для меню - меню будет сложено в столбик
	$('.menuBut').click(function(){
		$('.menuSmall').show();
	});
	$('.newMenu .menuSmall').on('mouseleave', function(){
		$(this).slideUp();
	});


	$('.products').hover(function(){
		$(this).children('.products-under').show();
		$(this).children('.products-under').css('height', 'auto');
		$(this).children('.products-under').children('.transp').hide();
		$(this).children('.products-over').hide();
	},
			function(){
				$(this).children('.products-over').show();
				$(this).children('.products-under').hide();
			}
	);
//Закрытие popup
	$('.popup .close').on('click', function(){
		$(this).parent().parent().hide();
	});
	$('.popup .button').on('click', function(){
		$(this).parent().parent().hide();
	});
	// Манипулящии с перемещением селекта
	//$('.search-form').on('click', function(){
	//	console.log('hello');
	//	$('.select2-container--open').css('top','60px');
	//});
	//ДОБавление figure в редакторе
	$('.editor img').each(function(index, item){

		if($(this).css('float') == 'left'){

			$(this).wrap('<figure></figure>').parent().append($('<figcaption>'+item.alt+'</figcaption>'));
			$(this).css('float', 'none');
			$(this).parent().css('float', 'left');
		}
		else if($(this).css('float') == 'right'){

			$(this).wrap('<figure></figure>').parent().append($('<figcaption>'+item.alt+'</figcaption>'));
			$(this).css('float', 'none');
			$(this).parent().css('float', 'right');
		}
		else{
			$(this).wrap('<figure></figure>').parent().append($('<figcaption>'+item.alt+'</figcaption>'));
		}
	});
});