
$(function(){
	//запуск слайдера для новостей
	$('#slider1').tinycarousel({
		axis: 'y', // vertical or horizontal scroller? 'x' or 'y' .
		animation : 'true',
		duration: 500
	});
	var cachedWidth1 = $(window).width();

			$('#slider3').tinycarousel({
				axis: 'x',
				controls: true
			});

	//if($(window).width()<700){
	var slWidth = $('#slider3 .viewport').width();
	$('#slider3 .overview li').css('width', slWidth-1);
	//console.log(slWidth);
	//$('#slider3 .overview li img').css('width', slWidth-3);
	//}
	function windowSize() {
		var newWidth1 = $(window).width();

		if (newWidth1 !== cachedWidth1) {

			var slWidth = $('#slider3 .viewport').width();
			$('#slider3 .overview li').css('width', slWidth-1);
			//console.log(slWidth);

			$('#slider3').tinycarousel({
				axis: 'x',
				controls: true
			});
			cachedWidth1 = newWidth1;
		}
	}

	$(window).resize(windowSize);


	var index = $('#menu');
	if (index.length>0){
		var html = $('#menu').html();
	    var content = tmpl(html, countries);  
	    $('.select-menu ul').append(content);   
	}
//////////////////////////////////////////////////////////////
	//Подгружение картинок после загрузки DOM
	var videoLink = $('.video').attr('data-video-url');
	var source = {
		mainBg: fv_BASE_URL+"/img/startBg.jpg",
		footerBg: fv_BASE_URL+"/img/bg-bottom.png",
		errorBg: fv_BASE_URL+"/img/404bg.png",
		aboutBg: fv_BASE_URL+"/img/aboutBg.jpg",
		//newsBg: fv_BASE_URL+"/img/news-bg.png",
		mapBg: fv_BASE_URL+"/img/sitemap-bg.png",
		//contactBg: fv_BASE_URL+"/img/contact_map.jpg",
		dot:fv_BASE_URL+"/img/dot.png",
		video: "<iframe width='auto' height='auto' src='"+videoLink+"' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"
	};

	$('.center-wr').css('backgroundImage','url('+source.mainBg+')');
	$('footer').css('backgroundImage','url('+source.footerBg+')');
	$('.error-wr').css('backgroundImage','url('+source.errorBg+')');
	$('.about-wr').css('backgroundImage','url('+source.aboutBg+')');
	$('.map-wr').css('backgroundImage','url('+source.mapBg+')');
	//$('.contact-wr').css('backgroundImage','url('+source.contactBg+')');

	//console.log(source.video);
	$('.video').html(source.video);

	///////////////////////////////////////////////
	//Слайдер новостей
	var src_first = $('#slider1 li').first().children().attr('src');
	$('.sl-big img').attr('src', src_first);
	$('#slider1 li').on('click', function(){
		var src = $(this).children('img').attr('src');
		$('.sl-big img').attr('src', src);
	});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//раскрыть селект

	$('.magic-select').on('click', function(e){
		e.stopPropagation();
		//$('.main-title').css('width','100%');
		//$('.top-wr .country').css('width','35%');
		$(this).children('.select-menu').slideToggle();
	});


	//при покидании области меню, оно прячется
	$('.select-menu').on('mouseleave', function(){
		$(this).slideUp();
		//$('.main-title').css('width','465px');
	});

	//при нажатии меняется страна
	$('.select-menu a').on('click', changeCountry);
	
		function changeCountry(){
			var title = $(this).html();
			//var titlefl = $(this).attr('style');

			$('.country').html(title);
			//$('.country').attr('style', titlefl);
			//$('.country').css('position', '');
			//$('.select-menu').hide();
			//$('.main-title').css('width','465px');
			$('.top-wr .country').css('paddingTop','18px');
		}


	/////////////////////////
	$('.language li').first().addClass('active');
	// Смена языка
	$('.selected').on('click', function(){
		//$('.language').show();
		$(this).siblings('.language').slideToggle();
	});
	$('.language li').on('click', function(){
		var href1 = $(this).children('a').attr('href');
		//console.log(href1);
		location.href = href1;
	});
////////////////////////////////////////////////////////////////////////////////////////////
    //Добавляем кнопку для меню - меню будет сложено в столбик
	$('.menuBut').click(function(){
		$('.menuSmall').show();
	});
	$('.newMenu .menuSmall').on('mouseleave', function(){
		$(this).slideUp();
	});


	$('.products').hover(function(){
		$(this).children('.products-under').show();
		$(this).children('.products-under').css('height', 'auto');
		$(this).children('.products-under').children('.transp').hide();
		$(this).children('.products-over').hide();
	},
			function(){
				$(this).children('.products-over').show();
				$(this).children('.products-under').hide();
			}
	);
//Закрытие popup
	$('.popup .close').on('click', function(){
		$(this).parent().parent().hide();
	});
	$('.popup .button').on('click', function(){
		$(this).parent().parent().hide();
	});
	// Манипулящии с перемещением селекта
	//$('.search-form').on('click', function(){
	//	console.log('hello');
	//	$('.select2-container--open').css('top','60px');
	//});
	//ДОБавление figure в редакторе
	$('.editor img').each(function(index, item){

		if($(this).css('float') == 'left'){

			$(this).wrap('<figure></figure>').parent().append($('<figcaption>'+item.alt+'</figcaption>'));
			$(this).css('float', 'none');
			$(this).parent().css('float', 'left');
		}
		else if($(this).css('float') == 'right'){

			$(this).wrap('<figure></figure>').parent().append($('<figcaption>'+item.alt+'</figcaption>'));
			$(this).css('float', 'none');
			$(this).parent().css('float', 'right');
		}
		else{
			$(this).wrap('<figure></figure>').parent().append($('<figcaption>'+item.alt+'</figcaption>'));
		}
	});
	$('body').click(function(){
		$('.dropdown-search').hide();
	});
	$('.dropdown-search li').click(function(){
		$('.dropdown-search').css('marginTop', '0');
	});

	//Добавление высоты страницы перечня новостей начиная со второй страницы
	var page = window.location.toString().slice(40);
	console.log(page);
	var calcpx = 'calc(100vh - 424px)';
	if(+page > 1){
		$('.news-container').css('minHeight', calcpx);
	}

});