<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2016, Thread
 */
return [
    'mailer' => [
        'useFileTransport' => false,
        'transport' => [
            'class' => \Swift_SmtpTransport::class,
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // MAIL SETTINGS
            'host' => 'mail.blaubergventilatoren.de',
            'username' => 'office@blauberg-group.com',
            'password' => 'GdjjTk5!df9fSzzL',
            'port' => '25',
        ],
    ],
    'user' => [
        'enableAutoLogin' => false,
    ],
    'urlManager' => [
        'rules' => require __DIR__ . '/part/url-rules.php',
    ],
    'errorHandler' => [
        'errorAction' => 'home/home/error',
    ],
    'request' => [
        'class' => \thread\app\web\Request::class,
        'enableCsrfValidation' => true,
        'enableCookieValidation' => true,
        'cookieValidationKey' => 'thread',
    ],
    'view' => [
        'theme' => [
           'baseUrl' => '@app/themes/vents',
            'pathMap' => [
                '@app/layouts' => '@app/themes/vents/layouts',
                '@app/modules' => '@app/themes/vents/modules',
            ],
        ],
    ],
    'oldbrowser' => [
        'class' => \frontend\components\oldbrowser\OldBrowser::class,
        'browsers' => [
            'Firefox' => 35,
            'Mozilla' => 30,
            'Safari' => 7,
            'Opera' => 15,
            'Chrome' => 25,
            'IE' => 9,
        ],
    ],
    'geolocation' => [
        'class' => \frontend\components\geolocation\Geolocation::class,
        'config' => [
            'provider' => 'geoplugin',
            'return_formats' =>  'php'
        ],
    ],
    'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => \himiklab\yii2\recaptcha\ReCaptcha::class,
            'siteKey' => '6LffASYTAAAAAOzNPp76vwu77r5GLyMMLu1d11TQ',
            'secret' => '6LffASYTAAAAANIVLNvgace4RMTSz6y2orbJiUOy'
    ],
    'i18n' => [
        'translations' => [
            'app' => [
                'class' => \yii\i18n\PhpMessageSource::class,
                'basePath' => '@frontend/messages',
                'fileMap' => [
                    'app' => 'app.php',
                ]
            ],
            'form' => [
                'class' => \yii\i18n\PhpMessageSource::class,
                'basePath' => '@frontend/messages',
                'fileMap' => [
                    'form' => 'form.php',
                ]
            ],
        ]
    ],

];
