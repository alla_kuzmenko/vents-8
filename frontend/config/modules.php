<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
return [
    /**
     * CORE
     */
    'home' => [
        'class' => \frontend\modules\home\Home::class,
    ],
    'configs' => [
        'class' => \frontend\modules\configs\Configs::class,
    ],
    'user' => [
        'class' => \frontend\modules\user\User::class,
    ],
    'page' => [
        'class' => \frontend\modules\page\Page::class,
    ],
    'news' => [
        'class' => \frontend\modules\news\News::class,
    ],
    'brands' => [
        'class' => \frontend\modules\brands\Brands::class,
    ],
    'aboutus' => [
        'class' => \frontend\modules\aboutus\Aboutus::class,
    ],
    'contacts' => [
        'class' => \frontend\modules\contacts\Contacts::class,
    ],
    'forms' => [
        'class' => \frontend\modules\forms\Forms::class,
    ],

    'seo' => [
        'class' => \frontend\modules\seo\Seo::class,
        'objects' => [
            'page' => [
                'page' => [
                    'class' => \frontend\modules\page\models\Page::class,
                    'site_map_method' => 'findSiteMap',
                    'robots_method' => 'findRobots',
                ],
            ],
            'news' => [
                'news' => [
                    'class' => \frontend\modules\news\models\Article::class,
                    'site_map_method' => 'findSiteMap',
                    'robots_method' => 'findRobots',
                ],
            ],
        ]
    ],
];
