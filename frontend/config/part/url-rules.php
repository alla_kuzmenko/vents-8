<?php
return [
    'gii' => 'gii',
    'error_404' => 'home/home/error',

    // Module [[Home]]
    '' => 'home/home/index',

    // Module [[Users]]
    'register' => 'user/register',
    'login' => 'user/login',
    'logout' => 'user/logout',
    'change-password' => 'user/profile/password-change',
    'profile' => 'user/profile/index',
    'request-password-reset' => 'user/profile/request-password-reset',
    'reset-password' => 'user/profile/reset-password',

    // Module [[Brands]]
    'brands' => 'brands/list/index',

    // Module [[Aboutus]]
    'about' => 'aboutus/list/index',

    // Module [[contacts]]
    'contacts' => 'contacts/contact/index',
    'contacts/contact/index' => 'contacts/contact/index',

    // Module [[News]]
//    'news/<alias:[\w\-]+>' => 'news/list/index',
    'news/<alias:[\w\-]+>' => 'news/article/index',
    'news' => 'news/list/index',

    // Module [[Page]]

    'find/ajax' => 'page/find/ajax',
    'find' => 'page/find/index',
    'find/<condition:[\w\-]*>' => 'page/find/index',
    '<alias:[\w\-]+>' => 'page/page/view',

    // Module [[Forms]]
    'forms/feedbackform/captcha' => 'forms/feedbackform/captcha',
    'forms/feedbackform/add' => 'forms/feedbackform/add',

    // Module [[SEO]]
    'sitemap/filling' => 'seo/sitemap/filling',
    'sitemap/mapcreate' => 'seo/sitemap/mapcreate',
    'robots/filling' => 'seo/robots/filling',
    'robots/create' => 'seo/robots/create',
    'sitemap' => 'page/page/sitemap',
];
