<?php

namespace frontend\modules\brands\models;

use Yii;

/**
 * Class BrandsItem
 *
 * @package frontend\modules\brands\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class BrandsItem extends \common\modules\brands\models\BrandsItem
{

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     *
     * @return yii\db\ActiveQuery
     */
    public static function find_base()
    {
        return self::find()->innerJoinWith(["lang"])->enabled();
    }

    /**
     * Using for query brand elements on index page
     * @return mixed
     */
    public static function findForIndexPage()
    {
        return self::find_base()->on_index()->orderBy(['position' => SORT_ASC]);
    }
}
