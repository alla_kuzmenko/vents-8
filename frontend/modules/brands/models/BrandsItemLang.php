<?php

namespace frontend\modules\brands\models;

/**
 * Class BrandsItemLang
 *
 * @package frontend\modules\brands\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class BrandsItemLang extends \common\modules\brands\models\BrandsItemLang {

    /**
     * 
     * @return array
     */
    public function behaviors() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function scenarios() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function attributeLabels() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function rules() {
        return [];
    }

}
