<?php

namespace frontend\modules\brands;

/**
 * Class Brands
 *
 * @package frontend\modules\brands
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Brands extends \backend\modules\brands\Brands {
    /**
     * Number of elements in GridView
     * @var int
     */
    public $itemOnPage = 6;
}
