<?php

use yii\helpers\Html;

/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
?>

<div class="brand-text">
    <div><img src="<?= $article->getArticleImage() ?>" alt="<?= Html::encode($article['lang']['title']) ?>"></div>
    <p class="description"><?= Html::encode(strip_tags($article['lang']['description'])) ?></p>
    <a href="<?= Html::encode($article['link']) ?>" class="button"
       target="_blank"><?= Yii::t('app', 'Visit website') ?></a>
</div>
