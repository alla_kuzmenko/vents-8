<?php
/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
 $this->title = 'Brands';
?>

<?php foreach ($models as $article): ?>
    <?= $this->render('_list_item', ['article' => $article]) ?>
<?php endforeach; ?>
