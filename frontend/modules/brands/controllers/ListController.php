<?php

namespace frontend\modules\brands\controllers;

use frontend\components\BaseController;
use thread\actions\ListQuery;
use frontend\modules\brands\models\BrandsItem;

/**
 * Class ListController
 *
 * @package frontend\modules\news\controllers
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class ListController extends BaseController
{

    public $title = "Brands List";

    public $defaultAction = 'index';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                ],
            ],
        ];
    }

    /**
     *
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => ListQuery::class,
                'query' => BrandsItem::find_base(),
                'recordOnPage' => $this->module->itemOnPage,
                'layout' => '/brands'
            ],
        ];
    }

}
