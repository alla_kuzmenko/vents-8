<?php

use frontend\components\BaseController;
use frontend\modules\seo\models\Seo;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\themes\vents\widgets\share\YandexShareWidget;
use frontend\themes\vents\widgets\news\SimilarNews;
use yii\widgets\Breadcrumbs;

/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
//$this->title = $model['lang']['title'];
echo Yii::$app->controller->renderPartial('/part/seo',['article' => $model]);

$this->context->breadcrumbs['news'] = [
    'label' => Yii::t('app', 'News'),
    'url' => Url::toRoute(['/news/list/index'])
];
$this->context->breadcrumbs['item'] = [
    'label' => Yii::t('app', $model['lang']['title']),
    'url' => $model->getUrl()
];

/** SEO */
Seo::getSeoParams( (isset($model) ? $model : '') );
Seo::registerMetaTag($this);
?>
        <!--Переход в админку-->
        <?= BaseController::getHtmlUrlToBack($model) ?>

        <?=
        Breadcrumbs::widget([
            'links' => $this->context->breadcrumbs
        ])
        ?>

        <div class="news-part1 clearfix">
            <div class="news-img">
                <figure class="img1">
                    <?php if ($model->getArticleImage()): ?>
                        <img src="<?= $model->getArticleImage() ?>" alt="news" srcset="<?= $model->getArticleImage() ?> 620w, <?= $model->getArticleImage() ?> 540w, <?= $model->getArticleImage() ?> 320w" sizes="(min-width:1200px) 620px, (min-width:1000px) 580px, (min-width:620px)  530px, 400px">
                    <?php endif; ?>
                </figure>
            </div>
            <div class="news-body">
                <div class="date"><?= $model->getPublishedTime(); ?></div>
                <div class="title"><?= $model['lang']['title'] ?></div>
                <div class="desc"><?= $model['lang']['description'] ?></div>
            </div>
        </div>
    </div>
</div>

<div class="news-body-center clearfix">
    <div class="left-part">
        <div class="clearfix editor">

            <?= $model['lang']['content']; ?>

        </div>

        <?php if (!empty($model->getArticleGallery())): ?>
            <section class="news-slider clearfix">
                <h2><?= Yii::t('app', 'Event gallery') ?>:</h2>
                <div class="sliders">
                <div class="sl-big"><img src="" alt="photo"></div>
                <div id="slider1">
                    <a class="buttons prev" href="#">left</a>
                    <div class="viewport">
                        <ul class="overview">
                            <?php foreach($model->getArticleGallery() as $img): ?>
                                <li><img src="<?= $img ?>" /></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <a class="buttons next" href="#">right</a>
                </div>
                <div id="slider3">
                    <a class="buttons prev" href="#"> </a>
                    <div class="viewport">
                        <ul class="overview">
                            <?php foreach($model->getArticleGallery() as $img): ?>
                                <li><img src="<?= $img ?>" /></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <a class="buttons next" href="#"> </a>
                </div>
                    </div>
            </section>
        <?php endif; ?>

        <div class="social"><?= Yii::t('app', 'Share') ?>:
            <?=
            YandexShareWidget::widget([
                'services' => 'facebook,twitter,gplus',
                'meta' => [
                    //'fb_app_id' => '150935738345402',
                    'type' => 'none',
                    'image' => $model->getArticleImage(),
                    'title' => Html::encode($model['lang']['title']),
                    'description' => Html::encode(strip_tags($model['lang']['description'])),
                    'url' => Yii::$app->getRequest()->hostInfo . Url::current(),
                ]
            ])
            ?>
        </div>
    </div>

    <div class="right-part">

        <div class="similar"><a href="<?= Url::toRoute(['/news/list/index']) ?>"><?= Yii::t('app', 'Other news') ?></a>:</div>
        <?= SimilarNews::widget() ?>

    </div>