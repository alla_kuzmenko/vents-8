<?php

use yii\helpers\Html;

/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
?>
<div class="new_s">
<div class="news">
    <a href="<?= $article->getUrl() ?>">
        <figure>
            <div class="upshadow"></div>
            <?php if ($article->getArticleImage()): ?>
                <img src="<?= $article->getArticleImage() ?>"
                     alt="news"
                     srcset="<?= $article->getArticleImage() ?> 620w,
                             <?= $article->getArticleImage() ?> 540w,
                             <?= $article->getArticleImage() ?> 320w"
                     sizes="(min-width:1200px) 620px,
                            (min-width:1000px) 430px,
                            (min-width:620px)  580px, 280px">
            <?php endif; ?>
            <figcaption>
                <p class="date"><?= $article->getPublishedTime() ?></p>
                <p class="title"><?= Html::encode($article['lang']['title']) ?></p>
            </figcaption>
        </figure>
        <div class="desc"><?= $article['lang']['description'] ?></div>
    </a>
</div>
</div>