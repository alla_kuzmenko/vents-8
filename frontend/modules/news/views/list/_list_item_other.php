<?php

use yii\helpers\Html;

/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
?>

<div class="news clearfix">
    <a href="<?= $article->getUrl() ?>">
        <?php if ($article->getArticleImage()): ?>
            <img src="<?= $article->getArticleImage() ?>">
        <?php endif; ?>
        <div class="date"><?= $article->getPublishedTime() ?></div>
        <div class="title"><?= Html::encode($article['lang']['title']) ?></div>
        <div class="desc"><?= $article['lang']['description'] ?></div>
    </a>
</div>