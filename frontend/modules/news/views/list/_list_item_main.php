<?php

use yii\helpers\Html;

/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
?>

<a href="<?= $article->getUrl() ?>">
    <?php if ($article->getArticleImage()): ?>
        <figure>
            <div class="upshadow"></div>
            <img class="cover"
             src="<?= $article->getArticleImage() ?>"
             alt="the best"
             srcset="<?= $article->getArticleImage() ?> 620w,
                        <?= $article->getArticleImage() ?> 540w,
                        <?= $article->getArticleImage() ?> 320w">
            <figcaption class="caption">
                <div class="date"><?= $article->getPublishedTime() ?></div>
                <div class="title"><?= Html::encode($article['lang']['title']) ?></div>
                <div class="desc"><?= $article['lang']['description'] ?></div>
            </figcaption>
        </figure>
    <?php endif; ?>

</a>
