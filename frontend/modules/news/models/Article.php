<?php

namespace frontend\modules\news\models;

use frontend\modules\configs\models\Language;
use yii\helpers\Url;
use Yii;

/**
 * Class Article
 *
 * @package frontend\modules\news\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Article extends \backend\modules\news\models\Article
{

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     *
     * @return yii\db\ActiveQuery
     */
    public static function find_base()
    {
        return self::find()->innerJoinWith(["lang"])->enabled()->orderBy(['published_time' => SORT_DESC]);
    }

    /**
     *
     * @param integer $id
     * @return ActiveRecord|null
     */
    public static function findById($id)
    {
        return self::find_base()->byID($id)->one();
    }

    /**
     *
     * @param integer $group_id
     * @return ActiveRecord|null
     */
    public static function findByGroupId($group = '')
    {
        return self::find_base()->/*group_id($group)->*/
        all();
    }

    /**
     *
     * @param string $alias
     * @return ActiveRecord|null
     */
    public static function findByAlias($alias)
    {
        return self::find_base()->alias($alias)->one();
    }

//    /**
//     *
//     * @return string
//     */
//    public function getUrl() {
//        return Url::toRoute(['/news/article/index', 'alias' => $this->alias]);
//    }
    /**
     * @return string
     */
    public function getPublishedTime()
    {
        return date('d', $this->published_time) . ' ' . self::_getMonthName($this->published_time) . ' ' . date('Y', $this->published_time);
    }

    /**
     * @param $Time
     * @return mixed
     */
    public static function _getMonthName($Time)
    {

        $month = intval(date("m", $Time));

        $m = (Yii::$app->params['MonthName'][Yii::$app->language])??Yii::$app->params['MonthName']['default'];

//        switch (Yii::$app->language) {
//            case "uk-UA" :
//                $m = array(1 => "січня", 2 => "лютого", 3 => "березеня", 4 => "квітня", 5 => "травня", 6 => "червня", 7 => "липня", 8 => "серпня", 9 => "вересеня", 10 => "жовтня", 11 => "листопада", 12 => "грудня");
//                break;
//            case "ru-RU" :
//                $m = array(1 => "января", 2 => "февраля", 3 => "марта", 4 => "апреля", 5 => "мая", 6 => "июня", 7 => "июля", 8 => "августа", 9 => "сентября", 10 => "октября", 11 => "ноября", 12 => "декабря");
//                break;
//            default :
//                $m = array(1 => "january", 2 => "february", 3 => "march", 4 => "april", 5 => "may", 6 => "june", 7 => "july", 8 => "august", 9 => "september", 10 => "october", 11 => "november", 12 => "december");
//        }
        return $m[$month];
    }

    /**
     * Url в админку
     */
    public function getUrlToBack()
    {
        $url = '/backend/{lang}news/article/update?id=' . $this->id;

        $lanModel = Language::getCurrentModel();

        if ($lanModel) {
            $url = ($lanModel['default']) ? str_replace('{lang}', '', $url) : str_replace('{lang}', $lanModel['alias'] . '/', $url);
        }

        return $url;
    }


    /**
     * @return string
     */
    public function getUrl($schema = false)
    {
        return Url::toRoute(['/news/article/index', 'alias' => $this->alias], $schema);
    }


    /**
     * Method for sitemap generation
     * @return mixed
     */
    public static function findSiteMap()
    {
        return self::findBase()->joinWith('seo')
            ->andWhere(['Like', '{{%seo}}.model_namespace', self::getModelNamespace()])
            ->andWhere(['{{%seo}}.in_site_map' => 1]);
    }

    /**
     * Method for robots.txt generation
     * @return mixed
     */
    public static function findRobots()
    {
        return self::findBase()->joinWith('seo')
            ->andWhere(['Like', '{{%seo}}.model_namespace', self::getModelNamespace()])
            ->andWhere(['{{%seo}}.in_robots' => 1]);
    }
}
