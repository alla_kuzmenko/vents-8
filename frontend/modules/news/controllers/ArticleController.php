<?php

namespace frontend\modules\news\controllers;

use thread\actions\RecordView;
use frontend\modules\news\models\Article;
use Yii;
use yii\helpers\Url;

/**
 * Class ArticleController
 *
 * @package frontend\modules\news\controllers
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class ArticleController extends \frontend\components\BaseController
{

    public $title = "Article";
    public $layout = "@app/layouts/column1";
    public $defaultAction = 'index';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                ],
            ],
            [
                'class' => \yii\filters\HttpCache::class,
                'only' => ['index'],
                'lastModified' => function ($action, $params) {
                    $q = Article::findByAlias(Yii::$app->getRequest()->get('alias'));
                    return ($q !== null) ? $q['updated_at'] : time();
                },
            ],
        ];
    }

    /**
     *
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => RecordView::class,
                'modelClass' => Article::class,
                'methodName' => 'findByAlias',
                'view' => 'article',
                'layout' => '/news'
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function init()
    {
        $item = Article::findByAlias(Yii::$app->request->get('alias'));
        if ($item === null) {
            Header('Location: ' . Url::toRoute(['/home/home/error']), 301);
            exit;
        }
        return parent::init();
    }

    /**
     * @return \frontend\modules\news\models\ActiveRecord|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getModel()
    {
        return Article::findByAlias(Yii::$app->getRequest()->get('alias', 0));
    }
}
