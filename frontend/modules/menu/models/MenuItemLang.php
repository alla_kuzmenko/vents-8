<?php

namespace frontend\modules\menu\models;

/**
 * Class ItemLang
 *
 * @package frontend\modules\menu\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class MenuItemLang extends \backend\modules\menu\models\MenuItemLang {

    /**
     * 
     * @return array
     */
    public function behaviors() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function scenarios() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function attributeLabels() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function rules() {
        return [];
    }

}
