<?php

namespace frontend\modules\contacts;

use Yii;

/**
 * Class Contacts
 * @author Alla Kuzmenko
 * @copyright (c) 2016
 */
class Contacts extends \backend\modules\contacts\Contacts
{
    /**
     * Number of elements in GridView
     * @var int
     */
    public $itemOnPage = 999;
}
