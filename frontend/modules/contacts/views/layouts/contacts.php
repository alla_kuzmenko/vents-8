<?php
use frontend\themes\vents\assets\AppAsset;

$bundle = AppAsset::register($this);
Yii::$app->view->registerJsFile($bundle->baseUrl . '/js/map/map1.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->beginContent('@app/layouts/main.php');
?>

   
    <?php echo $content ?>
    <div class="form-wr">

        <div class="form">
            <?= \frontend\modules\page\widgets\Feedback\Feedback::widget() ?>
        </div>
    </div>
    <!-- Контакты -->
    <script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "Organization",
 "url": "<?= Yii::$app->getUrlManager()->getHostInfo(); ?>",
 "logo": {
       "@type": "ImageObject",
       "url": "<?= Yii::$app->getUrlManager()->getHostInfo() . $bundle->baseUrl ?>/img/logo.png",
       "width": "174",
       "height": "77"
       },
 "contactPoint": [{
       "@type": "ContactPoint",
       "telephone": "+38(044)406-36-27",
       "contactType": "customer service"
       },
       {
       "@type": "ContactPoint",
       "telephone": "+3800-501-74-80",
       "contactType": "customer service"
       }],

   "location": {
           "@type": "PostalAddress",
           "addressLocality": "Ukraine",
           "addressRegion": "Kiev",
           "postalCode": "01030",
           "streetAddress": "Kotsyubyns'kogo street 1"
           }

}
</script>
<?php
echo $this->registerJs(" $(function () {

        $('body').on('submit', '#feedback-form', function () {
            var form = $(this);
            //console.log(form);

            $.ajax({
                url: \"/forms/feedbackform/add\",
                type: \"POST\",
                dataType: \"json\",
                data: form.serialize(),
                success: function (data) {
                    if (data.success == 0) {
                        $('#feedback-form').find('.errors').html('');
                        for (var i = 0; i < data.fields.length; i++) {
                            var errId = '.'+data.fields[i]['field']+'-error';
                            $('#feedback-form').find(errId).html(data.fields[i]['val']);
                        }

                    } 
                    else {                   
                        $('#feedback-form')[0].reset(); 
                        $('.popup').show();
                        

                    }
                }
            });
            return false;

        });
    });");
?>
<?php $this->endContent(); ?>