<?php
use frontend\modules\seo\models\Seo;
use frontend\themes\vents\assets\AppAsset;
use yii\widgets\Breadcrumbs;

$bundle = AppAsset::register($this);
if (!empty($models[0])) {
    $model = $models[0];
}
$this->title = Yii::t('app', 'Contacts');

$this->context->breadcrumbs[] = [
    'label' => $this->title,
    'url' => ''
];

/** SEO */
$seo = Seo::getSeoParams($model);

if (isset($seo)) {
    $this->title = $seo['title'];
    $this->registerMetaTag(['name' => 'description', 'content' => $seo['description']]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $seo['keywords']]);
}
?>
<?php if (!empty($model)) : ?>
    <div class="contact-wr" style="background-image: url('<?= $model->lang->getImage() ?>')">
        <div class="content">
            <?= $this->render('@app/layouts/parts/_header', ['bundle' => $bundle]) ?>

            <?=
            Breadcrumbs::widget([
                'links' => $this->context->breadcrumbs
            ])
            ?>

            <div class="contact">

                <div class="left-part">
                    <div class="left-corner"></div>
                    <div class="line"></div>
                </div>

                <h1><?= Yii::t('app', 'Contacts') ?></h1>

                <div class="branch clearfix">
                    <?php if (!empty($model->lang->delivery_phone)) : ?>
                        <div
                            class="delivery"><?= Yii::t('app', 'For delivery of products on the territory of Ukraine') ?>
                            :
                            <?php
                            $phones = explode(',', $model->lang->delivery_phone);
                            if (count($phones) > 1) {
                                foreach ($phones as $phone) {
                                    echo '<span class="phone-number">' . $phone . '</span>';
                                }
                            } else {
                                echo '<span class="phone-number">' . $model->lang->delivery_phone . '</span>';
                            }

                            ?>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($model->lang->customer_phone)) : ?>
                        <div class="custom"><?= Yii::t('app', 'Customer Service') ?>:
                            <?php
                            $phones = explode(',', $model->lang->customer_phone);
                            if (count($phones) > 1) {
                                foreach ($phones as $phone) {
                                    echo '<span class="pd phone-number">' . $phone . '</span>';
                                }
                            } else {
                                echo '<span class="pd phone-number">' . $model->lang->customer_phone . '</span>';
                            }

                            ?>
                            <span class="free"><?= $model->lang->customer_info ?></span>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($model->lang->export_phone)) : ?>
                        <div class="export"><?= Yii::t('app', 'export Department') ?>:
                            <?php
                            $phones = explode(',', $model->lang->export_phone);
                            if (count($phones) > 1) {
                                foreach ($phones as $phone) {
                                    echo '<span class="phone-number">' . $phone . '</span>';
                                }
                            } else {
                                echo '<span class="phone-number">' . $model->lang->export_phone . '</span>';
                            }

                            ?>

                        </div>
                    <?php endif; ?>

                    <?php if (!empty($model->lang->hr_phone)) : ?>
                        <div class="cadr"><?= Yii::t('app', 'Human Resources Department') ?>:
                            <?php
                            $phones = explode(',', $model->lang->hr_phone);
                            if (count($phones) > 1) {
                                foreach ($phones as $phone) {
                                    echo '<span class="phone-number">' . $phone . '</span>';
                                }
                            } else {
                                echo '<span class="phone-number">' . $model->lang->hr_phone . '</span>';
                            }

                            ?>
                        </div>
                    <?php endif; ?>

                </div>
                <?php if (!empty($model->lang->address)) : ?>
                    <div class="adr"><?= $model->lang->address ?></div>
                <?php endif; ?>
                <?php if (!empty($model->lang->emails)) : ?>
                    <div class="cont-email"><?= $model->lang->emails ?></div>
                <?php endif; ?>
                <div class="right-part">
                    <div class="line"></div>
                    <div class="right-corner"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php
/*
?>

                    <?php if (!empty($model->lang->hr_phone)) : ?>
                        <div class="cadr"><?= Yii::t('app', 'Human Resources Department') ?>:
                            <?php
                            $phones = explode(',', $model->lang->hr_phone);
                            if (count($phones) > 1) {
                                foreach ($phones as $phone) {
                                    echo '<span class="phone-number">' . $phone . '</span>';
                                }
                            } else {
                                echo '<span class="phone-number">' . $model->lang->hr_phone . '</span>';
                            }

                            ?>
                        </div>
                    <?php endif; ?>

*/