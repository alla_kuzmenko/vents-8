<?php
namespace frontend\modules\contacts\controllers;

use Yii;
//
use thread\actions\ListQuery;
//
use frontend\modules\contacts\models\Contact;
use frontend\components\BaseController;


/**
 * Class ContactController
 *
 * @package frontend\modules\contacts\controllers
 * @author Alla Kuzmenko
 * @copyright (c) 2016
 */
class ContactController extends BaseController
{

    public $title = 'Contacts';

    public $defaultAction = 'index';

    /**
     *
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => ListQuery::class,
                'query' => Contact::find_base(),
                'recordOnPage' => $this->module->itemOnPage,
                'layout' => 'contacts'
            ],
        ];
    }

}
