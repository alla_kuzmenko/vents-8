<?php
namespace frontend\modules\contacts\models;


/**
 * Class Contact
 * @package frontend\modules\contacts\models
 */
class Contact extends \backend\modules\contacts\models\Contact
{
    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public static function find_base()
    {
        return self::find()->innerJoinWith(["lang"])->enabled();
    }


}