<?php
namespace frontend\modules\forms\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Response;
//
use frontend\modules\forms\models\FeedbackForm;
use frontend\components\BaseController;
//
use backend\modules\forms\models\Topic;

/**
 * Class FeedbackformController
 *
 * @package frontend\modules\forms\controllers
 * @author Alla Kuzmenko
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2014, Thread
 */
class FeedbackformController extends BaseController

{
    public $model = FeedbackForm::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => \yii\captcha\CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionAdd()
    {

        $model = new $this->model;
        $model->setScenario('addfeedback');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $response = [
                'success' => 0,
                'fields' => [],
                'location' => Url::toRoute('/home/home/index')
            ];

            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->validate()) {
                $status = $model->addFeedback();


                if ($status === true) {
                    $this->sendEmail($model);
                    $response['success'] = 1;
                } else {
                    $response['success'] = 0;
                }
                return $response;

            } else {
                foreach ($model->getErrors() as $fiels => $val) {
                    $response['fields'][]['field'] = $fiels;
                    $response['fields'][count($response['fields']) - 1]['val'] = $val[0];
                }
                return $response;

            }

        } else {
            return $this->redirect(Url::toRoute('/home/home/index'));
        }
    }

    /**отправка письма админу по условию
     * @param $user
     * @return bool
     */
    public function sendEmail($user)
    {
        if (!empty($user->topic_id)) {
            //из админки тянем параметр
            $module = Yii::$app->getModule('configs');
            if (!empty($module)) {
                $email_topic = Topic::findById($user->topic_id);
                if (!empty($email_topic->email)) {
                    return Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'feedback-html', 'text' => 'feedback-text'],
                            ['user' => $user]
                        )
                        ->setFrom([$module->getParam('2')])//email_from from parametrs
                        ->setTo($email_topic->email)
                        ->setSubject('Feedback ' . \Yii::$app->name)
                        ->send();

                }
            }
        }

    }
}
