<?php

namespace frontend\modules\aboutus\models;

use yii\helpers\Url;
use Yii;

/**
 * Class Item
 *
 * @package frontend\modules\aboutus\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Item extends \backend\modules\aboutus\models\Item
{

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     *
     * @return yii\db\ActiveQuery
     */
    public static function find_base()
    {
        return self::find()->innerJoinWith(["lang"])->enabled();
    }
}
