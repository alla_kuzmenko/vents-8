<?php

namespace frontend\modules\aboutus\models;

/**
 * Class ItemLang
 *
 * @package frontend\modules\aboutus\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class ItemLang extends \common\modules\aboutus\models\ItemLang {

    /**
     * 
     * @return array
     */
    public function behaviors() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function scenarios() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function attributeLabels() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function rules() {
        return [];
    }

}
