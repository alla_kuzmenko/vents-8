<?php

namespace frontend\modules\aboutus;

/**
 * Class Aboutus
 *
 * @package frontend\modules\aboutus
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Aboutus extends \backend\modules\aboutus\Aboutus
{
    /**
     * Number of elements in GridView
     * @var int
     */
    public $itemOnPage = 999;
}
