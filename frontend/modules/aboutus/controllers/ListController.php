<?php

namespace frontend\modules\aboutus\controllers;

use frontend\components\BaseController;
use thread\actions\ListQuery;
use frontend\modules\aboutus\models\Item;

/**
 * Class ListController
 *
 * @package frontend\modules\aboutus\controllers
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class ListController extends BaseController
{

    public $title = "Aboutus List";

    public $defaultAction = 'index';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                ],
            ],
            [
                'class' => \yii\filters\HttpCache::class,
                'only' => ['index'],
                'lastModified' => function ($action, $params) {
                    $q = Item::find_base()->one();
                    return ($q !== null) ? $q['updated_at'] : time();
                },
            ],
        ];
    }

    /**
     *
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => ListQuery::class,
                'query' => Item::find_base(),
                'recordOnPage' => $this->module->itemOnPage,
                'layout' => 'list'
            ],
        ];
    }
}
