<?php

use yii\helpers\Html;

/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
?>

<div class="products">
    <div class="products-over">
        <div class="transp"></div>
        <figure>
            <img src="<?= $article->getArticleImage() ?>" alt="product">
            <figcaption><?= Html::encode($article['lang']['title']) ?></figcaption>
        </figure>
    </div>
    <div class="products-under big-text">
        <div class="transp"></div>
        <figure>
            <img src="<?= $article->getArticleImageSmall() ?>" alt="product">
            <figcaption><?= $article['lang']['content'] ?></figcaption>
        </figure>
    </div>
</div>