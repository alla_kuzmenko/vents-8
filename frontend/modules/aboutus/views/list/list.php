<?php
/**
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
use frontend\modules\seo\models\Seo;
use yii\helpers\Url;

/** SEO */

$this->title = Yii::t('app', 'About us');

$seo = Seo::getSeoParams((isset($model) ? $model : null));
if ($seo) {
    $this->title = $seo['title'];
    $this->registerMetaTag(['name' => 'description', 'content' => $seo['description']]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $seo['keywords']]);
}
$this->context->breadcrumbs[] = [
    'label' => Yii::t('app', 'About us'),
    'url' => Url::toRoute(['/aboutus/list/index'])
];
?>

<h2><?= Yii::t('app', 'Products we offer') ?>:</h2>

<?php foreach ($models as $article) : ?>
    <?= $this->render('_list_item', ['article' => $article]) ?>
<?php endforeach; ?>