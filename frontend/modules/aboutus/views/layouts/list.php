<?php
use frontend\themes\vents\assets\AppAsset;
use yii\widgets\Breadcrumbs;

$bundle = AppAsset::register($this);

$this->beginContent('@app/layouts/main.php');

?>
    <div class="about-wr">
        <div class="bg-dots">

            <?= $this->render('@app/layouts/parts/_header', ['bundle' => $bundle]) ?>

            <?=
            Breadcrumbs::widget([
                'links' => $this->context->breadcrumbs
            ])
            ?>

            <div class="about-content">
                <div class="about-info clearfix">
                    <div class="about-sl"><?= Yii::$app->getModule('configs')->getParam('4') ?></div>
                    <div class="staff"><span class="right">3 500</span><?= Yii::t('app', 'employees') ?></div>
                    <div class="prod"><span class="right">25 000</span><?= Yii::t('app', 'products items') ?></div>
                </div>
                <section>

                    <?= $content ?>

                </section>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>