<?php
namespace frontend\modules\seo\models;

use common\modules\seo\models\SeoMetatag as CommonSeoMetatagModel;
use common\modules\seo\models\SeoMetatagLang;
use yii\db\ActiveQuery;
use yii\helpers\Url;

/**
 * @author Fantamas
 * @property SeoMetatagLang $lang
 */
class SeoMetatag extends CommonSeoMetatagModel
{

    public static function findBase()
    {
        return parent::findBase()
            ->joinWith('lang')
            ->where(['url_hash' => sha1(Url::current())])
            ->enabled()
            ->undeleted();
    }

    /**
     * @return ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(SeoMetatagLang::class, ['rid' => 'id'])->andOnCondition(['lang' => \Yii::$app->language]);
    }

    /**
     * @return bool|string
     */
    public function getMetaTitle()
    {
        return $this->lang->title ?? false;
    }

    /**
     * @return bool|string
     */
    public function getMetaDescription()
    {
        return $this->lang->description ?? false;
    }

    /**
     * @return bool|string
     */
    public function getMetaKeywords()
    {
        return $this->lang->keywords ?? false;
    }
}
