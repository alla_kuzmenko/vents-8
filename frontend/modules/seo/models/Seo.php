<?php
namespace frontend\modules\seo\models;

use frontend\modules\configs\models\Language;
use thread\modules\seo\interfaces\SeoInterface;
use Yii;

/**
 * Class SitemapElement
 *
 * @package app\modules\sitemap\models;
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
class Seo extends \common\modules\seo\models\Seo
{

    private static $seo = null;

    /**
     * Получаем сео параметры, текущей страницы
     */
    public static function getSeoParams($model = null)
    {
        $params = [
            'title' => '',
            'description' => '',
            'keywords' => '',
            'viewPageInSearch' => true,
        ];

        $url =  $_SERVER['REQUEST_URI'];

        $lanModel = Language::getCurrentModel();
        if ($lanModel) {
            $url = ($lanModel['default']) ? $url : substr($url, mb_strlen($lanModel['alias'])+1);
            $url = ($url) ? $url : '/'; // т.к. поменяли урл
        }

        $seoMetaTags = SeoMetatag::findByUrl($url);

        if ($seoMetaTags) {
            $params = [
                'title' => $seoMetaTags['lang']['title'],
                'description' => $seoMetaTags['lang']['description'],
                'keywords' => $seoMetaTags['lang']['keywords'],
            ];
        }

        if ($model instanceof SeoInterface && !$seoMetaTags) {
            $seoModel = self::findByNamespace(get_class($model)::getModelNamespace(), $model->id);

            if ($seoModel) {
                $params = [
                    'title' => $seoModel['lang']['title'],
                    'description' => $seoModel['lang']['description'],
                    'keywords' => $seoModel['lang']['keywords'],
                    'viewPageInSearch' => $seoModel['in_search']
                ];
            }
        }

        return self::$seo = $params;
    }

    /**
     * Регистрация мето тегов
     *
     *  Переделать
     * @param $view ($this - view)
     */
    public static function registerMetaTag($view)
    {
        if (self::$seo === null) {
            self::getSeoParams();
        }

        $view->title = self::$seo['title'];
        $view->registerMetaTag(['name' => 'description', 'content' => self::$seo['description']]);
        $view->registerMetaTag(['name' => 'keywords', 'content' => self::$seo['keywords']]);

        if (isset(self::$seo['viewPageInSearch'])) {
            $view->registerMetaTag(['name' => 'ROBOTS', 'content' => 'NOINDEX, NOFOLLOW']);
        }
    }

}