<?php

namespace frontend\modules\seo\controllers;

use frontend\modules\seo\models\RobotsElement;
use Yii;
use frontend\modules\seo\models\SitemapXMLSimple;
use frontend\modules\seo\models\SitemapXMLElement;

/**
 * Class RobotsController
 *
 * @package app\modules\sitemap\controllers
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, VipDesign
 */
class RobotsController extends \frontend\components\BaseController
{

    public $title = "Robots";
    public $defaultAction = 'index';
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'create' => ['get'],
                    'filling' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @param string $secretKey
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCreate($secretKey)
    {
        if ($secretKey !== $this->module->secretKey) {
            throw new \yii\web\NotFoundHttpException;
        }

        $filename = Yii::getAlias('@webroot') . '/robots.txt';
        if (fopen($filename, 'a+')) {
            set_time_limit(0);
            $elements = RobotsElement::find();
            $content = '#Autogen' . "\n";
            foreach ($elements->batch(500) as $item) {
                foreach ($item as $i) {
                    $content .= "Disallow: " . $i->url . "\n";
                }
            }
            $fileContent = file_get_contents($filename);
            if (strpos($fileContent, '#Autogen')) {
                $oldContent = substr($fileContent, strpos($fileContent, '#Autogen'));
                $content = str_replace($oldContent, $content, $fileContent);
            }
            file_put_contents($filename, $content);
        } else {
            echo 'error open file';
        }
    }

    /**
     * Filling base
     */
    public function actionFilling($secretKey)
    {
        if ($secretKey !== $this->module->secretKey) {
            throw new \yii\web\NotFoundHttpException;
        }

        set_time_limit(0);

        $objects = $this->module->objects;

        foreach ($objects as $module_id => $module) {
            foreach ($module as $model_id => $model) {
                $this->cleanFill($module_id, $model_id);
                $this->fill($module_id, $model_id, $model);
            }
        }

    }

    /**
     * Заповнення даними бази визначеного модуля та моделі
     * @param string $module_id
     * @param string $model_id
     * @param array $model ['class' => class, 'method' => method]
     * @return bool
     */
    public function fill($module_id, $model_id, $model)
    {
        set_time_limit(0);

        if (!isset($model['robots_method'])) {
            return false;
        }

        $items = call_user_func([$model['class'], $model['robots_method']]);

        foreach ($items->batch(500) as $item) {
            $robotsElements = [];
            foreach ($item as $i) {
                $robotsElements[] = [
                    'module_id' => $module_id,
                    'model_id' => $model_id,
                    'key' => $i->id,
                    'url' => (isset($model['urlMethod'])) ? $model['urlMethod']($i) : $i->getUrl(true),
                    'created_at' => time(),
                    'updated_at' => time(),
                ];
            }

            $connection = RobotsElement::getDb();
            $connection->createCommand()->batchInsert(RobotsElement::tableName(), array_keys($robotsElements[0]), $robotsElements)->execute();
        }
    }

    /**
     * Очищення даних визначеного модуля та моделі
     * @param string $module_id
     * @param string $model_id
     */
    public function cleanFill($module_id, $model_id)
    {
        set_time_limit(0);

        $connection = RobotsElement::getDb();
        $connection->createCommand()->delete(RobotsElement::tableName(), 'module_id = \'' . $module_id . '\' AND model_id = \'' . $model_id . '\'')->execute();
    }
}
