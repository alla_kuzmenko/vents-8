<?php

namespace frontend\modules\configs\models;

use Yii;
//
use common\modules\configs\models\Language as CommonLanguageModel;

class Language extends CommonLanguageModel
{
    /**
     * @return mixed
     * выводим только те языки, которые опубликованы
     */
    public function getLanguages():array
    {
        return self::findBase()->enabled()->all();
    }

    /**
     * @param $param
     * @return bool
     */
    public static function getDefaultByUrl($param)
    {
        if ($data = self::findBase()->where(['published' => '1', 'alias' => $param])->one()) {
            if ($data['default'] == 1) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * For GeoIp
     * @param string $lang (default null)
     * @return bool
     */
    public static function getByLocal($lang = null)
    {
        if ($lang) {
            if ($data = self::findBase()->where(['published' => '1', 'local' => $lang])->one()) {
                return $data;
            }
        }
        return false;
    }
    /**
     * By Alias
     * @param string $lang (default null)
     * @return bool
     */
    public static function getByAlias($lang = null)
    {
        if ($lang) {
            if ($data = self::findBase()->where(['published' => '1', 'alias' => $lang])->one()) {
                return $data;
            }
        }
        return false;
    }

    /**
     * @param string $lang (default null)
     * @return bool
     */
    public static function exist($lang = null)
    {
        if ($lang) {
            if (self::findBase()->where(['published' => '1', 'label' => $lang])->one()) {
                return true;
            }
        }
        return false;
    }
    


}
