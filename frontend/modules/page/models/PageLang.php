<?php

namespace frontend\modules\page\models;

/**
 * Class PageLang
 *
 * @package frontend\modules\page\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class PageLang extends \common\modules\page\models\PageLang {

    /**
     * 
     * @return array
     */
    public function behaviors() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function scenarios() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function attributeLabels() {
        return [];
    }

    /**
     * 
     * @return array
     */
    public function rules() {
        return [];
    }

}
