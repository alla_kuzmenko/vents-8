<?php
namespace frontend\modules\page\models;

use common\modules\page\models\InNumbersWidget as CommonInNumbersWidget;

/**
 * Class InNumbersWidget
 * @package common\modules\page\models
 */
class InNumbersWidget extends CommonInNumbersWidget
{
    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public static function findBase()
    {
        return parent::findBase()->joinWith('lang')->enabled()->orderBy(['position' => SORT_ASC]);
    }
}
