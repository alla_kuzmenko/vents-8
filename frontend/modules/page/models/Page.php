<?php

namespace frontend\modules\page\models;

use common\modules\seo\models\Seo;
use thread\modules\configs\models\Language;
use yii\helpers\Url;

/**
 * Class Page
 *
 * @package frontend\modules\page\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Page extends \common\modules\page\models\Page
{
    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     *
     * @return yii\db\ActiveQuery
     */
    public static function find_base()
    {
        return parent::find()->innerJoinWith(["lang"])->enabled();
    }

    /**
     *
     * @param string $alias
     * @return ActiveRecord|null
     */
    public static function findByAlias($alias)
    {
        return self::find_base()->alias($alias)->one();
    }

    /**
     * Method for sitemap generation
     * @return mixed
     */
    public static function findSiteMap()
    {
        return self::findBase()->joinWith('seo')
            ->andWhere(['Like', '{{%seo}}.model_namespace', self::getModelNamespace()])
            ->andWhere(['{{%seo}}.in_site_map' => 1]);
    }

    /**
     * Method for robots.txt generation
     * @return mixed
     */
    public static function findRobots()
    {
        return self::findBase()->joinWith('seo')
            ->andWhere(['Like', '{{%seo}}.model_namespace', self::getModelNamespace()])
            ->andWhere(['{{%seo}}.in_robots' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeo()
    {
        return $this->hasMany(Seo::class, ['model_id' => 'id']);
    }

    /**
     * Url route to view particular page
     * @param null $schema
     * @return string
     */
    public function getUrl($schema = false)
    {
        return Url::toRoute(['/page/page/view', 'alias' => $this->alias], $schema);
    }

    /**
     * Show text for map
     *
     * @return string
     */
    public static function getMapText()
    {
        $data = self::findByAlias('map');

        if ($data == null) {
            return $data['lang']['content'];
        } else {
            return '';
        }

    }

    /**
     * Url в админку
     */
    public function getUrlToBack()
    {
        $url = '/backend/{lang}page/page/update?id=' . $this->id;

        $lanModel = Language::getCurrentModel();

        if ($lanModel) {
            $url = ($lanModel['default']) ? str_replace('{lang}', '', $url) : str_replace('{lang}', $lanModel['alias'] . '/', $url);
        }

        return $url;
    }
}
