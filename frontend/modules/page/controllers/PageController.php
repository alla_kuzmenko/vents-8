<?php

namespace frontend\modules\page\controllers;

use frontend\components\BaseController;
use frontend\modules\seo\models\Seo;
use Yii;
use yii\web\NotFoundHttpException;
use frontend\modules\page\models\Page;

/**
 * Class PageController
 *
 * @package frontend\modules\page\controllers
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class PageController extends BaseController
{
    public $title = "Page";
    public $layout = "/page";
    public $defaultAction = 'index';
    public $model;


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                    'contacts' => ['get'],
                    'view' => ['get'],
                ],
            ],
            [
                'class' => \yii\filters\HttpCache::class,
                'only' => ['index', 'view'],
                'lastModified' => function ($action, $params) {
                    $q = Page::findByAlias(Yii::$app->getRequest()->get('alias'));
                    return ($q !== null) ? $q['updated_at'] : time();
                },
            ],
        ];
    }

    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "/start";
        return $this->run('view', ['alias' => 'start']);
    }

    /**
     *
     * @param string $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias)
    {
        $model = Page::findByAlias($alias);

        if ($model === null) {
            return $this->redirect(['/home/home/error'], 301);
            die();
        }

        //layouts
        if ($model->alias == 'sitemap') {
            $this->layout = 'sitemap';
            $view = 'index';
        } elseif ($model->alias == 'basestart') {
            $this->layout = 'base_start';
            $view = 'index';
        } else {
            $this->layout = 'privacy-policy';
            $view = 'default';
        }

        $this->model = $model;

        $this->breadcrumbs[] = [
            'label' => $this->model['lang']['title'],
            'url' => $model->getUrl()
        ];


        $seo = Seo::getSeoParams($model);

        return $this->render($view, [
            'model' => $model,
            'seo' => $seo
        ]);
    }

}
