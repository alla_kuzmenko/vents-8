<?php

namespace frontend\modules\page\controllers;

use Yii;
use yii\helpers\Url;
use  yii\web\Response;
use frontend\components\BaseController;
use frontend\modules\news\models\Article;
use frontend\modules\page\models\Page;

/**
 * Class PageController
 *
 * @package frontend\modules\page\controllers
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class FindController extends BaseController
{
    public $title = "Search";
    public $layout = "find";
    public $defaultAction = 'index';
    public $model;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                    'contacts' => ['get'],
                    'view' => ['get'],
                ],
            ]
        ];
    }

    /**
     * @param $condition - строка поиска
     * @return mixed
     */
    public function actionIndex()
    {
        $posts = [];
        $condition = Yii::$app->getRequest()->get('condition', '');
//        $condition = '';
        if (!empty($condition)) {

            $pagesModels = Page::findBase()->innerJoinWith('lang')
                ->orWhere(['LIKE', 'title', $condition])
                ->orWhere(['LIKE', 'content', $condition])
                ->all();

            $articleModels = Article::findBase()->innerJoinWith('lang')
                ->orWhere(['LIKE', 'title', $condition])
                ->orWhere(['LIKE', 'description', $condition])
                ->all();

            $posts = array_merge($pagesModels, $articleModels);

            $this->breadcrumbs[] = [
                'label' => Yii::t('app', 'Search'),
                'url' => Url::toRoute(['/page/find/index', 'condition' => $condition])
            ];

        }

        return $this->render('index', ['condition' => $condition, 'posts' => $posts]);
    }

    /**
     * @return string
     */
    public function actionAjax()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get()) {
            $condition = Yii::$app->request->get('condition');
            $pagesModels = Page::findBase()->innerJoinWith('lang')
                ->orWhere(['LIKE', 'title', $condition])
                ->orWhere(['LIKE', 'content', $condition])
                ->all();

            $articleModels = Article::findBase()->innerJoinWith('lang')
                ->orWhere(['LIKE', 'title', $condition])
                ->orWhere(['LIKE', 'description', $condition])
                ->all();
            $posts = array_merge($pagesModels, $articleModels);

            Yii::$app->getResponse()->format = Response::FORMAT_JSON;
            return $this->renderPartial('ajax', ['posts' => $posts, 'condition' => $condition]);
        } else {
            // if isset condition or is not ajax request return empty string
            return '';
        }
    }

}