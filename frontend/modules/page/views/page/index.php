<?php

use frontend\components\BaseController;
use frontend\modules\seo\models\Seo;
use yii\helpers\Html;

/** SEO */
Seo::getSeoParams( (isset($model) ? $model : '') );
Seo::registerMetaTag($this);
?>

<!--Переход в админку-->
<?= BaseController::getHtmlUrlToBack($model) ?>


<?php //Html::tag('h1', Html::encode($model['lang']['title']), []); ?>
<?= $model['lang']['content'] ?>

