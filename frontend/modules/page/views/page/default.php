<?php

use frontend\components\BaseController;
use frontend\modules\seo\models\Seo;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/** SEO */
Seo::getSeoParams( (isset($model) ? $model : '') );
Seo::registerMetaTag($this);

?>

    <!--Переход в админку-->
<?= BaseController::getHtmlUrlToBack($model) ?>

        <?=
        Breadcrumbs::widget([
            'links' => $this->context->breadcrumbs
        ])
        ?>

        <div class="neck">
            <?= Html::tag('h1', Html::encode($model['lang']['title']), []); ?>
        </div>

    </div>
</div>
<div class="policy clearfix">
    <div class="policy-body editor">

    <?= $model['lang']['content'] ?>