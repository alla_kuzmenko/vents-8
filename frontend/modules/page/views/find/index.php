<?php

$this->title = Yii::t('app', 'Search');



?>

<div class="result-content">
    <div class="clearfix result-line">
        <div class="search-word"> "<?= $condition ?>"</div>
        <div class="amount"><?= Yii::t('app', 'Found') ?> <?= count($posts) ?></div>
    </div>

    <?php if ($posts): ?>
        <?php foreach ($posts as $post) : ?>

            <div class="short-info">
                <a href="<?= $post->getUrl() ?>"><?= Yii::$app->getModule('page')->smarty_modifier_mb_truncate($post['lang']['title'], $condition, 250) ?></a>
                <div class="text"><?= Yii::$app->getModule('page')->smarty_modifier_mb_truncate($post['lang']['content'], $condition, 250) ?></div>
            </div>

        <?php endforeach; ?>

    <?php else : ?>
        <?= Yii::t('app', 'Not found') ?>
    <?php endif; ?>
</div>



