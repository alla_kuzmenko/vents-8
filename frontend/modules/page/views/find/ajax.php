<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="dropdown-search">
    <ul>
        <?php
        foreach ($posts as $k => $post) :
            echo Html::beginTag('li').Html::a($post['lang']['title'], $post->getUrl()).Html::endTag('li');
            if ($k >= 2) :
                break;
            endif;
        endforeach;
        ?>
    </ul>
    <?= Html::a(
        Yii::t('app', 'Show all results'),
        Url::to('/find/' . $condition),
        ['class'=>'all-results']
    )
    ?>
</div>