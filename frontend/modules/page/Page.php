<?php

namespace frontend\modules\page;

/**
 * Class Page
 *
 * @package frontend\modules\page
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Page extends \common\modules\page\Page
{
    /**
     * Get short description
     *
     * @param $string
     * @param $word
     * @param int $length
     * @param string $etc
     * @param string $charset
     * @param bool|false $break_words
     * @param bool|false $middle
     * @return mixed|string
     */
    public function smarty_modifier_mb_truncate(
        $string,
        $word = false,
        $length = 120,
        $etc = ' ...',
        $charset = 'UTF-8',
        $break_words = false,
        $middle = false
    )
    {
        $string = strip_tags($string);

        if ($length == 0)
            return '';

        if (strlen($string) > $length) {
            $length -= min($length, strlen($etc));
            if ($word) {
                $string = preg_replace("/".$word."/iu", "<b>\$0</b>", $string);
            }
            if (!$break_words && !$middle) {
                $string = preg_replace('/\s+?(\S+)?$/', '', mb_substr($string, 0, $length + 1, $charset));
            }
            if (!$middle) {
                return mb_substr($string, 0, $length, $charset) . $etc;
            } else {
                return mb_substr($string, 0, $length / 2, $charset) . $etc . mb_substr($string, -$length / 2, $charset);
            }
        } else {
            if ($word) {
                $string = preg_replace("/".$word."/iu", "<b>\$0</b>", $string);
            }
            return $string;
        }


    }
}
