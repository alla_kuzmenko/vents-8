<?php

namespace frontend\modules\page\widgets\Sitemap;

use thread\app\base\widgets\Widget;
use frontend\modules\page\models\Page;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class LogIn
 *
 * @package frontend\modules\forms\widgets\Feedback
 * @author Alla Kuzmenko
 * @copyright (c) 2016, Thread
 *
 *
 *
 */
class Sitemap extends Widget
{

    public $view = 'Sitemap';
    public $name = 'sitemap';
    public $modelPage;


    /**
     *
     */
    public function init()
    {
        $this->modelPage = Page::find_base()->all();

    }


    public function run()
    {
        return $this->render($this->view, ['modelPage' => $this->modelPage]);
    }

}