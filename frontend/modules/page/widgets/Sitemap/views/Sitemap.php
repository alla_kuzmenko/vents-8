<?php
use yii\helpers\Url;

?>


<div class="sitemap">
    <div class="node"><a href="/"><?= Yii::t('app', 'Home') ?></a></div>
    <div class="node sub-node first-node"><a href="<?= Url::toRoute(['/news/list/index']) ?>"><?= Yii::t('app', 'News') ?></a>
    </div>
    <div class="node sub-node"><a
            href="<?= Url::toRoute(['/aboutus/list/index']) ?>"><?= Yii::t('app', 'About us') ?></a></div>
    <?php
    foreach ($modelPage as $page) {
        ?>
        <div class="node sub-node"><a href="<?= $page->getUrl() ?>"><?= $page->lang->title ?></a></div>

        <?php
    }

    ?>
</div>
