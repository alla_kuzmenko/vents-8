<?php
use frontend\themes\vents\assets\AppAsset;
use yii\helpers\Html;

$bundle = AppAsset::register($this);
?>

    <div id="map" class="clear"></div>
    <div class="top-content">
        <div class="main-title">
            <h1><?= Yii::$app->getModule('configs')->getParam('4') ?></h1>

            <div class="magic-select">
                <div class="country clearfix"><span class="iconW"><?= Yii::t('app', 'Choose a country') ?></span></div>
                <div class="select-menu">
                    <ul>
                        <?php foreach ($modelCountry as $country): ?>
                            <?php if (!empty($country['companys']) && $country['published'] == 1):
                                echo '<li><a><div class="flages"><img src="'.$bundle->baseUrl . '/img/flags_new/' . $country['alias'] . '.png"><span data-zoom="' . $country['zoom'] . '">' . $country['lang']['title'] . '</span></div></a></li>';
                            endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php

$js_markers = [];
function jsString($str = '')
{
    return preg_replace("/('|\"|\r?\n)/", '', $str);
}

foreach ($markers as $marker) {

    $link_site = ($marker['link_site']) ? '<div class="mp-site"><a href="' . $marker['link_site'] . '" target="_blank">' . $marker['link_site'] . '</a></div>' : '';
    $link_shop = ($marker['link_site']) ? '<div class="mp-shop"><a href="' . $marker['link_shop'] . '" target="_blank">' . $marker['link_shop'] . '</a></div>' : '';

    $js_markers[] = '
    {
        type_marker:\'' . $marker['type_marker'] . '\',
        lat:' . $marker['lat'] . ',
        lng:' . $marker['lng'] . ',
        content:\'<div class="mp-info"><div class="mp-office edi">' . $marker['lang']['title'] . '</div>'
        . '<div class="mp-country">' . $marker['country']['lang']['title'] . ', ' . $marker['city']['lang']['title'] . '</div>'
        . $link_site
        . $link_shop
        .'<div class="editor">'. jsString($marker['lang']['content']).'</div>'
        . '</div>' . '\'
    }';
}
$js_markers = implode(",", $js_markers);

$script = <<< JS
/*
 * @author Anastasia Popova
 * @copyright (c) 2016, VipDesign
 */

/**
 * initialize function
 * @returns {undefined}
 */

var myLatlng = new google.maps.LatLng(25.4501, 10.6234);
var myOptions = {
    zoom: 2,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    zoomControl: false,
    scrollwheel: false,
    draggable: false,
    minZoom: 2
}

var map = new google.maps.Map(document.getElementById("map"), myOptions);

/**
 * map styles
 * @type Array
 */
var styles = [
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {"color": "#0c4da1"}
        ]
    },
    {
        featureType: 'administrative.country',
        elementType: 'geometry',
        stylers: [
            {"color": "#54a7d9"},
            {"lightness": 11}
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {"color": "#2963ba"}
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {"color": "#808080"}
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {"color": "#54a7d9"},
            {"lightness": 11}
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{ "visibility": "off" }]
    },
    {
        featureType: "administrative.country",
        elementType: "labels",
        stylers: [{ visibility: "off" }]
    }
];

map.setOptions({styles: styles});

$(document).ready(function () {

    /**
    *
    * @param {type} geocoder
    * @param {type} resultsMap
    * @returns {undefined}
    * calculates where the county is situated
    */

    function geocodeAddress(geocoder, resultsMap) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                //get center on the country
                var lat = results[0].geometry.location.lat()+2;
                var lng = results[0].geometry.location.lng();
                resultsMap.setCenter(new google.maps.LatLng(lat, lng));
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    $('.select-menu a span').on('click', function(){

        address = $(this).html();

        var geocoder = new google.maps.Geocoder();
        geocodeAddress(geocoder, map);

        var zoom = $(this).data('zoom');
        //set zoom for country
        if (zoom) {
            map.setZoom(zoom);
        } else {
            map.setZoom(5);
        }
    });
});

//create infoWindow
var infoWindow = new google.maps.InfoWindow();

/**
 * function create marker
 * @returns {undefined}
 */
var onMarkerClick = function () {

    var marker = this;
    var latLng = marker.getPosition();
    infoWindow.setContent(marker.content);
    infoWindow.open(map, marker);

    $('.gm-style-iw').next().on('click',function () {
        var zoom = map.getZoom();
        //if we click on map while zoom = 2, show h1
        if(zoom==2){
            $('.main-title').find('h1').show();
            $('.magic-select').css('top','0px');
            $('.magic-select').css('display','block');
            map.setCenter(myLatlng);
        }
    });
};

var moveToMarker = function(){
    var zoom = map.getZoom();
    var marker = this;

if(zoom>2){

    var center =  marker.getPosition();
    var span = map.getBounds().toSpan();

    var newCenter = {
      lat: center.lat() + span.lat()*0.35,
      lng: center.lng()
    };
    map.panTo(newCenter);
}

    //if we click on map while zoom = 2, hide h1
    if(zoom==2){
    $('.main-title').find('h1').hide();
    $('.magic-select').css('top','138px');
    $('.top-wr').css('min-height','550px');
       $('.magic-select').css('display','none');
        if($('.top-wr').hasClass('learn-more')){


        }
    }

};

google.maps.event.addListener(map, 'click', function () {
    infoWindow.close();
    var zoom = map.getZoom();
    //if we click on map while zoom = 2, show h1
    if(zoom==2){
     $('.main-title').find('h1').show();
     $('.magic-select').css('top','0px');
      $('.magic-select').css('display','block');
      map.setCenter(myLatlng);
     }
});

function addMarker(marker) {
    if(marker.type_marker=='white'){
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(marker.lat, marker.lng),
        icon: ''+fv_BASE_URL+'/img/marker.png',
        map: map,
        content: marker.content
    });}
else{
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(marker.lat, marker.lng),
        icon: ''+fv_BASE_URL+'/img/marker-active.png',
        map: map,
        content: marker.content
    });}

    google.maps.event.addListener(marker, 'click', onMarkerClick);
    google.maps.event.addListener(marker, 'click', moveToMarker);
}

var markers = [$js_markers];

for (i=0; i<markers.length; i++){
    addMarker(markers[i]);
}

google.maps.event.addDomListener(map,'zoom_changed', function() {

    var zoom =  map.getZoom();

    if($('.top-wr').hasClass('learn-more')){
        if (zoom ==2) {
            $('#map').css('max-width','1024px');
            google.maps.event.trigger(map, 'resize');
            map.set('draggable', false);
            map.set('zoomControl', false);
            map.set('center', myLatlng);
            $('.top-wr').removeClass('top-abs');
            $('header').removeClass('header-abs');
            $('.header-wr').css('paddingTop','35px');
            $('header').css('marginLeft', 'auto');
            $('.magic-select').css('top','');
            $('.main-title').find('h1').show();
        } else if (zoom >2) {
            var halfHeaderWidth = $('header').width()/2;
            $('#map').css('max-width','100%');
            google.maps.event.trigger(map, 'resize');
            map.set('draggable', true);
            map.set('zoomControl', true);
            map.set('zoomControl', true);
            $('.top-wr').addClass('top-abs');
            $('header').addClass('header-abs');
            $('header').css('marginLeft', '-'+halfHeaderWidth+'px');
            $('.header-wr').css('paddingTop','0');
            $('.magic-select').css('top','-200px');
            $('.main-title').find('h1').hide();
        }
    } else {
        if (zoom ==2) {
            $('#map').css('max-width','1024px');
            google.maps.event.trigger(map, 'resize');
            map.set('draggable', false);
            map.set('zoomControl', false);
            map.set('center', myLatlng);
            $('.top-wr').removeClass('top-abs');
            $('header').removeClass('header-abs');
            $('.header-wr').css('paddingTop','35px');
            $('header').css('marginLeft', 'auto');
            $('.magic-select').css('top','');
            $('.main-title').find('h1').show();
        } else if (zoom >2) {
            var halfHeaderWidth = $('header').width()/2;
            $('#map').css('max-width','100%');
            google.maps.event.trigger(map, 'resize');
            map.set('draggable', true);
            map.set('zoomControl', true);
            map.set('zoomControl', true);
            $('.top-wr').addClass('top-abs');
            $('header').addClass('header-abs');
            $('header').css('marginLeft', '-'+halfHeaderWidth+'px');
            $('.header-wr').css('paddingTop','0');
            $('.magic-select').css('top','-200px');
            $('.main-title').find('h1').hide();
        }
    }
});
JS;

$this->registerJs($script, yii\web\View::POS_READY);