<?php

namespace frontend\modules\location\widgets;

use thread\app\base\widgets\Widget;
use frontend\modules\location\models\Country;

/**
 * Class CompanyMap
 *
 * @package frontend\modules\location\widgets
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class CompanyMap extends Widget
{

    public $view = 'company_map';

    public function init()
    {

    }

    /**
     * @return string
     */
    public function run()
    {
        $modelCountry = Country::findToMap();

        $markers = [];
        foreach ($modelCountry as $country) {
            foreach ($country['companys'] as $company) {
                $markers[] = $company;
            }

        }

        return $this->render($this->view, ['modelCountry' => $modelCountry, 'markers' => $markers]);
    }

}