<?php

namespace frontend\modules\location\models;

/**
 * Class RegionLang
 *
 * @package frontend\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class RegionLang extends \common\modules\location\models\RegionLang {

    /**
     *
     * @return array
     */
    public function behaviors() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules() {
        return [];
    }

}
