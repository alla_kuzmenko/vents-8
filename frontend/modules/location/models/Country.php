<?php

namespace frontend\modules\location\models;

/**
 * Class Country
 *
 * @package frontend\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Country extends \common\modules\location\models\Country
{
    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     *
     * @return yii\db\ActiveQuery
     */
    public static function find_base()
    {
        return parent::find()->enabled();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id'])->enabled();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['location_country_id' => 'id'])->enabled();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanys()
    {
        return $this->hasMany(Company::class, ['country_id' => 'id'])->enabled();
    }

    /**
     * @return mixed
     */
    public static function findToMap()
    {
        return self::findBase()
            ->joinWith([
                'companys',
                'lang' => function ($query) {
                    $query->orderBy(['title' => SORT_DESC]);
                },
            ])
            ->_lang()
            ->published()
            ->all();
    }
}
