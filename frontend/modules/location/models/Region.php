<?php

namespace frontend\modules\location\models;

/**
 * Class Region
 *
 * @package frontend\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Region extends \common\modules\location\models\Region
{
    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountrys()
    {
        return $this->hasMany(Country::class, ['region_id' => 'id'])->enabled();
    }

}
