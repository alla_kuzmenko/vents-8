<?php

namespace frontend\modules\location\models;

/**
 * Class CountryLang
 *
 * @package frontend\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class CountryLang extends \common\modules\location\models\CountryLang {

    /**
     *
     * @return array
     */
    public function behaviors() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules() {
        return [];
    }

}
