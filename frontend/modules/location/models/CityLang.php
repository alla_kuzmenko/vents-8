<?php

namespace frontend\modules\location\models;

/**
 * Class CityLang
 *
 * @package frontend\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class CityLang extends \common\modules\location\models\CityLang {

    /**
     *
     * @return array
     */
    public function behaviors() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules() {
        return [];
    }

}
