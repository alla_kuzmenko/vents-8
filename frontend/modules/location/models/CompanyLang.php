<?php

namespace frontend\modules\location\models;

/**
 * Class CompanyLang
 *
 * @package frontend\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class CompanyLang extends \common\modules\location\models\CompanyLang {

    /**
     *
     * @return array
     */
    public function behaviors() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels() {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules() {
        return [];
    }

}
