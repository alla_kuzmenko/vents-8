<?php

namespace frontend\modules\location\models;

/**
 * Class Company
 *
 * @package frontend\modules\location\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Company extends \common\modules\location\models\Company
{
    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     *
     * @return yii\db\ActiveQuery
     */
    public static function find_base()
    {
        return parent::find()->enabled();
    }

}
