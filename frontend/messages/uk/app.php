<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
return [
    'contacts'=>'Контакти',
    'For delivery of products on the territory of Ukraine'=>'З питань поставки продукції по території України',
//    'Customer Service'=>'Служба підтримки клієнтів',
    'free Ukraine from fixed phones'=>'безкоштовно зі стаціонарних телефонів України',
//    'export Department'=>'Відділ експорту',
//    'Human Resources Department'=>'Відділ кадрів',
    'address_vents'=>'Україна, 01030, м.Київ, вул.М.Коцюбинського, 1',
//    'Show all results' => 'Показати всі результати',


    "Contacts" => "Зв'язатися з нами",
    "Site map" => "Карта сайту",
    "About us" => "Про Нас",
    "our brands" => "НАШІ БРЕНДИ",
    "Choose a country" => "Оберіть країну",
    "Home" => "Головна",
    "News" => "Новини",
    "In the world" => "У СВІТІ",
    "In numbers" => "В ЦИФРАХ",
    "Products we offer" => "ПРОДУКТИ, ЯКІ МИ ПРОПОНУЄМО:",
    "products items" => "найменувань продукції",
    "Customer Service" => "Служба підтримки клієнтів:",
    "export Department" => "З питань поставки продукції на території України:",
    "Human Resources Department" => "Відділ кадрів:",
    "FeedbackForm" => "ФОРМА ЗВОРОТНОГО ЗВ'ЯЗКУ",
    "name" => "Ваше ім'я та прізвище",
    "question" => "Опишіть Ваше питання",
    "Choose topic" => "Оберіть тему",
    "send" => "Надіслати",
    "Close" => "Закрити",
    "Show all results" => "Показати всі результати пошуку",
    "Sorry, page not found" => "Вибачте, така сторінка не знайдена",
    "To main" => "Перейти на головну",
    "Visit website" => "Відвідати веб-сайт",
    "employees" => "працівників",

];