<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
return [

    "Learn more"=> "En savoir davantage",
    "Other news" => "Autres actualités",
    "Share"=> "Partager",
    "Search" => "Chercher",
    "Found"=> "Trouvé",
    "Not found" => "Par trouvé",
    "Event gallery"=> "Galerie",
    "Topic" => "Thème",
    "email"=> "e-mail",
    "phone" => "Téléphone",
    "Forms"=> "Formulaires",


    "Contacts" => "Contactez-nous",
    "Site map" => "Plan du site",
    "About us" => "A propos de nous",
    "our brands" => "NOS MARQUES",
    "Choose a country" => "Choisissez un pays",
    "Home" => "Page d'accueil",
    "News" => "Actualités",
    "In the world" => "DANS LE MONDE",
    "In numbers" => "EN CHIFFRES",
    "Products we offer" => "NOS PRODUITS:",
    "products items" => "produits",
    "Customer Service" => "Service clients :",
    "export Department" => "Pour la livraison des produits en Ukraine:",
    "Human Resources Department" => "Service des ressources humaines:",
    "FeedbackForm" => "RETOUR",
    "name" => "Votre nom et prénom",
    "question" => "Décrivez votre question",
    "Choose topic" => "Choisissez le sujet",
    "send" => "Envoyer",
    "Close" => "Fermer",
    "Show all results" => "Montrer des résultats",
    "Sorry, page not found" => "Désolé, page non trouvée",
    "To main" => "Retour à la page d'accueil",
    "Visit website" => "Visitez le site web",
    "employees" => "employés",
];
