<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
return [

    "Learn more" => "Saperne di più",
    "Other news" => "Più notizie",
    "Share" => "Condividere",
    "Search" => "Ricerca",
    "Found" => "Oggetto di ricerca trovato",
    "Not found" => "Oggetto di ricerca non trovato",
    "Event gallery" => "Galleria di eventi",
    "Topic" => "Tema",
    "email" => "E-Mail",
    "phone" => "Tel.:",
    "Forms" => "Forme",


    "Contacts" => "Invitiamo a contattarci",
    "Site map" => "Mappa del sito",
    "About us" => "Chi siamo",
    "our brands" => "I NOSTRI BRAND",
    "Choose a country" => "Scegliere un paese",
    "Home" => "Principale",
    "News" => "Notizie",
    "In the world" => "NEL MONDO",
    "In numbers" => "DATI NUMERICI",
    "Products we offer" => "PRODOTTI CHE OFFRIAMO:",
    "products items" => "articoli di prodotti",
    "Customer Service" => "Servizio clienti:",
    "export Department" => "Per le domande relative alla consegna della produzione sul territorio d'Ucraina:",
    "Human Resources Department" => "Ufficio personale:",
    "FeedbackForm" => "MODULO PER COMMENTI E SUGGERIMENTI",
    "name" => "Suo nome e cognome",
    "question" => "Descrivere la domanda",
    "Choose topic" => "Scegliere un tema",
    "send" => "Inviare",
    "Close" => "Chiudere",
    "Show all results" => "Dimostrare tutti i risultati",
    "Sorry, page not found" => "Siamo spiacenti, la pagina richiesta non è stata trovata",
    "To main" => "Ritornare alla pagina principale",
    "Visit website" => "Visitare il sito web",
    "employees" => "impiegati",
];
