<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 * @version 28/03/2015
 */
return [

    'Learn more' => 'Leer más',
    'Other news' => 'Más noticias',
    'Share' => 'Compartir',
    'Search' => 'Buscar',
    'Found' => 'Encontrado',
    'Not found' => 'No encontrado',
    'Event gallery' => 'Galería de eventos',
    'Topic' => 'Tema',
    'email' => 'Correo electrónico',
    'phone' => 'Teléfono',
    'Forms' => 'Formas',

    'Contacts' => 'Ponerse en contacto con nosotros',
    'Site map' => 'Mapa del sitio web',
    'About us' => 'Sobre nosotros',
    'our brands' => 'NUESTRAS MARCAS',
    'Choose a country' => 'Elegir el país',
    'Home' => 'Inicio',
    'News' => 'Noticias',
    'In the world' => 'EN EL MUNDO',
    'In numbers' => 'EN NÚMEROS',
    'Products we offer' => 'NUESTROS PRODUCTOS:',
    'products items' => 'productos',
    'Customer Service' => 'Servicio de Atención al Cliente:',
    'export Department' => 'Para entrega de los productos en el territorio de Ucrania:',
    'Human Resources Department' => 'Departamento de RR.HH.:',
    'FeedbackForm' => 'CONTACTE CON NOSOTROS',
    'name' => 'Su nombre y apellidos',
    'question' => 'Explique su pregunta',
    'Choose topic' => 'Elija el tema',
    'send' => 'Enviar',
    'Close' => 'Cerrar',
    'Show all results' => 'Mostrar todos los resultados de la búsqueda',
    'Sorry, page not found' => 'Sentimos no haber encontrado esta página',
    'To main' => 'Ir al inicio',
    'Visit website' => 'Ir al sitio web',
    'employees' => 'empleados',
];
