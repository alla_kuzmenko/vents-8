<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 * @version 28/03/2015
 */
return [

    'Learn more' => 'Узнать больше',
    'Other news' => 'Больше новостей',
    'Share' => 'Поделиться',
    'Search' => 'Поиск',
    'Found' => 'Найдено',
    'Not found' => 'Не найдено',
    'Event gallery' => 'Галлерея событий',
    'Topic' => 'Тема',
    'email' => 'E-Mail',
    'phone' => 'Тел.:',
    'Forms' => 'Формы',

    'Contacts' => 'Связаться с нами',
    'Site map' => 'Карта сайта',
    'About us' => 'О Нас',
    'our brands' => 'НАШИ БРЕНДЫ',
    'Choose a country' => 'Выберите страну',
    'Home' => 'Главная',
    'News' => 'Новости',
    'In the world' => 'В МИРЕ',
    'In numbers' => 'В ЦИФРАХ',
    'Products we offer' => 'ПРОДУКТЫ, КОТОРЫЕ МЫ ПРЕДЛАГАЕМ:',
    'products items' => 'наименований продукции',
    'Customer Service' => 'Служба поддержки клиентов:',
    'export Department' => 'По вопросам поставки продукции по територии Украины :',
    'Human Resources Department' => 'Отдел кадров:',
    'FeedbackForm' => 'ФОРМА ОБРАТНОЙ СВЯЗИ',
    'name' => 'Ваше имя и фамилия',
    'question' => 'Опишите Ваш вопрос',
    'Choose topic' => 'Выберите тему',
    'send' => 'Отправить',
    'Close' => 'Закрыть',
    'Show all results' => 'Показать все результаты поиска',
    'Sorry, page not found' => 'Извините, такая страница не найдена',
    'To main' => 'Перейти на главную',
    'Visit website' => 'Посетить вебсайт',
    'employees' => '',
];
