<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
return [
//    // Contacts page
//    'Contacts' => 'Связаться с нами', // ++++
    'For delivery of products on the territory of Ukraine' => 'По вопросам поставки продукции по територии Украины',
//    'Customer Service' => 'Служба поддержки клиентов', /++
    'free Ukraine from fixed phones' => 'бесплатно со стационарных телефонов Украины',// --
//    'export Department' => 'Отдел экспорта', // ++++
//    'Human Resources Department' => 'Отдел кадров', // +++
    'address_vents' => 'Украина, 01030, г.Киев, ул.М.Коцюбинского, 1', // -

    // 'Feedback form'
    // start page
//    'Choose a country' => 'Выберите страну', // +++
//    'In the world' => 'В мире', // +++
//    'Learn more' => 'Узнать больше', // -
//    'In numbers' => 'В цифрах', // +
//    'our brands' => 'наши бренды', // +
    'Slogan Slogan Slogan' => 'Slogan Slogan Slogan', // -
//    'Visit website' => 'Посетить вебсайт', // ++++
//    'To main' => 'На главную', // ++
//    'employees' => 'сотрудников', // +
//    'products items' => 'наименований продукции', // +++
//    'Products we offer' => 'Продукты, которые мы предлагаем', // ++
//    'Show all results' => 'Показать все результаты поиска', // ++++
//    'News' => 'Новости', /// +++++
//    'About us' => 'О нас', /// +++
//    'Other news' => 'Другие новости', /// ?
//    'Share' => 'Поделиться', //
//    'Home' => 'Главная', /// ++++
//    'Site map' => 'Карта сайта', /// +++++
//    'Search' => 'Поиск', //// ?
//    'Found' => 'Найдено', //// --
//    'Not found' => 'Не найдено', //// --
//    'Event gallery' => 'Галлерея', /// --
//    'Sorry, page not found' => 'К сожалению, страница не найдена', // ++++++
//    'Close' => 'Закрыть', // ++++,



'Learn more' => 'Узнать больше',
'Other news' => 'Больше новостей',
'Share' => 'Поделиться',
'Search' => 'Поиск',
'Found' => 'Найдено',
'Not found' => 'Не найдено',
'Event gallery' => 'Галлерея событий',
'Topic' => 'Тема',
'email' => 'E-Mail',
'phone' => 'Тел.:',
'Forms' => 'Формы',

    'Contacts' => 'Связаться с нами',
    'Site map' => 'Карта сайта',
    'About us' => 'О Нас',
    'our brands' => 'НАШИ БРЕНДЫ',
    'Choose a country' => 'Выберите страну',
    'Home' => 'Главная',
    'News' => 'Новости',
    'In the world' => 'В МИРЕ',
    'In numbers' => 'В ЦИФРАХ',
    'Products we offer' => 'ПРОДУКТЫ, КОТОРЫЕ МЫ ПРЕДЛАГАЕМ:',
    'products items' => 'наименований продукции',
    'Customer Service' => 'Служба поддержки клиентов:',
    'export Department' => 'По вопросам поставки продукции по територии Украины :',
    'Human Resources Department' => 'Отдел кадров:',
    'FeedbackForm' => 'ФОРМА ОБРАТНОЙ СВЯЗИ',
    'name' => 'Ваше имя и фамилия',
    'question' => 'Опишите Ваш вопрос',
    'Choose topic' => 'Выберите тему',
    'send' => 'Отправить',
    'Close' => 'Закрыть',
    'Show all results' => 'Показать все результаты поиска',
    'Sorry, page not found' => 'Извините, такая страница не найдена',
    'To main' => 'Перейти на главную',
    'Visit website' => 'Посетить вебсайт',
    'employees' => '',







];