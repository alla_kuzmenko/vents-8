<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 * @version 28/03/2015
 */
return [

    'Learn more' => 'Weiterlesen',
    'Other news' => 'Weitere Nachrichten',
    'Share' => 'Teilen',
    'Search' => 'Suche',
    'Found' => 'Gefunden',
    'Not found' => 'Nicht gefunden',
    'Event gallery' => 'Bildergalerie',
    'Topic' => 'Thema',
    'email' => 'E-Mail',
    'phone' => 'Telefon',
    'Forms' => 'Formen',

    'Contacts' => 'Kontaktieren Sie uns',
    'Site map' => 'Seitenübersicht',
    'About us' => 'Über uns',
    'our brands' => 'UNSERE MARKEN',
    'Choose a country' => 'Land auswählen',
    'Home' => 'Startseite',
    'News' => 'Nachrichten',
    'In the world' => 'AUF DER WELT',
    'In numbers' => 'IN ZAHLEN',
    'Products we offer' => 'VON UNS ANGEBOTENE PRODUKTE:',
    'products items' => 'Produkte',
    'Customer Service' => 'Kundendienst:',
    'export Department' => 'Zur Lieferung der Produkte auf dem Gebiet der Ukraine:',
    'Human Resources Department' => 'Personalabteilung:',
    'FeedbackForm' => 'FEEDBACKFORMULAR',
    'name' => 'Ihr Vor- und Nachname',
    'question' => 'Beschreiben Sie bitte Ihre Frage',
    'Choose topic' => 'Thema auswählen',
    'send' => 'Senden',
    'Close' => 'Schließen',
    'Show all results' => 'Alle Suchergebnisse anzeigen',
    'Sorry, page not found' => 'ENTSCHULDIGUNG, DIESE SEITE WURDE NICHT GEFUNDEN',
    'To main' => 'Zur Hauptseite gehen',
    'Visit website' => 'Webseite besuchen',
    'employees' => 'Mitarbeiter',
];
