<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 * @version 28/03/2015
 */
return [
    //module name
//    'Forms' => 'Forms',
//    'FeedbackForm' => 'Feedback Form',
//    'Topic' => 'Topic',
//    'name' =>'Your name and surname',
//    'question' =>'Describe your question',
//    'email' => 'Email',
//    'Choose topic' => 'Choose topic',
//    'phone' => 'Phone',
//    'send' => 'Send',
    'Feedback form' =>'Feedback form',


    'Learn more' => 'Learn more',
    'Other news' => 'Other news',
    'Share' => 'Share',
    'Search' => 'Search',
    'Found' => 'Found',
    'Not found' => 'Not found',
    'Event gallery' => 'Event gallery',
    'Topic' => 'Topic',
    'email' => 'email',
    'phone' => 'phone',
    'Forms' => 'Forms',


    'Contacts' => 'Contact us',
    'Site map' => 'Site Map',
    'About us' => 'About us',
    'our brands' => 'OUR BRANDS',
    'Choose a country' => 'Choose a country',
    'Home' => 'Home',
    'News' => 'News',
    'In the world' => 'IN THE WORLD',
    'In numbers' => 'IN NUMBERS',
    'Products we offer' => 'PRODUCTS WE OFFER:',
    'products items' => 'product items',
    'Customer Service' => 'Customer Service:',
    'export Department' => 'For delivery of products on the territory of Ukraine :',
    'Human Resources Department' => 'HR Department:',
    'FeedbackForm' => 'FEEDBACK FORM',
    'name' => 'Your name and surname',
    'question' => 'Describe your question',
    'Choose topic' => 'Choose topic',
    'send' => 'Send',
    'Close' => 'Close',
    'Show all results' => 'Show all results',
    'Sorry, page not found' => 'SORRY, PAGE NOT FOUND',
    'To main' => 'Go to Main page',
    'Visit website' => 'Visit website',
    'employees' => 'employees',



];
