<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
return [
    // Contacts page
//    'Contacts' => 'Contacts',
    'For delivery of products on the territory of Ukraine' => 'For delivery of products on the territory of Ukraine',
//    'Customer Service' => 'Customer Service',
    'free Ukraine from fixed phones' => 'free Ukraine from fixed phones',
//    'export Department' => 'Export Department',
//    'Human Resources Department' => 'HR Department',
    'address_vents' => 'Ukraine, 01030, Kiev, 1 M.Kotsyubinskogo Str.',
    // 'Feedback form'
    // start page
//    'Choose a country' => 'Choose a country',
//    'In the world' => 'In the world',
//    'Learn more' => 'Learn more',
//    'In numbers' => 'In numbers',
//    'our brands' => 'our brands',
    'Slogan Slogan Slogan' => 'Slogan Slogan Slogan',
//    'Visit website' => 'Visit website',
//    'To main' => 'To main',
//    'employees' => 'employees',
//    'products items' => 'products items',
//    'Products we offer' => 'Products we offer',
//    'Show all results' => 'Show all results',
    'News',
    'About us',
    'Other news',
    'Share',
    'Home',
    'Site map',
    'Search',
    'Found',
    'Not found',
    'Event gallery',
    'Sorry, page not found',
    'Close',


'Learn more' => 'Learn more',
'Other news' => 'Other news',
'Share' => 'Share',
'Search' => 'Search',
'Found' => 'Found',
'Not found' => 'Not found',
'Event gallery' => 'Event gallery',
'Topic' => 'Topic',
'email' => 'email',
'phone' => 'phone',
'Forms' => 'Forms',

'Contacts' => 'Contact us',
'Site map' => 'Site Map',
'About us' => 'About us',
'our brands' => 'OUR BRANDS',
'Choose a country' => 'Choose a country',
'Home' => 'Home',
'News' => 'News',
'In the world' => 'IN THE WORLD',
'In numbers' => 'IN NUMBERS',
'Products we offer' => 'PRODUCTS WE OFFER:',
'products items' => 'product items',
'Customer Service' => 'Customer Service:',
'export Department' => 'For delivery of products on the territory of Ukraine :',
'Human Resources Department' => 'HR Department:',
'FeedbackForm' => 'FEEDBACK FORM',
'name' => 'Your name and surname',
'question' => 'Describe your question',
'Choose topic' => 'Choose topic',
'send' => 'Send',
'Close' => 'Close',
'Show all results' => 'Show all results',
'Sorry, page not found' => 'SORRY, PAGE NOT FOUND',
'To main' => 'Go to Main page',
'Visit website' => 'Visit website',
'employees' => 'employees',




];