<?php

namespace frontend\components;

use frontend\modules\seo\models\SeoMetatag;
use yii\helpers\Url;
use Yii;
use frontend\modules\configs\models\Language;

/**
 * Class BaseController
 *
 * @package frontend\controllers
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2014, Thread
 */
abstract class BaseController extends \yii\web\Controller
{
    /**
     * Базовий layout
     * @var string
     */
    //public $layout = "@app/layouts/main";

    /**
     * Хлібні крихти
     * @var array ['label'=>'', url=>'']
     */
    public $breadcrumbs = [];

    /**
     * Назва базового методу дії
     * @var string
     */
    public $defaultAction = 'index';

    /**
     * // TODO:: ПЕРЕДАЛАТЬ КАКУ, с поиском
     *
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {

        \Yii::$app->geolocation->redirectOnBrowserLanguage();

        Yii::$app->oldbrowser->init();

        /** @var SeoMetatag $seo */
//        $seo = SeoMetatag::findBase()->one();
//        if ($seo) {
//            \Yii::$app->view->title = $seo->getMetaTitle();
//            \Yii::$app->view->registerMetaTag([
//                'name' => 'description',
//                'content' => $seo->getMetaTitle()
//            ]);
//        }

        $searchCondition = \Yii::$app->getRequest()->get('search', null);

        if ($searchCondition) {
            return $this->redirect(Url::to(['/page/find/index', 'condition' => $searchCondition]));
        }

        return parent::beforeAction($action);
    }

    public function init()
    {
        parent::init();

        $userGuest = Yii::$app->getUser()->isGuest;
        $model = new Language();
        $items = $model->getLanguages();

        foreach ($items as $key => $lang) {

            if ($userGuest) {
                if ($lang['local'] == Yii::$app->language &&  isset($lang['only_editor']) && $lang['only_editor'] == 1) {
                    Header('Location: ' . Yii::$app->getUrlManager()->baseUrl . '/error_404', 301);
                    exit;
                }
            }
        }
    }

    /**
     * @param $model
     * @return string
     */
    public static function getHtmlUrlToBack($model)
    {
        if (method_exists($model, 'getUrlToBack')) {

            $html = '<div class="fixed-block" style="position: fixed; z-index: 9999;">
                    <div class="btn btn-info" style="width: 100%;
    height: 75px;
    -ms-box-sizing: border-box;
    box-sizing: border-box;
    background-color: #e8edef;
    text-transform: uppercase;
    color: #0c4da1;
    line-height: 75px;
    font-size: 12px;
    font-weight: 600;
    padding: 0 20px;
    position: relative;
    margin-top: 10px;
    display: block;">
                        <a href="' . $model->getUrlToBack() . '"> Отредактировать страницу </a>
                    </div>
                </div>';
        }

        $role = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());


        return (!Yii::$app->getUser()->isGuest && isset($role['admin'])) ? $html : '';
    }
}
