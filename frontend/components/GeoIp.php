<?php

namespace frontend\components;

use Yii;
use yii\web\Cookie;
use frontend\modules\configs\models\Language;

/**
 * Class GeoIp
 * @package frontend\components
 */
class GeoIp
{
    /**
     * GeoIp constructor.
     */
    public function __construct()
    {
        $this->getByBrowser();
    }

    /**
     * Set lang for user from it position
     */
    public function getByIp()
    {
        // check if session geo isset and geo extension has any data
        if (!Yii::$app->session->has('geo') && $geo = Yii::$app->geolocation->getInfo()) {
            // catch current lang from url
            $url = explode("/", substr($_SERVER["REQUEST_URI"], 1));
            // check if lang is in database
            if (Language::getDefaultByUrl($url[0])) {
                // check if isset geo country language and it isset on Language table
                if ($geo["geoplugin_countryCode"] && Language::exist($geo["geoplugin_countryCode"])) {
                    // Set in session lang and redirect to geo lang
                    Yii::$app->session->set('geo', $geo["geoplugin_countryCode"]);
                    $geoLang = strtolower($geo["geoplugin_countryCode"]);
                    header("Location: http://{$_SERVER['HTTP_HOST']}/{$geoLang}{$_SERVER['REQUEST_URI']}");
                    exit;
                }
            }
        }
    }

    /**
     * Set lang for user from it browser
     */
    public function getByBrowser()
    {
        // check if isset cookie userLang
        if (!Yii::$app->request->cookies->get('userLang')) {
            // catch current lang from url
            $url = explode("/", substr($_SERVER["REQUEST_URI"], 1));
            // check if lang is in database
            if (Language::getDefaultByUrl($url[0])) {
                // get lang from browser
                if ($lang = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                    // convert lang uk_UA to uk-UA
                    $lang = str_replace('_', '-', $lang);
                    // check if isset lang in database
                    if ($userLang = Language::getByLocal($lang)) {
                        // set cookie userLang
                        $cookie = new Cookie([
                            'name' => 'userLang',
                            'value' => 1,
                            'expire' => time() + 86400 * 365,
                        ]);
                        Yii::$app->getResponse()->getCookies()->add($cookie);
                        // check if userLang is default
                        if ($userLang['default'] != 1) {
                            //redirect user to page with his lang
                            header("Location: http://{$_SERVER['HTTP_HOST']}/{$userLang['alias']}{$_SERVER['REQUEST_URI']}");
                        }
                    }
                }
            }
        }
    }
}
