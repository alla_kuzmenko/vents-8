<?php
namespace frontend\components\oldbrowser;

use Yii;
use yii\base\Component;

/***
 * Class OldBrowser
 * @package frontend\components\oldbrowser
 */
class OldBrowser extends Component
{
    // Browser
    public $browsers = [
        'Firefox' => 35,
        'Mozilla' => 30,
        'Opera' => 15,
        'Chrome' => 25,
        'IE' => 9,
        'Safari' => 7,
    ];

    public $layout = '//layouts/main';

    protected $_detect;

    /**
     * @throws \yii\base\ExitException
     */
    public function init()
    {
        parent::init();

        require_once __DIR__ . '/lib/Mobile_Detect.php';

        $this->_detect = new \Mobile_Detect();

        foreach ($this->browsers as $browser => $version_type) {
            if (
                !$this->_detect->isTablet() &&
                !$this->_detect->isMobile() &&
                ($this->_detect->version($browser)) &&
                ($this->_detect->version($browser) < $version_type)
            ) {
                //echo $browser . ' ' . $this->_detect->version($browser);
                Yii::$app->controller->layout = '//../components/oldbrowser/views/layouts/main';
                echo Yii::$app->controller->render('//../components/oldbrowser/views/index');
                Yii::$app->end();
            }
        }
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return call_user_func_array(array($this->_mobiledetect, $name), $arguments);
    }
}