<?php

namespace frontend\components\oldbrowser\assets;

use yii\web\AssetBundle;

/**
 * Class Asset
 * @package frontend\components\oldbrowser\assets
 */
class AppAsset extends AssetBundle {

    public $sourcePath = __DIR__;

    public $css = [
        'css/old_browser_styles.css',
    ];

    public $depends = [
        \yii\web\JqueryAsset::class,
    ];
}
