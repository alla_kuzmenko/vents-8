<?php
use frontend\components\oldbrowser\assets\AppAsset;

$bundle = AppAsset::register($this);
?>

<div class="wrap">
    <div class="main old_br_main">
        <h1>СAUTION! You are using the out-of-date browser</h1>

        <p>This site is built on the advanced, modern technologies and does not support older browsers.</p>

        <p>We strongly recommend that you choose and install any of the modern browsers. It's free and takes only a few
            minutes.</p><br/>
        <table class="brows" cellspacing="0">
            <tr>
                <td style="height: 140px; vertical-align: top;"><a href="http://www.microsoft.com/windows/Internet-explorer/default.aspx"
                                              target="_blank"><img src="<?= $bundle->baseUrl ?>/img/ie.jpg"
                                                                   alt="Internet Explorer"/></a></td>
                <td style="height: 140px; vertical-align: top;"><a href="http://www.opera.com/download/" target="_blank"><img
                            src="<?= $bundle->baseUrl ?>/img/op.jpg" alt="Opera Browser"/></a></td>
                <td style="height: 140px; vertical-align: top;"><a href="http://www.mozilla.com/firefox/" target="_blank"><img
                            src="<?= $bundle->baseUrl ?>/img/mf.jpg" alt="Mozilla Firefox"/></a></td>
                <td style="height: 140px; vertical-align: top;"><a href="http://www.google.com/chrome" target="_blank"><img
                            src="<?= $bundle->baseUrl ?>/img/gc.jpg" alt="Google Chrome"/></a></td>
                <td style="height: 140px; vertical-align: top;"><a href="http://www.apple.com/safari/download/" target="_blank"><img
                            src="<?= $bundle->baseUrl ?>/img/as.jpg" alt="Apple Safari"/></a></td>
            </tr>
            <tr class="brows_name">
                <td><a href="http://www.microsoft.com/windows/Internet-explorer/default.aspx" target="_blanck">Internet
                        Explorer</a></td>
                <td><a href="http://www.opera.com/download/" target="_blank">Opera Browser</a></td>
                <td><a href="http://www.mozilla.com/firefox/" target="_blank">Mozilla Firefox</a></td>
                <td><a href="http://www.google.com/chrome" target="_blank">Google Chrome</a></td>
                <td><a href="http://www.apple.com/safari/download/" _blank="_blanck">Apple Safari</a></td>
            </tr>
        </table>
        <h2>Why do I need to change the browser?</h2>

        <p>Your browser is not just a browser of the old version, and outdated browser of old generation browser. He can
            not provide all the features that can provide modern browsers, and its speed is several times lower! Your
            browser is not able to correctly display the majority of sites.</p>

        <p>If for some reason you do not have access to the possibility of installing programs, we recommend using the
            "portable" versions of browsers. They do not require installation on your computer and work with any disk or
            your stick: <a href="http://portableapps.com/apps/internet/firefox_portable" target="_blanck">Mozilla
                Firefox</a> or <a href="http://portableapps.com/apps/internet/google_chrome_portable" target="_blanck">Google
                Chrome</a>.</p>
    </div>
</div>
