<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
return [
    /**
     * CORE
     */
    'page' => [
        'class' => backend\modules\page\Page::class,
    ],
    'news' => [
        'class' => backend\modules\news\News::class,
    ],
    'user' => [
        'class' => thread\modules\user\User::class,
    ],
    'seo' => [
        'class' => backend\modules\seo\Seo::class,
    ],
    'location' => [
        'class' => backend\modules\location\Location::class,
    ],
    'menu' => [
        'class' => backend\modules\menu\Menu::class,
    ],
    'brands' => [
        'class' => backend\modules\brands\Brands::class,
    ],
    'forms' => [
        'class' => \thread\modules\forms\Forms::class,
    ],
    'aboutus' => [
        'class' => \thread\modules\aboutus\Aboutus::class,
    ],
    'contacts' => [
        'class' => \thread\modules\contacts\Contacts::class,
    ],
    'configs' => [
        'class' => \thread\modules\configs\Configs::class,
    ],
];
