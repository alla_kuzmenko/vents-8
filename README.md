Vents Project
=============

1. Clone project
2. Run `php init` command. Choose 0 - Development environment
3. Set your db connection properties
4. Implement migrations
5. Enjoy ;-)