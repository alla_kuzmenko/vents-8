<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2016, Thread
 */

$rootDir = dirname(dirname(__DIR__));
/**
 * BACKEND
 */
Yii::setAlias('layout-backend',  $rootDir . DIRECTORY_SEPARATOR . 'backend/themes/inspinia/layouts');
