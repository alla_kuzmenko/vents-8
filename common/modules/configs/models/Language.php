<?php

namespace common\modules\configs\models;

use Yii;

class Language extends \thread\modules\configs\models\Language
{
    /**
     * @return null|string
     */
    public function getImage()
    {
        /** @var contacts $Module */
        $Module = Yii::$app->getModule('configs');
        $path = $Module->getUploadPath();
        $url = $Module->getUploadUrl();
        $image = null;
        if (isset($this->img_flag) && file_exists($path . $this->img_flag)) {
            $image = $url . $this->img_flag;
        }
        return $image;
    }

}
