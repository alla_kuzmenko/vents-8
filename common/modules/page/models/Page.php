<?php
namespace common\modules\page\models;

use thread\modules\page\models\Page as ThreadPageModel;
use thread\modules\seo\interfaces\SeoInterface;

class Page extends ThreadPageModel implements SeoInterface
{
    /** Для сео виджета */
    const COMMON_NAMESPACE = self::class;

    /** fore seo */
    public static function getModelNamespace()
    {
        return self::class;
    }

}
