<?php
namespace common\modules\page\models;

use yii\helpers\ArrayHelper;
//
use thread\app\base\models\ActiveRecordLang;
use thread\app\model\Language;
//
use common\modules\page\Page as PageModule;

/**
 * Class InNumbersWidgetLang
 * @package common\modules\page\models
 */
class InNumbersWidgetLang extends ActiveRecordLang
{
    /**
     * Db connection
     * @return string
     */
    public static function getDb()
    {
        return PageModule::getDb();
    }

    /**
     * @inheritdoc
     * @return string
     */
    public static function tableName()
    {
        return '{{%in_numbers_widget_lang}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['title'], 'required'],
                ['rid', 'exist', 'targetClass' => InNumbersWidget::class, 'targetAttribute' => 'id'],
                ['content', 'string'],
                ['title', 'string', 'max' => 255],
            ]
        );
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'rid' => \Yii::t('app', 'rid'),
            'lang' => \Yii::t('app', 'lang'),
            'title' => \Yii::t('app', 'title'),
            'content' => \Yii::t('app', 'content'),
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return [
            'backend' => ['title', 'content'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Language::class, ['lang' => 'id']);
    }
}
