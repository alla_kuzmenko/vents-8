<?php
namespace common\modules\page\models;

use thread\app\base\models\ActiveRecord;
//
use common\modules\page\Page as PageModule;

/**
 * Class InNumbersWidget
 * @package common\modules\page\models
 */
class InNumbersWidget extends ActiveRecord
{
    /**
     * Using db connection
     * @return string
     */
    public static function getDb()
    {
        return PageModule::getDb();
    }

    /**
     * Page table name
     * @return string
     */
    public static function tableName()
    {
        return '{{%in_numbers_widget}}';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())],
            [['position', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return [
            'published' => ['published'],
            'deleted' => ['deleted'],
            'backend' => ['position', 'published', 'deleted'],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'ID'),
            'position' => \Yii::t('app', 'Position'),
            'created_at' => \Yii::t('app', 'Create time'),
            'updated_at' => \Yii::t('app', 'Update time'),
            'published' => \Yii::t('app', 'Published'),
            'deleted' => \Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(InNumbersWidgetLang::class, ['rid' => 'id'])
            ->andOnCondition(['lang' => \Yii::$app->language]);
    }
}
