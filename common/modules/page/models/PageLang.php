<?php

namespace common\modules\page\models;

use Yii;
use thread\modules\page\models\PageLang as ThreadPageLangModel;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 * @copyright (c) 2016, VipDesign
 */
class PageLang extends ThreadPageLangModel
{
    
}
