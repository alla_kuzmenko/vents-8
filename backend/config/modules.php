<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
return [
    /**
     * CORE
     */
    'gridview' => [
        'class' => \kartik\grid\Module::class,
    ],
    'home' => [
        'class' => backend\modules\home\Home::class,
    ],
    'page' => [
        'class' => backend\modules\page\Page::class,
    ],
    'news' => [
        'class' => backend\modules\news\News::class,
    ],
    'user' => [
        'class' => backend\modules\user\User::class,
    ],
    'seo' => [
        'class' => backend\modules\seo\Seo::class,
    ],
    'location' => [
        'class' => backend\modules\location\Location::class,
    ],
    'menu' => [
        'class' => backend\modules\menu\Menu::class,
    ],
    'brands' => [
        'class' => backend\modules\brands\Brands::class,
    ],
    'aboutus' => [
        'class' => backend\modules\aboutus\Aboutus::class,
    ],
    'forms' => [
        'class' => backend\modules\forms\Forms::class,
    ],
    'configs' => [
        'class' => backend\modules\configs\Configs::class,
    ],
    'contacts' => [
        'class' => backend\modules\contacts\Contacts::class,
    ],
];
