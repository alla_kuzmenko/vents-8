<?php
use thread\app\base\models\ActiveRecord;
//
use backend\modules\aboutus\models\{
    Item, ItemLang
};
use backend\themes\inspinia\widgets\forms\ActiveForm;

/**
 * @var Item $model
 * @var ItemLang|ActiveRecord $modelLang
 */
?>

<?php $form = ActiveForm::begin(); ?>

<?= \backend\themes\inspinia\widgets\forms\Form::submit($model, $this); ?>
<?= $form->field($model, 'image_link')->imageOne($model->getArticleImage()); ?>
<?= $form->field($model, 'image_link_s')->imageOne($model->getArticleImageSmall()); ?>
<?= $form->field($modelLang, 'title')->textInput(['maxlength' => true]); ?>
<?= $form->field($modelLang, 'content')->textArea(['rows' => '3']); ?>
<?= $form->field($model, 'published')->checkbox(); ?>
<?= \backend\themes\inspinia\widgets\forms\Form::submit($model, $this); ?>
<?php ActiveForm::end();
