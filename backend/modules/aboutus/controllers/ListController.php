<?php
namespace backend\modules\aboutus\controllers;

use Yii;
use backend\modules\aboutus\models\search\Item;
use backend\modules\aboutus\models\ItemLang;
use thread\actions\fileapi\UploadAction;
use thread\app\base\controllers\BackendController;
use yii\helpers\ArrayHelper;

/**
 * Class ListController
 *
 * @package backend\modules\aboutus\controllers
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class ListController extends BackendController
{
    public $model = Item::class;
    public $modelLang = ItemLang::class;
    public $title = 'About us';

    /**
     * @return array
     */

    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'fileupload' => [
                    'class' => UploadAction::class,
                    'path' => Yii::$app->getModule('aboutus')->getUploadPath()
                ],
            ]
        );
    }
}
