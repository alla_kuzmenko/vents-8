<?php
namespace backend\modules\aboutus;

use Yii;
use common\modules\aboutus\Aboutus as AboutusModule;

/**
 * Class Aboutus
 *
 * @package backend\modules\aboutus
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Aboutus extends AboutusModule
{
    /**
     * Number of elements in GridView
     * @var int
     */
    public $itemOnPage = 20;

    /**
     * Image upload path
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getAlias('@uploads') . '/' . $this->name . '/';
    }

    /**
     * Image upload URL
     * @return string
     */
    public function getUploadUrl()
    {
        return '/uploads/' . $this->name . '/';
    }
}
