<?php
namespace backend\modules\aboutus\models;

use common\modules\aboutus\models\Item as CommonItemModel;
use Yii;

/**
 * Class Item
 *
 * @package backend\modules\aboutus\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Item extends CommonItemModel
{
    /**
     * @return null|string
     */
    public function getArticleImage()
    {
        /** @var aboutus $Module */
        $Module = Yii::$app->getModule('aboutus');
        $path = $Module->getUploadPath();
        $url = $Module->getUploadUrl();
        $image = null;
        if (isset($this->image_link) && file_exists($path . $this->image_link)) {
            $image = $url . $this->image_link;
        }
        return $image;
    }

    /**
     * @return null|string
     */
    public function getArticleImageSmall()
    {
        /** @var aboutus $Module */
        $Module = Yii::$app->getModule('aboutus');
        $path = $Module->getUploadPath();
        $url = $Module->getUploadUrl();
        $image = null;
        if (isset($this->image_link_s) && file_exists($path . $this->image_link_s)) {
            $image = $url . $this->image_link_s;
        }
        return $image;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        $title = !empty($this->lang)
            ? $this->lang->title
            : "{'ru-RU' " . $this->default_title . "}";
        return $title;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->language == 'ru-RU' && isset($this->lang->title))
            $this->default_title =  $this->lang->title;

        return parent::beforeSave($insert);
    }
}
