<?php
namespace backend\modules\aboutus\models\search;

use backend\modules\aboutus\Aboutus as AboutusModule;
use thread\app\base\models\query\ActiveQuery;
use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\aboutus\models\Item as ItemModel;

class Item extends ItemModel
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['alias', 'image_link', 'link'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @param ActiveQuery $query
     * @param array $params
     * @return ActiveDataProvider
     */
    public function baseSearch($query, $params)
    {
        /** @var AboutusModule $module */
        $module = Yii::$app->getModule('aboutus');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $module->itemOnPage
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->joinWith(['lang']);

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);


        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ItemModel::find()->with(['lang'])->undeleted();
        return $this->baseSearch($query, $params);
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function trash($params)
    {
        $query = ItemModel::find()->with(['lang'])->deleted();
        return $this->baseSearch($query, $params);
    }
}
