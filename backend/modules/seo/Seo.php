<?php

namespace backend\modules\seo;
use common\modules\seo\Seo as CommonSeoModule;

/**
 * @author Fantamas
 */
class Seo extends CommonSeoModule
{
    public $itemOnPage = 20;
}
