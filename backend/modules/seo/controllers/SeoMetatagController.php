<?php
namespace backend\modules\seo\controllers;

use backend\modules\seo\models\search\SeoMetatag;
use backend\modules\seo\models\SeoMetatagLang;
use thread\app\base\controllers\BackendController;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class SeoMetatagController extends BackendController
{
    public $model = SeoMetatag::class;
    public $modelLang = SeoMetatagLang::class;
    public $title = 'SEO Meta Tags';
}
