<?php
namespace backend\modules\seo\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
//
use backend\modules\seo\Seo as SeoModule;
use backend\modules\seo\models\SeoMetatag as SeoMetatagModel;
use yii\helpers\ArrayHelper;

class SeoMetatag extends SeoMetatagModel
{
    public $title;


    public function rules()
    {
        return [
            [['url', 'url_hash'], 'safe'],
            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())]
        ];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), Model::scenarios());
    }


    /**
     * @param $query
     * @param $params
     * @return ActiveDataProvider
     */
    public function baseSearch($query, $params)
    {
        /** @var SeoModule $module */
        $module = Yii::$app->getModule('seo');
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => $module->itemOnPage
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC
                    ]
                ]
            ]
        );

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['published' => $this->published]);

        return $dataProvider;
    }

    /**
     * Description
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeoMetatagModel::find()->with(['lang'])->undeleted();
        return $this->baseSearch($query, $params);
    }

    /**
     * Description
     *
     * @param $params
     * @return ActiveDataProvider
     */
    public function trash($params)
    {
        $query = SeoMetatagModel::find()->with(['lang'])->deleted();
        return $this->baseSearch($query, $params);
    }
}
