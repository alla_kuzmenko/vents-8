<?php
use backend\themes\inspinia\widgets\GridView;

/**
 * @var \backend\modules\seo\models\search\SeoMetatag $model
 */

$filter = new \backend\modules\seo\models\search\SeoMetatag();
$filter->setAttributes(Yii::$app->getRequest()->get('SeoMetatag'));

echo GridView::widget(
    [
        'dataProvider' => $model->search(Yii::$app->request->queryParams),
        'filterModel' => $filter,
        'columns' => [
            'url',
//            'lang.title',
            [
                'class' => \thread\widgets\grid\ActionCheckboxColumn::class,
                'attribute' => 'published',
                'action' => 'published',
            ],
            [
                'class' => \backend\themes\inspinia\widgets\gridColumns\ActionColumn::class,
                'deleteDisabledIds' => [1]
            ],
        ]
    ]
);
