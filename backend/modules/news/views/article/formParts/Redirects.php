<?php
use yii\helpers\Html;
use thread\modules\configs\models\Language;

$items = (new Language)->getLanguages();

/**
 *
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c), Thread
 */
$langs = $model->getRedrects();

foreach ($items as $item) {
    if ($item['local'] != Yii::$app->language) {
        ?>
        <div class="form-group field-seolang-description">
            <?= Html::label($item['local'], [
                'class' => 'control-label',
                ''
            ]) ?>
            <?= Html::input('text', 'lang[' . $item['local'] . ']', ($langs->{$item['local']})??'', [
                'class' => 'form-control',
                'style' => 'width:100%',
                'placeholder' => $item['local']
            ]) ?>
        </div>
        <?php
    }
}

