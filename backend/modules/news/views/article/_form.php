<?php
use thread\modules\seo\widgets\seo\SeoWidget;
use thread\app\base\models\ActiveRecord;
//
use backend\modules\news\models\{
    Article, ArticleLang
};
use backend\themes\inspinia\widgets\{
    forms\ActiveForm, forms\Form, Tabs
};

/**
 * @var Article $model
 * @var ArticleLang|ActiveRecord $modelLang
 */
?>
<?php $form = ActiveForm::begin(); ?>

    <div class="row form-group">
        <div class="col-sm-6">
            <div class="btn btn-warning">
                <a href="<?= $model->getFrontendUrl() ?>"> Перейти на сайт </a>
            </div>

        </div>
        <div class="col-sm-6">
            <?= Form::submit($model, $this) ?>
        </div>
    </div>

<?= Tabs::widget([
    'items' => [
        [
            'label' => Yii::t('app', 'General information'),
            'content' => $this->render('formParts/_main', [
                'form' => $form,
                'model' => $model,
                'modelLang' => $modelLang
            ])
        ],
        [
            'label' => Yii::t('app', 'SEO'),
            'content' => SeoWidget::widget(['nameSpaceModel' => Article::COMMON_NAMESPACE])
        ],
        [
            'label' => Yii::t('app', 'Redirects'),
            'content' => $this->render('formParts/Redirects', [
                'form' => $form,
                'model' => $model,
                'modelLang' => $modelLang
            ])
        ]
    ]
]) ?>

<?= $form->field($model, 'default_title')->hiddenInput(['value' => '']) ?>
<?= Form::submit($model, $this) ?>
<?php ActiveForm::end();
