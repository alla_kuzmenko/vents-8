<?php
namespace backend\modules\news\models;

use backend\modules\configs\models\Language;
use backend\modules\news\News;
use common\modules\news\models\Article as CommonArticleModel;
use common\modules\seo\models\Seo;
use thread\modules\seo\behaviors\SeoBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Article extends CommonArticleModel
{

    /**
     * @return array
     */

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'SeoBehavior' => [
                    'class' => SeoBehavior::class,
                    'modelNamespace' => self::COMMON_NAMESPACE
                ],
            ]
        );
    }

//    /**
//     * @inheritdoc
//     * @return array
//     */
//    public function rules()
//    {
//        return [
//            [['title', 'image_link', 'content'], 'required'],
//            [['title', 'image_link', 'description', 'default_title', 'content', 'published_time'], 'string'],
//            [['published_time'], 'date', 'format' => 'php:' . Yii::$app->modules['news']->params['format']['date'], 'timestampAttribute' => 'published_time'],
//            [['group_id', 'created_at', 'updated_at'], 'integer'],
//            [['default_title'], 'default', 'value' => ''],
//            [['published', 'deleted'], 'in', 'range' => array_keys(static::statusKeyRange())],
//        ];
//    }

    /**
     * @return null|string
     */
    public function getArticleImage()
    {
        /** @var News $newsModule */
        $newsModule = Yii::$app->getModule('news');
        $path = $newsModule->getUploadPath() . 'article/';
        $url = $newsModule->getUploadUrl() . 'article/';
        $image = null;
        if (!empty($this->image_link) && file_exists($path . $this->image_link)) {
            $image = $url . $this->image_link;
        }
        return $image;
    }

    /**
     * @return array
     */
    public function getArticleGallery()
    {
        /** @var News $newsModule */
        $newsModule = Yii::$app->getModule('news');
        $path = $newsModule->getUploadPath() . 'article/';
        $url = $newsModule->getUploadUrl() . 'article/';
        $images = [];
        if (!empty($this->gallery_link)) {
            $this->gallery_link = $this->gallery_link[0] == ',' ? substr($this->gallery_link, 1) : $this->gallery_link;
            $images = explode(',', $this->gallery_link);
        }
        $imagesSources = [];
        foreach ($images as $image) {
            if (file_exists($path . $image)) {
                $imagesSources[] = $url . $image;
            }
        }
        return $imagesSources;
    }


    /**
     * @return $this
     */
    public function getSeo()
    {
        return $this->hasOne(Seo::class, ['model_id' => 'id'])->andWhere(['model_namespace' => self::COMMON_NAMESPACE]);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        $title = !empty($this->lang)
            ? $this->lang->title
            : "{'ru-RU' " . $this->default_title . "}";
        return $title;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (Yii::$app->language == 'ru-RU' && isset($this->lang->title))
            $this->default_title = $this->lang->title;

        return parent::beforeSave($insert);
    }

    /**
     * @param $params
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params)
    {
        return (new search\Article())->search($params);
    }

    /**
     * @param $params
     * @return \yii\data\ActiveDataProvider
     */
    public function trash($params)
    {
        return (new search\Article())->trash($params);
    }

    /**
     * Получаем front url
     * @return string
     */
    public function getFrontendUrl()
    {
        $url = '/news/' . $this->alias;
        $lanModel = Language::getCurrentModel();

        if ($lanModel) {
            $url = ($lanModel['default']) ? $url : '/' . $lanModel['alias'] . $url;
        }

        return $url;
    }

    public function beforeValidate()
    {

        $langs = Yii::$app->getRequest()->post('lang', []);
        $this->redirects = json_encode($langs);

        return parent::beforeValidate();
    }

}
