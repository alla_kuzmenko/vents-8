<?php

namespace backend\modules\home\controllers;

use Yii;
//
use thread\app\base\controllers\BackendController;
use yii\helpers\Url;
use yii\web\ErrorAction;

/**
 * Class HomeController
 *
 * @package backend\modules\home\controllers
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2014, Thread
 */
class HomeController extends BackendController {

    public $layout = "@app/layouts/start";

    /**
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
                'view' => Yii::$app->getUser()->isGuest ? '@app/layouts/nologin-error.php' : '@app/layouts/error.php'
            ],
        ];
    }

    /**
     *
     * @return string
     */
    public function actionIndex()
    {
//        return $this->render('index');
        return $this->redirect(Url::toRoute(['/page/page/list']));
    }

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

}
