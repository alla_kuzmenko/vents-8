<?php

namespace backend\modules\home;

use thread\app\base\module\abstracts\Module;

/**
 * Class Home
 * 
 * @package frontend\modules\home
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2014, Thread
 */
class Home extends Module
{
    
}
