<?php

namespace backend\modules\user;

/**
 * Class User
 *
 * @package backend\modules\user
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
class User extends \common\modules\user\User
{
    public $username_attribute = 'username';
}
