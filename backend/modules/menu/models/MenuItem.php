<?php

namespace backend\modules\menu\models;

use Yii;

/**
 * @author Fantamas
 */
class MenuItem extends \common\modules\menu\models\MenuItem
{
    /**
     * Find base Item object for current language active and undeleted
     * @return mixed
     */
    public static function findBase()
    {
        return parent::findBase()->_lang()->enabled();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        $title = !empty($this->lang)
            ? $this->lang->title
            : "{'ru-RU' " . $this->default_title . "}";
        return $title;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->language == 'ru-RU' && isset($this->lang->title))
            $this->default_title =  $this->lang->title;

        return parent::beforeSave($insert);
    }
}
