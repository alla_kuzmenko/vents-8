<?php
namespace backend\modules\forms\models;

use Yii;
use thread\modules\forms\models\FeedbackForm as BaseFeedbackFormModel;

/**
 * Class FeedbackForm
 * @package common\modules\forms\models
 * 
 */

class FeedbackForm extends BaseFeedbackFormModel
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'question','topic_id', 'email'], 'required'],
            [['topic_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'question', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['phone'], 'string', 'min' => 5, 'max' => 20],
        ];
    }

    /**
     * @param $params
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params)
    {
        return (new search\FeedbackForm())->search($params);
    }

    /**
     * @param $params
     * @return \yii\data\ActiveDataProvider
     */
    public function trash($params)
    {
        return (new search\FeedbackForm())->trash($params);
    }
}
