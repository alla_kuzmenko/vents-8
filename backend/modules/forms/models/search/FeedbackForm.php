<?php
namespace backend\modules\forms\models\search;


use backend\modules\forms\Forms as FormsModule;
use thread\app\base\models\query\ActiveQuery;
use thread\app\model\interfaces\search\BaseBackendSearchModel;
use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\forms\models\FeedbackForm as FeedbackFormModel;
use yii\base\Model;

class FeedbackForm extends FeedbackFormModel implements BaseBackendSearchModel
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['topic_id'], 'integer'],
            [['name', 'question', 'email'], 'string', 'max' => 255],
            //[['email'], 'email'],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param ActiveQuery $query
     * @param array $params
     * @return ActiveDataProvider
     */
    public function baseSearch($query, $params)
    {
        /** @var FormsModule $module */
        $module = Yii::$app->getModule('forms');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $module->itemOnPage
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->joinWith(['topics']);

        $query->andFilterWhere(['in', 'topic_id', $this->topic_id]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', FeedbackFormModel::tableName() . '.email', $this->email]);
        $query->andFilterWhere(['like', 'phone', $this->phone]);
        $query->andFilterWhere(['like', 'question', $this->question]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FeedbackFormModel::find()->joinWith(['topics'])->undeleted();
        return $this->baseSearch($query, $params);
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function trash($params)
    {
        $query = FeedbackFormModel::find()->joinWith(['topics'])->deleted();
        return $this->baseSearch($query, $params);
    }
}
