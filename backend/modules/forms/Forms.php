<?php
namespace backend\modules\forms;

use common\modules\forms\Forms as CommonFormsModule;

/**
 * @author Alla Kuzmenko 
 */
class Forms extends CommonFormsModule
{
    /**
     * Number of elements in GridView
     * @var int
     */
    public $itemOnPage = 20;

}
