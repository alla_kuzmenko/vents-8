<?php

use backend\themes\inspinia\widgets\GridView;
use backend\modules\forms\models\Topic;

/**
 * @var $model \backend\modules\forms\models\search\FeedbackForm
 */

echo GridView::widget([
    'dataProvider' => $model->search(Yii::$app->request->queryParams),
    'title' => Yii::t('app', 'All calls'),
    'filterModel' => $filter,
    'columns' => [
        'name',
        'email',
        [
            'attribute' => 'topic_id',
            'value' => 'topics.lang.title',
            'filter' => Topic::getDropdownList()
        ],
        'question',
        'phone',
        [
            'class' => \backend\themes\inspinia\widgets\gridColumns\ActionColumn::class
        ],
    ]
]);
