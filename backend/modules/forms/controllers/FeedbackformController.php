<?php
namespace backend\modules\forms\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use thread\app\base\controllers\BackendController;
use thread\actions\{
    Create, ListModel, Update
};
use backend\modules\forms\models\{
    FeedbackForm, search\FeedbackForm as filterFeedbackFormModel
};

/**
 * @author Alla Kuzmenko
 */
class FeedbackformController extends BackendController
{
    public $title = 'Feedback form';
    public $model = FeedbackForm::class;
    public $filterModel = filterFeedbackFormModel::class;

    public function actions(){
        return ArrayHelper::merge(parent::actions(), [
            'list' => [
                'class' => ListModel::class,
                'modelClass' => $this->model,
                'layout' => 'crud',
            ],
            'create' => [
                'class' => Create::class,
            ],
            'update' => [
                'class' => Update::class,
            ],
        ]);
    }
}
