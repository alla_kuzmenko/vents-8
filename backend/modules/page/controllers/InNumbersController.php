<?php
namespace backend\modules\page\controllers;

use thread\app\base\controllers\BackendController;
//
use backend\modules\page\models\search\InNumbersSearchWidget;
use backend\modules\page\models\InNumbersWidgetLang;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class InNumbersController extends BackendController
{
    public $model = InNumbersSearchWidget::class;
    public $modelLang = InNumbersWidgetLang::class;
    public $title = 'Pages';
}
