<?php
use backend\themes\inspinia\widgets\forms\ActiveForm;
use backend\themes\inspinia\widgets\forms\Form;

?>

<?php $form = ActiveForm::begin(); ?>

<?= Form::submit($model, $this) ?>

<?= $form->field($modelLang, 'title')->textInput(['maxlength' => true]) ?>
<?= $form->field($modelLang, 'content')->textArea(['rows' => '3']) ?>
<?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'published')->checkbox() ?>

<?= Form::submit($model, $this) ?>

<?php ActiveForm::end(); ?>
