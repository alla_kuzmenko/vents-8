<?php
use thread\modules\seo\widgets\seo\SeoWidget;
//
use backend\modules\page\models\{
    Page, PageLang
};
use backend\themes\inspinia\widgets\forms\{
    ActiveForm, Form
};
use backend\themes\inspinia\widgets\Tabs;

/**
 * @var Page $model
 * @var PageLang $modelLang
 */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="row form-group">
    <div class="col-sm-6">
        <div class="btn btn-warning">
            <a href="<?= $model->getFrontendUrl() ?>"> Перейти на сайт </a>
        </div>

    </div>
    <div class="col-sm-6">
        <?= Form::submit($model, $this) ?>
    </div>
</div>

<?= Tabs::widget([
    'items' => [
        [
            'label' => Yii::t('app', 'General'),
            'content' => $this->render('parts/_main', [
                'form' => $form,
                'model' => $model,
                'modelLang' => $modelLang,
            ])
        ],
        [
            'label' => Yii::t('app', 'SEO'),
            'content' => SeoWidget::widget(['nameSpaceModel' => Page::COMMON_NAMESPACE])
        ],
    ],
]); ?>
<div class="row form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <?= $form->field($model, 'published')->checkbox() ?>
    </div>
</div>

<?= Form::submit($model, $this) ?>

<?php ActiveForm::end(); ?>
