<?php

namespace backend\modules\page\models;

use Yii;
use common\modules\page\models\PageLang as CommonPageLangModel;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 * @copyright (c) 2016, VipDesign
 */
class PageLang extends CommonPageLangModel
{
    
}
