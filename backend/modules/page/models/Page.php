<?php
namespace backend\modules\page\models;

use frontend\modules\configs\models\Language;
use Yii;
use thread\models\query\ActiveQuery;
use common\modules\page\models\Page as CommonPageModel;
use thread\modules\seo\behaviors\SeoBehavior;
use yii\helpers\ArrayHelper;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class Page extends CommonPageModel
{
    /**
     * Find base Page object for current language active and undeleted
     * @return ActiveQuery
     */
    public static function findBase()
    {
        return parent::findBase()->_lang()->enabled();
    }
    
    /**
     * Find ONE Page object in array by its alias
     * @param string $alias
     * @return array
     */
    public static function findByAlias($alias)
    {
        return self::findBase()->alias($alias)->asArray()->one();
    }

    /**
     * Behaviors
     *
     * @return array
     */
    public function behaviors()
    {

        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'SeoBehavior' => [
                    'class' => SeoBehavior::class,
                    'modelNamespace' => self::COMMON_NAMESPACE
                ]
            ]
        );
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        $title = !empty($this->lang)
            ? $this->lang->title
            : "{'ru-RU' " . $this->default_title . "}";
        return $title;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->language == 'ru-RU' && isset($this->lang->title))
            $this->default_title =  $this->lang->title;

        return parent::beforeSave($insert);
    }

    /**
     * Получаем front url
     * @return string
     */
    public function getFrontendUrl()
    {
        $url = '/'.$this->alias;
        $lanModel = Language::getCurrentModel();

        if ($lanModel) {
            $url = ($lanModel['default']) ? $url : '/'. $lanModel['alias'] . $url;
        }

        return $url;
    }
}
