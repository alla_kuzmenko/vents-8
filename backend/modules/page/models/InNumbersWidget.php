<?php
namespace backend\modules\page\models;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class InNumbersWidget extends \common\modules\page\models\InNumbersWidget
{
    /**
     * @return ActiveQuery
     */
    public static function findBase()
    {
        return parent::findBase()->_lang()->enabled();
    }
}
