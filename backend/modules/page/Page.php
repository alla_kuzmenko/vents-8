<?php
namespace backend\modules\page;

use common\modules\page\Page as CommonPageModule;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class Page extends CommonPageModule
{
    public $itemOnPage = 20;
}
