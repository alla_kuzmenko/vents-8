<?php

use backend\themes\inspinia\widgets\GridView;
use thread\widgets\grid\ActionDeleteColumn;
use thread\widgets\grid\ActionRestoreColumn;

/**
 *
 * @package admin\modules\aboutus\view
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 *
 */
echo GridView::widget([
    'dataProvider' => $model->trash(Yii::$app->request->queryParams),
    'columns' => [
        'lang.image',
        [
            'class' => ActionDeleteColumn::class,
        ],
        [
            'class' => ActionRestoreColumn::class
        ],
    ]
]);
