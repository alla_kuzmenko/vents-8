<?php
use thread\app\base\models\ActiveRecord;
//
use backend\modules\aboutus\models\{
    Item, ItemLang
};
use backend\themes\inspinia\widgets\forms\ActiveForm;

/**
 * @var Item $model
 * @var ItemLang|ActiveRecord $modelLang
 */
?>

<?php $form = ActiveForm::begin(); ?>

<?= \backend\themes\inspinia\widgets\forms\Form::submit($model, $this); ?>
<h2 style="color: darkgreen">Добавляемая картинка должна быть 1920х616 в формате JPG!!!</h2>
<?= $form->field($modelLang, 'image')->imageOne($modelLang->getImage()); ?>
<?= $form->field($modelLang, 'delivery_phone')->textArea(['rows' => '3']); ?>
<?= $form->field($modelLang, 'export_phone')->textArea(['rows' => '3']);?>
<?= $form->field($modelLang, 'customer_phone')->textArea(['rows' => '3']); ?>
<?= $form->field($modelLang, 'customer_info')->textArea(['rows' => '3']); ?>
<?= $form->field($modelLang, 'hr_phone')->textArea(['rows' => '3']); ?>
<?= $form->field($modelLang, 'address')->textArea(['rows' => '3']); ?>
<?= $form->field($modelLang, 'emails')->textArea(['rows' => '3']); ?>
<?= $form->field($model, 'published')->hiddenInput(['value'=> 1])->label(false) ?>
<?= \backend\themes\inspinia\widgets\forms\Form::submit($model, $this); ?>
<?php ActiveForm::end();
