<?php
use backend\themes\inspinia\widgets\GridView;

/**
 * @var \backend\modules\contacts\models\search\Contact $model
 */

echo GridView::widget([
    'dataProvider' => $model->search(Yii::$app->request->queryParams),
    'title' => Yii::t('app', 'Contacts'),
    'columns' => [
        'lang.delivery_phone',
        'lang.export_phone',
        'lang.customer_phone',
        'lang.customer_info',
        'lang.hr_phone',
        'lang.address',
        'lang.emails',
        [
            'class' => \backend\themes\inspinia\widgets\gridColumns\ActionColumn::class
        ],
    ]
]);
