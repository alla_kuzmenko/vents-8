<?php
namespace backend\modules\contacts\models;

use common\modules\contacts\models\ContactLang as CommonContactLangModel;
use Yii;

/**
 * Class ContactLang
 * @package backend\modules\contacts\models
 */
class ContactLang extends CommonContactLangModel
{
    /**
     * @return null|string
     */
    public function getImage()
    {
        /** @var contacts $Module */
        $Module = Yii::$app->getModule('contacts');
        $path = $Module->getUploadPath();
        $url = $Module->getUploadUrl();
        $image = null;
        if (isset($this->image) && file_exists($path . $this->image)) {
            $image = $url . $this->image;
        }
        return $image;
    }

}