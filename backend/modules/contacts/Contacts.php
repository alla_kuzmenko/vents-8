<?php

namespace backend\modules\contacts;

use common\modules\contacts\Contacts as CommonContactsModule;
use Yii;

/**
 * Class Contacts
 * @author Alla Kuzmenko
 * @copyright (c) 2016
 */
class Contacts extends CommonContactsModule
{
    /**
     * Number of elements in GridView
     * @var int
     */
    public $itemOnPage = 20;
    
    /**
     * Image upload path
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getAlias('@uploads') . '/' . $this->name . '/';
    }

    /**
     * Image upload URL
     * @return string
     */
    public function getUploadUrl()
    {
        return '/uploads/' . $this->name . '/';
    }
}
