<?php
namespace backend\modules\contacts\controllers;

use Yii;
use backend\modules\contacts\models\search\Contact;
use backend\modules\contacts\models\ContactLang;
use thread\actions\fileapi\UploadAction;
use thread\app\base\controllers\BackendController;
use yii\helpers\ArrayHelper;
use thread\actions\ListModel;

/**
 * Class ContactController
 *
 * @package backend\modules\contacts\controllers
 * @author Alla Kuzmenko
 * @copyright (c) 2016
 */
class ContactController extends BackendController
{
    public $model = Contact::class;
    public $modelLang = ContactLang::class;
    public $title = 'Contacts';


    public function actionVad()
    {
        echo 'ddd';
    }

    /**
     * @return array
     */


    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'list' => [
                    'class' => ListModel::class,
                    'modelClass' => $this->model,
                    'layout' => '@app/layouts/base',
                ],
                'fileupload' => [
                    'class' => UploadAction::class,
                    'path' => Yii::$app->getModule('contacts')->getUploadPath()
                ],
            ]
        );
    }
}
