<?php
namespace backend\modules\location\controllers;

use backend\modules\location\models\{
    City, CityLang, search\City as filterCityModel
};
use thread\app\base\controllers\BackendController;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class CityController extends BackendController
{
    public $model = City::class;
    public $modelLang = CityLang::class;
    public $filterModel = filterCityModel::class;
    public $title = 'City';

    /**
     * Get City by country
     */
    public function actionGetcitybycountry()
    {
        $query = (Yii::$app->request->get('country'))
            ? City::findBase()->andWhere(['location_country_id' => Yii::$app->request->get('country')])->all()
            : City::findBase()->all();

        echo json_encode(ArrayHelper::map($query, 'id', 'lang.title'));
    }

}
