<?php
namespace backend\modules\location\controllers;

use backend\modules\location\models\{
    Region, RegionLang, search\Region as filterRegionModel
};
use thread\app\base\controllers\BackendController;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class RegionController extends BackendController
{
    public $model = Region::class;
    public $modelLang = RegionLang::class;
    public $filterModel = filterRegionModel::class;
    public $title = 'Region';
}
