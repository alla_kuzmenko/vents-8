<?php
namespace backend\modules\location\controllers;

use backend\modules\location\models\{
    Country, CountryLang, search\Country as filterCountryModel
};
use thread\app\base\controllers\BackendController;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class CountryController extends BackendController
{
    public $model = Country::class;
    public $modelLang = CountryLang::class;
    public $filterModel = filterCountryModel::class;
    public $title = 'Country';

    /**
     * Get Country by region
     */
    public function actionGetcountrybyregion()
    {
        $query = (Yii::$app->request->get('region'))
            ? Country::findBase()->andWhere(['region_id' => Yii::$app->request->get('region')])->all()
            : Country::findBase()->all();

        echo json_encode(ArrayHelper::map($query, 'id', 'lang.title'));
    }

}
