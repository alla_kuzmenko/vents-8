<?php
namespace backend\modules\location\controllers;

use backend\modules\location\models\{
    Company, CompanyLang, search\Company as filterCompanyModel
};
use thread\app\base\controllers\BackendController;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */
class CompanyController extends BackendController
{
    public $model = Company::class;
    public $modelLang = CompanyLang::class;
    public $filterModel = filterCompanyModel::class;
    public $title = 'Company';
}
