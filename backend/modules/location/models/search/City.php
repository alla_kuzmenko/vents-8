<?php

namespace backend\modules\location\models\search;


use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\location\Location as LocationModule;
use thread\app\model\interfaces\search\BaseBackendSearchModel;
use backend\modules\location\models\{
    City as CityModel, CityLang
};

class City extends CityModel implements BaseBackendSearchModel
{
    public $title;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['location_country_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 255],
            [['published'], 'in', 'range' => array_keys(self::statusKeyRange())],
        ];
    }

    /**
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param ActiveQuery $query
     * @param array $params
     * @return ActiveDataProvider
     */
    public function baseSearch($query, $params)
    {
        /** @var LocationModule $module */
        $module = Yii::$app->getModule('location');
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => $module->itemOnPage
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC
                    ]
                ]
            ]
        );

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['in', 'location_country_id', $this->location_country_id]);
        $query->andFilterWhere(['like', 'published', $this->published]);
        $query->andFilterWhere(['like', CityLang::tableName() . '.title', $this->title]);

        return $dataProvider;
    }

    /**
     * Search
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CityModel::find()->joinWith(['lang'])->undeleted();
        return $this->baseSearch($query, $params);
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function trash($params)
    {
        $query = CityModel::find()->joinWith(['lang'])->deleted();
        return $this->baseSearch($query, $params);
    }
}
