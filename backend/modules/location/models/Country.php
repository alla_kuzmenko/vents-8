<?php

namespace backend\modules\location\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\modules\location\models\Country as CommonCountryModel;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */

class Country extends CommonCountryModel
{
    /**
     * Find base Page object for current language active and undeleted
     *
     * @return ActiveQuery
     */
    public static function findBase()
    {
        return parent::findBase()->_lang()->enabled();
    }

    /**
     * Find ONE Model object in array by its alias
     *
     * @param $alias
     * @return mixed
     */
    public static function findByAlias($alias)
    {
        return self::findBase()->alias($alias)->asArray()->one();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        $title = !empty($this->lang)
            ? $this->lang->title
            : "{'ru-RU' " . $this->default_title . "}";
        return $title;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->language == 'ru-RU' && isset($this->lang->title))
            $this->default_title =  $this->lang->title;

        return parent::beforeSave($insert);
    }

    /**
     * Backend form dropdown list
     * @return array
     */
    public static function getDropdownList()
    {
        return ArrayHelper::map(self::findBase()->all(), 'id', 'lang.title');
    }

    /**
     * @param $params
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params)
    {
        return (new search\Country())->search($params);
    }

    /**
     * @param $params
     * @return \yii\data\ActiveDataProvider
     */
    public function trash($params)
    {
        return (new search\Country())->trash($params);
    }
}
