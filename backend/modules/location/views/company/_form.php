<?php
use backend\themes\inspinia\widgets\forms\ActiveForm;
use yii\helpers\Url;

/**
 * @var $model \backend\modules\location\models\City
 * @var $modelLang \backend\modules\location\models\CityLang
 * @var $form \backend\themes\inspinia\widgets\forms\ActiveForm
 */

$Country = ($model->country_id) ? \backend\modules\location\models\Country::dropDownList() : [];
$City = ($model->city_id) ?  \backend\modules\location\models\City::dropDownList() : [];
$type_marker= \backend\modules\location\models\Company::typeMarkerRange();
?>

<?php $form = ActiveForm::begin(); ?>

<?= \backend\themes\inspinia\widgets\forms\Form::submit($model, $this); ?>


<?= $form->field($modelLang, 'title')->textInput(['maxlength' => true]); ?>
<?= $form->field($modelLang, 'street')->textInput(['maxlength' => true]); ?>
<?= $form->field($modelLang, 'house')->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'type_marker')->dropDownList($type_marker); ?>

<?= $form->field($model, 'region_id')->dropDownList([0=>'-- not selected --'] + \backend\modules\location\models\Region::dropDownList()); ?>
<?= $form->field($model, 'country_id')->dropDownList([0=>'-- not selected --'] + $Country); ?>
<?= $form->field($model, 'city_id')->dropDownList([0=>'-- not selected --'] + $City); ?>

<?= $form->field($model, 'lat')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'lng')->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'link_site')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'link_shop')->textInput(['maxlength' => true]); ?>

<?= $form->field($modelLang, 'content')->editor(); ?>

<?= $form->field($model, 'published')->checkbox(); ?>

<?= \backend\themes\inspinia\widgets\forms\Form::submit($model, $this); ?>

<?php ActiveForm::end();

$this->registerJs("
$( document ).ready(function() {

    $('#company-region_id').on('change', function() {
       getCountry();
    })

    $('#company-country_id').on('change', function() {
       getCity();
    })

    function getCountry() {
        $.ajax({
            type: 'GET',
            url: '".Url::toRoute(['/location/country/getcountrybyregion'])."',
            data: 'region='+$('#company-region_id').val(),
        })
        .success(function( data ) {
            data = JSON.parse(data);
            console.log(data);
            $('#company-country_id').empty();
            if (data) {
                 $('#company-country_id').append('<option>-- not selected --</option>');
                for (key in data) {
                    $('#company-country_id').append('<option value= '+ key + ' >   ' + data[key] + ' </option>');
                }
            }
        })
    }

    function getCity() {
        $.ajax({
            type: 'GET',
            url: '".Url::toRoute(['/location/city/getcitybycountry'])."',
            data: 'country='+$('#company-country_id').val(),
        })
        .success(function( data ) {
            data = JSON.parse(data);
            console.log(data);
            $('#company-city_id').empty();
            if (data) {
                $('#company-city_id').append('<option>-- not selected --</option>');
                for (key in data) {
                    $('#company-city_id').append('<option value= '+ key + ' >   ' + data[key] + ' </option>');
                }
            }
        })
    }
});
");