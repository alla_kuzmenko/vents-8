<?php
use backend\themes\inspinia\widgets\GridView;
use backend\modules\location\models\{
    Region, Country, City
};

/**
 * @var \backend\modules\location\models\search\Company $model
 */
echo GridView::widget([
    'dataProvider' => $model->search(Yii::$app->request->queryParams),
    'title' => Yii::t('app', 'Company'),
    'filterModel' => $filter,
    'columns' => [
        [
            'attribute' => 'title',
            'value' => 'lang.title',
            'label' => Yii::t('app', 'Title'),
        ],
        'lang.street',
        'lang.house',
        [
            'attribute' => 'region_id',
            'value' => 'region.lang.title',
            'filter' => Region::getDropdownList()
        ],
        [
            'attribute' => 'country_id',
            'value' => 'country.lang.title',
            'filter' => Country::getDropdownList()
        ],
        [
            'attribute' => 'city_id',
            'value' => 'city.lang.title',
            'filter' => City::getDropdownList()
        ],
        [
            'attribute' => 'type_marker',
            'value' => 'type_marker',
        ],

        [
            'class' => \thread\widgets\grid\ActionCheckboxColumn::class,
            'attribute' => 'published',
            'action' => 'published'
        ],
        [
            'class' => \backend\themes\inspinia\widgets\gridColumns\ActionColumn::class
        ],

    ]
]);
