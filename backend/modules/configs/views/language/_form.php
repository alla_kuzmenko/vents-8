<?php
use backend\themes\inspinia\widgets\forms\ActiveForm;

/**
 * @var \backend\modules\news\models\GroupLang $modelLang
 * @var \backend\modules\news\models\Group $model
 */
$form = ActiveForm::begin();
echo \backend\themes\inspinia\widgets\forms\Form::submit($model, $this, 'left');
echo $form->field($model, 'alias')->textInput(['maxlength' => true]);
echo $form->field($model, 'local')->textInput(['maxlength' => true]);
echo $form->field($model, 'label')->textInput(['maxlength' => true]);
echo '<h2 style="color: darkgreen">Добавляемая картинка должна быть 17х11 в формате PNG!!!</h2>';
echo $form->field($model, 'img_flag')->imageOne($model->getImage());
echo $form->field($model, 'default')->checkbox();
echo $form->field($model, 'published')->checkbox();
echo $form->field($model, 'only_editor')->checkbox();
echo \backend\themes\inspinia\widgets\forms\Form::submit($model, $this, 'left');
ActiveForm::end();
