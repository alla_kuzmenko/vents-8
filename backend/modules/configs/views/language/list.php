<?php

use yii\helpers\Html;
use thread\widgets\grid\SwitchboxColumn;
use backend\themes\inspinia\widgets\GridView;

echo GridView::widget([
    'dataProvider' => $model->search(Yii::$app->request->queryParams),
    'title' => Yii::t('app', 'Language'),
    'filterModel' => $filter,
    'columns' => [
        [
            'attribute' => 'img_flag',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::img($data->getImage());
            },
        ],
        'alias',
        'label',
        'local',
        [
            'class' => \thread\widgets\grid\ActionCheckboxColumn::class,
            'attribute' => 'default',
            'action' => 'default',
            'disabled' => true
        ],
        [
            'class' => \thread\widgets\grid\ActionCheckboxColumn::class,
            'attribute' => 'published',
            'action' => 'published'
        ],
        [
            'class' => \thread\widgets\grid\ActionCheckboxColumn::class,
            'attribute' => 'only_editor',
            'action' => 'only_editor'
        ],
        [
            'class' => \backend\themes\inspinia\widgets\gridColumns\ActionColumn::class
        ],
    ]
]);
