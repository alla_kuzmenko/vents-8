<?php
use backend\themes\inspinia\widgets\forms\ActiveForm;

/**
 * @var \backend\modules\news\models\GroupLang $modelLang
 * @var \backend\modules\news\models\Group $model
 */
$form = ActiveForm::begin();
echo \backend\themes\inspinia\widgets\forms\Form::submit($model, $this, 'left');
echo $form->field($modelLang, 'title')->textarea();
echo $form->field($model, 'value')->textInput(['maxlength' => true]);
echo \backend\themes\inspinia\widgets\forms\Form::submit($model, $this, 'left');
ActiveForm::end();
