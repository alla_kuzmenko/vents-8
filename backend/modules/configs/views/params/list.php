<?php

use yii\helpers\Html;
//
use thread\widgets\grid\SwitchboxColumn;
//
use backend\themes\inspinia\widgets\GridView;

echo GridView::widget([
    'dataProvider' => $model->search(Yii::$app->request->queryParams),
    'title' => Yii::t('app', 'Params'),
    'filterModel' => $filter,
    'columns' => [
        'id',
        [
            'attribute' => 'title',
            'value' => 'lang.title',
            'label' => Yii::t('app', 'Value'),
        ],
        [
            'attribute' => 'value',
            'value' => 'value',
            'label' => Yii::t('app', 'Title'),
        ],
        [
            'class' => \backend\themes\inspinia\widgets\gridColumns\ActionColumn::class
        ],
    ]
]);
