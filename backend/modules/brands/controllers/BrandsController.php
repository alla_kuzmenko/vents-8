<?php
namespace backend\modules\brands\controllers;

use thread\actions\AttributeSwitch;
use Yii;
use backend\modules\brands\models\search\BrandsItem;
use backend\modules\brands\models\BrandsItemLang;
use thread\actions\fileapi\UploadAction;
use thread\app\base\controllers\BackendController;
use yii\helpers\ArrayHelper;

/**
 * Class BrandsController
 *
 * @package backend\modules\brands\controllers
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class BrandsController extends BackendController
{
    public $model = BrandsItem::class;
    public $modelLang = BrandsItemLang::class;
    public $title = 'Brands';

    /**
     * @return array
     */

    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'fileupload' => [
                    'class' => UploadAction::class,
                    'path' => Yii::$app->getModule('brands')->getUploadPath()
                ],
                'onindex' => [
                    'class' => AttributeSwitch::class,
                    'modelClass' => $this->model,
                    'attribute' => 'on_index'
                ],
            ]
        );
    }
}
