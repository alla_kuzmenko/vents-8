<?php
namespace backend\modules\brands\models;

use common\modules\brands\models\BrandsItem as CommonBrandsItemModel;
use Yii;

/**
 * Class BrandsItem
 *
 * @package backend\modules\brands\models
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class BrandsItem extends CommonBrandsItemModel
{

    /**
     * @return string
     */
    public function getTitle()
    {
        $title = !empty($this->lang)
            ? $this->lang->title
            : "{'ru-RU' " . $this->default_title . "}";
        return $title;
    }
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->language == 'ru-RU' && isset($this->lang->title))
            $this->default_title =  $this->lang->title;

        return parent::beforeSave($insert);
    }
}
