<?php

use backend\modules\brands\models\{
    BrandsItem, BrandsItemLang
};
use thread\app\base\models\ActiveRecord;
use backend\themes\inspinia\widgets\forms\ActiveForm;

/**
 * @var BrandsItem $model
 * @var BrandsItemLang|ActiveRecord $modelLang
 */

$form = ActiveForm::begin();

echo \backend\themes\inspinia\widgets\forms\Form::submit($model, $this);
echo $form->field($modelLang, 'title')->textInput(['maxlength' => true]);
echo $form->field($model, 'link')->textInput(['maxlength' => true]);
echo $form->field($model, 'image_link')->imageOne($model->getArticleImage());
echo $form->field($model, 'image_link_s')->imageOne($model->getArticleImageSmall());
echo $form->field($model, 'alias')->textInput(['maxlength' => true]);
echo $form->field($modelLang, 'description')->textArea(['rows' => '3']);
echo $form->field($model, 'position')->textInput();
echo $form->field($model, 'on_index')->checkbox();
echo $form->field($model, 'published')->checkbox();
echo \backend\themes\inspinia\widgets\forms\Form::submit($model, $this);

ActiveForm::end();
