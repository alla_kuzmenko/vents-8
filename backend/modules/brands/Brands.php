<?php
namespace backend\modules\brands;

use Yii;
use common\modules\brands\Brands as BrandsModule;

/**
 * Class Brands
 *
 * @package backend\modules\brands
 * @author Andrii Bondarchuk
 * @copyright (c) 2016
 */
class Brands extends BrandsModule
{
    /**
     * Number of elements in GridView
     * @var int
     */
    public $itemOnPage = 20;

    /**
     * Image upload path
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getAlias('@uploads') . '/' . $this->name . '/';
    }

    /**
     * Image upload URL
     * @return string
     */
    public function getUploadUrl()
    {
        return '/uploads/' . $this->name . '/';
    }
}
