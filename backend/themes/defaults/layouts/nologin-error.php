<?php

/**
 * @author FilamentV <vortex.filament@gmail.com>
 * @copyright (c) 2015, Thread
 */
$this->beginContent('@app/layouts/main.php');
?>
<?= $content ?>
<?php $this->endContent(); ?>