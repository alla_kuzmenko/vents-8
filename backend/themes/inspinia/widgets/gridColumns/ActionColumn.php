<?php
namespace backend\themes\inspinia\widgets\gridColumns;

use Closure;
use thread\app\base\models\ActiveRecord;
use yii\grid\Column;
use yii\helpers\Html;

/**
 * @author Roman Gonchar <roman.gonchar92@gmail.com>
 */

class ActionColumn extends Column
{

    /**
     * @var array
     */
    public $contentOptions = [
        'class' => 'text-center',
    ];

    /**
     * @var array
     */
    public $headerOptions = [
        'class' => 'text-center'
    ];

    /**
     * @var mixed
     */
    public $updateLink;

    /**
     * @var mixed
     */
    public $deleteLink;

    public $onDelete = true;
    public $onUpdate = true;

    public $updateDisabledIds = [];
    public $deleteDisabledIds = [];

    /**
     * @var string
     */
    public $header = 'Actions';

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        return $this->renderUpdateLink($model) . ' ' . $this->renderDeleteLink($model);
    }

    /**
     * @param ActiveRecord $model
     * @return array|bool
     */
    protected function getUpdateLink($model)
    {
        if (!empty($this->updateLink)) {
            if ($this->updateLink instanceof \Closure) {
                $f = $this->updateLink;
                return $f($model);
            } else {
                $r = ['update'];
                foreach ($this->updateLink as $data) {
                    $r[$data] = $model->$data;
                }
                return $r;
            }
        } else {
            return ['update', 'id' => $model->id];
        }
    }

    /**
     * @param ActiveRecord $model
     * @return array|bool
     */
    protected function getDeleteLink($model)
    {
        if (!empty($this->deleteLink)) {
            if ($this->deleteLink instanceof \Closure) {
                $f = $this->deleteLink;
                return $f($model);
            } else {
                $r = ['intrash'];
                foreach ($this->deleteLink as $data) {
                    $r[$data] = $model->$data;
                }

                return $r;
            }
        }
        return (isset($r)) ? $r : ['intrash', 'id' => $model->id];
    }

    /**
     * Render action update button
     * @param $model
     * @return string
     */
    protected function renderUpdateLink($model)
    {
        return Html::a('<i class="fa fa-pencil"></i> ', $this->getUpdateLink($model), [
            'class' => 'btn btn-success btn-xs',
        ]);
    }

    /**
     * Render action delete button
     * @param $model
     * @return string
     */
    protected function renderDeleteLink($model)
    {

        if ($this->onDelete instanceof Closure) {
            $view = call_user_func($this->onDelete, $model);
        }

        if (isset($view) && $view) {
            return Html::a('<i class="fa fa-trash"></i> ', $this->getDeleteLink($model), [
                'class' => 'btn btn-danger btn-xs'
            ]);
        }
    }

    /**
     * Renders the header cell content.
     * The default implementation simply renders [[header]].
     * This method may be overridden to customize the rendering of the header cell.
     * @return string the rendering result
     */
    protected function renderHeaderCellContent()
    {
        return trim($this->header) !== '' ? \Yii::t('app', $this->header) : $this->getHeaderCellLabel();
    }
}
